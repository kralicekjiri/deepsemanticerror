# coding:utf-8
import csv
import cv2
import time
import os
import numpy as np
import math
import random
import matplotlib.pyplot as plt

from data_util import GeneratorEnqueuer

min_text_size =  5  #if the text size is smaller than this, we ignore it during training')
min_crop_side_ratio =  0.1 #when doing random crop from input image, the min length of min(H, W')


def get_images(data_path):
  
  base_dir = os.path.dirname(data_path)
  with open(data_path) as f:
    files = f.readlines()
  files = [x.strip() for x in files] 
  files_out = []
  for x in files:
    if len(x) == 0:
      continue
    if not x[0] == '/':
      x = '{0}/{1}'.format(base_dir, x)
    files_out.append(x) 
  return files_out


def load_annoataion(p):
  '''
  load annotation from the text file
  :param p:
  :return:
  '''
  text_polys = []
  text_tags = []
  if not os.path.exists(p):
    return np.array(text_polys, dtype=np.float32)
  with open(p, 'r') as f:
    reader = csv.reader(f,  delimiter=' ',  quotechar='"')
    for line in reader:
      label = line[-1]
      # strip BOM. \ufeff for python3,  \xef\xbb\bf for python2
      line = [i.strip('\ufeff').strip('\xef\xbb\xbf') for i in line]
      
      cls, x, y, w, h, angle = list(map(float, line[:6]))
      label = 'train'
      #cls = 0 
      text_polys.append([x, y, w, h, angle, cls])
      if label == '*' or label == '###':
          text_tags.append(True)
      else:
          text_tags.append(False)
    return np.array(text_polys, dtype=np.float), np.array(text_tags, dtype=np.bool)

def draw_box_points(img, points, color = (0, 255, 0), thickness = 1):
  try:  
    cv2.line(img, (points[0][0], points[0][1]), (points[1][0], points[1][1]), color, thickness)
    cv2.line(img, (points[2][0], points[2][1]), (points[1][0], points[1][1]), color, thickness)
    cv2.line(img, (points[2][0], points[2][1]), (points[3][0], points[3][1]), color, thickness)
    cv2.line(img, (points[0][0], points[0][1]), (points[3][0], points[3][1]), color, thickness)
  except:
    pass


def random_crop(img, word_gto, crop_ratio = 0.2, vis = False):

  crops = []
  
  x_minus = 0
  y_minus = 0
  if img.shape[1] > img.shape[0]:
    x_minus = img.shape[1] - img.shape[0]
  else:
    y_minus = img.shape[0] - img.shape[1]
    
  crop_size = min(img.shape[1], img.shape[0])
  xs = int(random.uniform(0, crop_ratio * crop_size))
  xe = int(img.shape[1] - xs - x_minus - random.uniform(0, crop_ratio * crop_size))

  ys = int(random.uniform(0, crop_ratio * crop_size))
  ye =  int(img.shape[0] - ys - y_minus -  random.uniform(0, crop_ratio * crop_size))

  crop_rect = (xs, ys, xe, ye)
  crop_img = img[crop_rect[1]:crop_rect[3], crop_rect[0]:crop_rect[2]]
  crops.append(crop_rect)

  normo = math.sqrt(img.shape[1] * img.shape[1] + img.shape[0] * img.shape[0]);
  image_size = (crop_img.shape[1], crop_img.shape[0])
  normo2 = math.sqrt(image_size[0] * image_size[0] + image_size[1] * image_size[1] )

  word_gto[:, 0] = (word_gto[:, 0] * img.shape[1] - xs)/ float(crop_img.shape[1])
  word_gto[:, 1] = (word_gto[:, 1] * img.shape[0] - ys)/ float(crop_img.shape[0])

  for i in range(0, word_gto.shape[0]):
    gt = word_gto[i, :]
    angle = gt[4]
    if(angle < -50):
      angle = 0
    gtbox = ((gt[0] * image_size[0], gt[1] * image_size[1]), (gt[2] * normo, gt[3] * normo), angle * 180 / math.pi)
    pts = cv2.boxPoints(gtbox)
    
    
    dh = pts[0] - pts[1]
    dw = pts[1] - pts[2]

    h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1]) / normo2
    w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1]) / normo2

    gt[2] = w
    gt[3] = h
    if gt[4] > -50:
      gt[4] = math.atan2((pts[2][1] - pts[1][1]), (pts[2][0] - pts[1][0]))
    
    if vis:
      gtbox = ((gt[0] * crop_img.shape[1], gt[1] * crop_img.shape[0]), (gt[2] * normo2, gt[3] * normo2), angle * 180 / math.pi)
      
      pts = cv2.boxPoints(gtbox)
      draw_box_points(crop_img, pts, (255, 0, 0), 2)
      cv2.imshow('crop', crop_img)
      cv2.waitKey(0)
    
  return crop_img

def random_rotation(img,  word_gto):

  center = (img.shape[1] / 2, img.shape[0] / 2)
  angle =  random.uniform(-300, 300) / 10
  M  = cv2.getRotationMatrix2D(center, angle, 1)
  dst_size = (img.shape[1], img.shape[0])
  dst = cv2.warpAffine(img, M, dst_size)

  angle_rad = - angle * math.pi / 180
  for i in range(0, word_gto.shape[0]):
    word = word_gto[i]
    #float x2 = ((word[0] * img.cols - center.x) * cos(angle_rad)) - ((word[1] * img.rows - center.y) * sin(angle_rad)) + center.x;
    x2 = ((word[0] * img.shape[1] - center[0]) * math.cos(angle_rad)) - ((word[1] * img.shape[0] - center[1]) * math.sin(angle_rad)) + center[0]
    #float y2 = ((word[0] * img.cols - center.x) * sin(angle_rad)) + ((word[1] * img.rows - center.y) * cos(angle_rad)) + center.y;
    y2 = ((word[0] * img.shape[1] - center[0]) * math.sin(angle_rad)) + ((word[1] * img.shape[0] - center[1]) * math.cos(angle_rad)) + center[1]
    word[0] = x2 / dst_size[0]
    word[1] = y2 / dst_size[1]
    word[4] += angle_rad

  return dst


def transform_boxes(im, scaled, word_gto, tags, vis = False):

  image_size = (scaled.shape[1], scaled.shape[0])
  o_size = (im.shape[1], im.shape[0])

  normo = math.sqrt(o_size[0] * o_size[0] + o_size[1] * o_size[1] )
  normo2 = math.sqrt(scaled.shape[0] * scaled.shape[0] + scaled.shape[1] * scaled.shape[1] )
  scalex = o_size[0] / float(image_size[0])
  scaley = o_size[1] / float(image_size[1])
  
  
  for poly_idx, gt in enumerate(word_gto):
    
    angle = gt[4];
    if(angle < -50):
      angle = 0
      
    gtbox = ( (gt[0] * o_size[0], gt[1] * o_size[1]), (gt[2] * normo, gt[3] * normo), angle * 180 / math.pi)
    pts = cv2.boxPoints(gtbox) 
    
    pts[:, 0]/= scalex
    pts[:, 1] /= scaley
    if np.sum(pts[pts <= 0]) > 0:
      tags[poly_idx]  = True 
    if np.sum(pts[:, 0] > scaled.shape[1]) > 0:
      tags[poly_idx] = True
    if np.sum(pts[:, 1] > scaled.shape[0]) > 0:
      tags[poly_idx] = True

    dh = pts[0] - pts[1];
    dw = pts[1] - pts[2];

    h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1]) / normo2
    w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1]) / normo2
    
    gt[0] = (np.sum(pts[:, 0]) / 4) / scaled.shape[1]
    gt[1] = np.sum(pts[:, 1]) / 4 / scaled.shape[0]
    
    gt[2] = w
    gt[3] = h

    if gt[4] < -50:
      gt[4] = -100;
    else:
      gt[4] = math.atan2((pts[2][1] - pts[1][1]), pts[2][0] - pts[1][0])
      
    if vis:
      gtbox = ((gt[0] * scaled.shape[1], gt[1] * scaled.shape[0]), (gt[2] * normo2, gt[3] * normo2), angle * 180 / math.pi)
      
      pts = cv2.boxPoints(gtbox)
      draw_box_points(scaled, pts, (255, 0, 0), 2)
      cv2.imshow('crop', scaled)
      cv2.waitKey(0)
      
def point_dist_to_line(p1, p2, p3):
    # compute the distance from p3 to p1-p2
    cross = np.linalg.norm(np.cross(p2 - p1, p1 - p3))
    norm = np.linalg.norm(p2 - p1)
    if norm > 0:
      return cross / norm 
    return cross
      

def generate_rbox(im, im_size, polys, tags, vis = False):
  h, w = im_size
  scale_factor = 4
  hs = int(h / scale_factor)
  ws = int(w / scale_factor)
  
  poly_mask = np.zeros((hs, ws), dtype=np.uint8)
  score_map = np.zeros((hs, ws), dtype=np.uint8)
  geo_map = np.zeros((hs, ws, 5), dtype=np.float32)
  # mask used during traning, to ignore some hard areas
  training_mask = np.ones((hs, ws), dtype=np.uint8)
  normo = math.sqrt(w * w + h * h)
  for poly_idx, poly_tag in enumerate(zip(polys, tags)):
    gt = poly_tag[0]
    angle = gt[4]
    if angle > math.pi:
      angle -= math.pi
    if angle < math.pi:
      angle += math.pi
    tag = poly_tag[1]
    # score map
      
    poly_h = gt[3] * normo / scale_factor
    gtbox = ((gt[0] * w, gt[1] * h), (gt[2] * normo // 1.5, gt[3] * normo // 4), angle * 180 / math.pi)
    pts = cv2.boxPoints(gtbox)
    pts = pts / scale_factor
    gtbox2 = ((gt[0] * w, gt[1] * h), (gt[2] * normo, gt[3] * normo), angle * 180 / math.pi)
    pts2 = cv2.boxPoints(gtbox2)
    pts2 = pts2 / scale_factor
    
    if tag == True or poly_h < 1 or np.sum(pts < 0) > 0:
      cv2.fillPoly(training_mask, np.asarray([pts2], np.int32), 0)
      continue
    
    cv2.fillPoly(score_map, np.asarray([pts], np.int32), 1)
    cv2.fillPoly(poly_mask, np.asarray([pts], np.int32), poly_idx + 1)
    
    # TODO filter small
    xy_in_poly = np.argwhere(poly_mask == (poly_idx + 1))
    
    if vis:
      scaled = cv2.resize(im, dsize=(int(im.shape[1] / scale_factor), int(im.shape[0]/ scale_factor)))
      draw_box_points(scaled, pts, (0, 255, 0), 2)
      cv2.imshow('im', scaled)
      
      pts_o = pts * scale_factor
      draw_box_points(im, pts_o, (255, 0, 0), 2)
      cv2.imshow('orig', im)
      cv2.waitKey(0)
    
    
    for y, x in xy_in_poly:
      point = np.array([x, y], dtype=np.float32)
      
      d1 = point_dist_to_line(pts2[1], pts2[2], point)
      geo_map[y, x, 0] = d1 
      
      d1 = point_dist_to_line(pts2[0], pts2[3], point)
      geo_map[y, x, 1] = d1 
      
      d1 = point_dist_to_line(pts2[0], pts2[1], point)
      geo_map[y, x, 2] = d1 
      
      d1 = point_dist_to_line(pts2[2], pts2[3], point)
      geo_map[y, x, 3] = d1 
      
      geo_map[y, x, 4] = angle
      
  score_map[training_mask == 0] = 0
  return score_map, geo_map, training_mask


def generator(input_size=512, batch_size=4, train_list='/mnt/textspotter/software/yash_data/ICDAR_MLT/icdar_mlt_train.txt', vis=False, in_train = True):
  image_list = get_images(train_list)
  print('{} training images in {}'.format(len(image_list), train_list))
  index = np.arange(0, len(image_list))
  while True:
    if in_train:
      np.random.shuffle(index)
    images = []
    image_fns = []
    score_maps = []
    geo_maps = []
    training_masks = []
    for i in index:
      try:
        im_fn = image_list[i]
        #if len(images) == 0:
        #  im_fn = '/mnt/textspotter/tmp/SynthText/icdar2013-Train/311.jpg'
        #print(im_fn)
        if not os.path.exists(im_fn):
          continue
        im = cv2.imread(im_fn)
        if im is None:
          continue
        h, w, _ = im.shape
        txt_fn = im_fn.replace(os.path.basename(im_fn).split('.')[1], 'txt')
        if (not os.path.exists(txt_fn) and in_train):
          continue
        elif in_train:
          text_polys, text_tags = load_annoataion(txt_fn)
        if in_train:
          im = random_rotation(im, text_polys)
          im = random_crop(im, text_polys, vis=False)
          if random.randint(0, 100) < 10:
            im = np.invert(im)
        #TODO - random rotation 
        if in_train: 
          if text_polys.shape[0] == 0:
              continue
        h, w, _ = im.shape
        new_h, new_w, _ = im.shape
        resize_h = input_size
        resize_w = input_size
        if input_size == -1:
          image_size = [(im.shape[1]) // 32 * 32 , (im.shape[0]) // 32 * 32] 
          while image_size[0] * image_size[1] > 1024 * 600:
            image_size[0] /= 1.5        
            image_size[1] /= 1.5   
            image_size[0] = int(image_size[0] // 32) * 32
            image_size[1] = int(image_size[1] // 32) * 32
          print(image_size)    
          resize_h = int(image_size[1])
          resize_w = int(image_size[0])
                    
        scaled = cv2.resize(im, dsize=(resize_w, resize_h))
        if in_train:
          transform_boxes(im, scaled, text_polys, text_tags, vis=False)
        
        im = scaled
        new_h, new_w, _ = im.shape
        if in_train:
          score_map, geo_map, training_mask = generate_rbox(im, (new_h, new_w), text_polys, text_tags)
          if score_map.sum() == 0:
            continue
        if vis:
          fig, axs = plt.subplots(3, 2, figsize=(20, 30))
          axs[0, 0].imshow(im[:, :, ::-1])
          axs[0, 0].set_xticks([])
          axs[0, 0].set_yticks([])
          axs[0, 1].imshow(score_map[::, ::])
          axs[0, 1].set_xticks([])
          axs[0, 1].set_yticks([])
          axs[1, 0].imshow(geo_map[::, ::, 0])
          axs[1, 0].set_xticks([])
          axs[1, 0].set_yticks([])
          axs[1, 1].imshow(geo_map[::, ::, 1])
          axs[1, 1].set_xticks([])
          axs[1, 1].set_yticks([])
          axs[2, 0].imshow(geo_map[::, ::, 2])
          axs[2, 0].set_xticks([])
          axs[2, 0].set_yticks([])
          axs[2, 1].imshow(training_mask[::, ::])
          axs[2, 1].set_xticks([])
          axs[2, 1].set_yticks([])
          plt.tight_layout()
          plt.show()
          plt.close()
        
        
        images.append(im[:, :, ::-1].astype(np.float32))
        image_fns.append(im_fn)
        if in_train:
          score_maps.append(score_map[:, :, np.newaxis].astype(np.float32))
          geo_maps.append(geo_map[:, :, :].astype(np.float32))
          training_masks.append(training_mask[:, :, np.newaxis].astype(np.float32))
        if len(images) == batch_size:
          images = np.asarray(images, dtype=np.float)
          images /= 128
          images -= 1
          if in_train:
            training_masks = np.asarray(training_masks, dtype=np.uint8)
            score_maps = np.asarray(score_maps, dtype=np.uint8)
            geo_maps = np.asarray(geo_maps, dtype=np.float)
            yield images, image_fns, score_maps, geo_maps, training_masks
          else:
            yield images, image_fns
          images = []
          image_fns = []
          score_maps = []
          geo_maps = []
          training_masks = []
      except Exception as e:
        import traceback
        traceback.print_exc()
        continue
      
    if not in_train:
      yield None
      break    


def get_batch(num_workers, **kwargs):
  try:
    enqueuer = GeneratorEnqueuer(generator(**kwargs), use_multiprocessing=True)
    enqueuer.start(max_queue_size=24, workers=num_workers)
    generator_output = None
    while True:
      while enqueuer.is_running():
        if not enqueuer.queue.empty():
          generator_output = enqueuer.queue.get()
          break
        else:
          time.sleep(0.01)
      yield generator_output
      generator_output = None
  finally:
    if enqueuer is not None:
      enqueuer.stop()

if __name__ == '__main__':
  
  data_generator = get_batch(num_workers=1, input_size=544, batch_size=1, vis=True)
  while True:
    data = next(data_generator)
  
