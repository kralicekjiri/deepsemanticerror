import torch
import numpy as np
import cv2
import net_utils
import argparse
import icdar_2
import time
import math
from icdar_2 import draw_box_points
import locality_aware_nms as nms_locality
from torch.utils.serialization import load_lua
from models import Model, ModelResNet, ModelResNetSep
import lanms
import sys

def  get_txt_file_name(img_name):
  if '.png' in img_name:
    txt_file = img_name.replace('.png', '.txt')
  elif '.jpg' in img_name:
    txt_file = img_name.replace('.jpg', '.txt')
  elif '.jpeg' in img_name:
    txt_file = img_name.replace('.jpeg', '.txt')
  elif '.JPG' in img_name:
    txt_file = img_name.replace('.JPG', '.txt')
  elif '.gif' in img_name:
    txt_file = img_name.replace('.gif', '.txt')
  else:
    print(img_name)
  return 'icdar_test_3/' + txt_file.replace('ts','res')


def get_points(img, iou_map, rbox, angle_pred, args, im_name):
  print(im_name)
  draw = np.copy(img)
  xy_text = np.argwhere(iou_map > args.score_map_thresh)
  if xy_text.shape[0] == 0:
    return
  polys = []
  ious = []
  angle_pred = angle_pred.squeeze(0)
  for i in range(0, xy_text.shape[0]):
    pos = xy_text[i]
    angle = angle_pred[pos[0], pos[1]]
    cv2.circle(draw, (pos[1] * 4, pos[0] * 4), 2, (0, 0, 255) )
    offset = rbox[pos[0], pos[1]]
    pos_g = np.array([(pos[1] - offset[0] * math.sin(angle)) * 4, (pos[0] - offset[0] * math.cos(angle)) * 4 ])
    pos_g2 = np.array([ (pos[1] + offset[1] * math.sin(angle)) * 4, (pos[0] + offset[1] * math.cos(angle)) * 4 ])
    pos_r = np.array([(pos[1] - offset[2] * math.cos(angle)) * 4, (pos[0] - offset[2] * math.sin(angle)) * 4 ])
    pos_r2 = np.array([(pos[1] + offset[3] * math.cos(angle)) * 4, (pos[0] + offset[3] * math.sin(angle)) * 4 ])
    center = (pos_g + pos_g2 + pos_r + pos_r2) / 4
    dw = pos_r - pos_r2
    dh =  pos_g - pos_g2
    w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1])
    h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1])
    rect = ( (center[0], center[1]), (w, h), angle * 180 / math.pi )
    pts = cv2.boxPoints(rect)
    polys.append(pts)
    ious.append(iou_map[pos[0], pos[1]])
    draw_box_points(draw, pts)
  polys = np.asarray(polys)
  polys = polys.reshape(polys.shape[0], -1)
  ious = np.asarray(ious)
  ious = ious.reshape(ious.shape[0], 1)
  stacked = np.hstack((polys, ious))
  return stacked

def write_points(img, im_name, boxes):
  draw2 = np.copy(img)
  f_out = open(get_txt_file_name(im_name[0].split('/')[7]), 'w')
  for i in range(0, boxes.shape[0]):
    pts = boxes[i]
    prob = 1
    pts  = pts[0:8]
    pts = pts.reshape(4, -1)
    pts = np.asarray(pts, dtype=np.int)
    string_to_write = str(pts[0][0]) + ','+str(pts[0][1]) + ','+str(pts[1][0]) + ','+str(pts[1][1]) + ','+str(pts[2][0]) + ','+str(pts[2][1]) +','+ str(pts[3][0]) + ','+str(pts[3][1]) + ','+str(prob)+'\n'
    f_out.write(string_to_write)
    draw_box_points(draw2, pts)
  f_out.close()
  print(boxes.shape)
  del boxes
  #cv2.imshow('orig', draw)
  #cv2.imshow('nms', cv2.resize(draw2, (640,640)))
 
def get_image(im_name, scale):
  im = cv2.imread(im_name)
  new_h, new_w, _ = im.shape
  image_size = [(scale*im.shape[1]) // 32 * 32 , (scale*im.shape[0]) // 32 * 32]
  resize_h = int(image_size[1])
  resize_w = int(image_size[0])
  scaled = cv2.resize(im, dsize=(resize_w, resize_h))
  im = scaled
  return im

def convert_points(im_org, box, scale):
        image_size = [(scale*im_org.shape[1]) // 32 * 32, (scale*im_org.shape[0]) // 32 * 32]
        resize_h = int(image_size[1])
        resize_w = int(image_size[0])
        original_h = im_org.shape[0]
        original_w = im_org.shape[1]
        org_box = []
        for i in range(0,len(box)):
                org_box.append([round(box[i][0]*(original_w/resize_w)) , round(box[i][1]*(original_h/resize_h))])
        return org_box

if __name__ == '__main__': 
  
  parser = argparse.ArgumentParser()
  parser.add_argument('-valid_list', default='/mnt/textspotter/software/yash_data/ICDAR_MLT/icdar_mlt_test.txt')
  parser.add_argument('-cuda', type=int, default=1)
  parser.add_argument('-model', default='/mnt/textspotter/software/yash_data/detector/real_only/SSTextResSep_456000.h5')
  parser.add_argument('-debug', default=1)
  parser.add_argument('-score_map_thresh', default=0.8)
  parser.add_argument('-small', type=int, default=2)
  args = parser.parse_args()    
  
  if args.small == 1:
    net = ModelResNet()
  elif args.small == 2:
    print("Using resnet")
    net = ModelResNetSep()
  else:
    net = Model()
    
  if args.cuda:
    print('Using cuda ...')
    net = net.cuda()
  net_utils.load_net(args.model, net)
  net = net.eval()
  
  test_list = open(args.valid_list, 'r').read().splitlines()

  list_scales = [0.5, 1.0, 1.5]

  for given_image in test_list:

    im_org = cv2.imread(given_image)
    if im_org is None:
      continue
    all_points = []
    for given_scale in list_scales:
      images = []
      image_fns = []
      im = get_image(given_image, given_scale)
      images.append(im[:, :, ::-1].astype(np.float32))
      image_fns.append(given_image)
      images = np.asarray(images, dtype=np.float)
      images /= 128
      images -= 1
      
      im_data = net_utils.np_to_variable(images, is_cuda=args.cuda, volatile=True).permute(0, 3, 1, 2)
      start = time.time()
      iou_pred, rbox, angle_pred = net(im_data)
      end = time.time()
      seconds = end - start
      fps = 1 / seconds 
      print("loc fps:{0}".format(fps))
      
      del im_data
      
      rbox = rbox.data.cpu()[0].numpy()
      rbox = rbox.swapaxes(0, 1)
      rbox = rbox.swapaxes(1, 2)
      iou = iou_pred.data.cpu()[0].numpy()
      iou = iou.squeeze(0)
      points_stacked = get_points(images[0], iou, 
                      rbox, angle_pred.data.cpu()[0].numpy(), args, image_fns)
      if points_stacked is None:
        continue
      for i in range(0,len(points_stacked)):
        prob = points_stacked[i][8]
        box = convert_points(im_org, points_stacked[i][0:8].reshape(4,-1), given_scale)
        box = [box[0][0], box[0][1], box[1][0], box[1][1], box[2][0], box[2][1], box[3][0], box[3][1], prob]
        all_points.append(box)

    boxes = lanms.merge_quadrangle_n9(np.asarray(all_points).astype(np.float64), 0.3)
    write_points(im_org, image_fns, boxes)
    cv2.waitKey(0)
    del rbox, angle_pred, iou_pred, iou, image_fns, images
