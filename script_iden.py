from __future__ import division

'''
Created on Sep 3, 2017

@author: Michal.Busta at gmail.com
'''
import os, sys
import json
import torch
import numpy as np
import cv2
from random import shuffle
import net_utils
import random
import argparse

import math
from torchvision import transforms
from icdar import draw_box_points

from models import ModelResNetSep2


from ocr_tools2 import OCRModel
from ocr_test_utils import print_seq_ext

from text_utils import normalize_text
from demo import resize_image, resize_image_up

from eval import get_boxes
import torch.nn.functional as F

import csv
import unicodedata as ud
from PIL import Image
import torch.nn as nn 

f = open('codec.txt', 'r')
codec = f.readlines()[0]
#codec = u' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~£ÁČĎÉĚÍŇÓŘŠŤÚŮÝŽáčďéěíňóřšťúůýž'
f.close()
print(len(codec))

buckets = [54, 80, 124, 182, 272, 410, 614, 922, 1383, 2212]

eval_text_length = 3

jap_sym = '々乄匂圷圸塰墸妛屶扌𢭏曻膤栃槞樰橳橸檪欟毛毟火熕燵牛牜瓦瓧瓰田畩疂疒癪石硴礀竹筺簗簱籏籗籡笹米籾粭舟艠艸苆𦰩萙蘰虫蟐衣褝襅角鵤足身躮躵軈辵込𨑕遖𨕫邑𨛗金鈩錺錻鎺鑓門閖阜陦風颪食饂𩛰魚魞魸鮖鮗鮱鮲鯎鯏鯑𩸽鯱鰘鰚鰰鱛鱜鱩鱪鱫鱰鳥鳰鴫鵆鵇鶫龜𪚲円'

maping_lang = {}
maping_lang[''] = 'Symbols'
maping_lang['LATIN'] = 'Latin'
maping_lang['DIGIT'] = 'Latin'
maping_lang['ARABIC'] = 'Arabic'
maping_lang['BENGALI'] = 'Bangla'
maping_lang['HANGUL'] = 'Korean'
maping_lang['CJK'] = 'Chinese'
maping_lang['HIRAGANA'] = 'Japanese'
maping_lang['KATAKANA'] = 'Japanese'

scripts = ['', 'DIGIT', 'LATIN', 'ARABIC', 'BENGALI', 'HANGUL', 'CJK', 'HIRAGANA', 'KATAKANA']

def load_detections(p):
  '''
  load annotation from the text file
  :param p:
  :return:
  '''
  text_polys = []
  if not os.path.exists(p):
    return np.array(text_polys, dtype=np.float32)
  with open(p, 'r') as f:
    reader = csv.reader(f,  delimiter=',',  quotechar='"')
    for line in reader:
      # strip BOM. \ufeff for python3,  \xef\xbb\bf for python2
      line = [i.strip('\ufeff').strip('\xef\xbb\xbf') for i in line]
      
      x2, y2, x3, y3, x4, y4, x1, y1, conf = list(map(float, line[:9]))
      #cls = 0 
      text_polys.append([x1, y1, x2, y2, x3, y3, x4, y4, conf])
     
    return np.array(text_polys, dtype=np.float)
  
def load_gt(p, is_icdar = False):
  '''
  load annotation from the text file, 
  :param p:
  :return:
  '''
  text_polys = []
  text_gts = []
  if not os.path.exists(p):
    return np.array(text_polys, dtype=np.float32)
  with open(p, 'r') as f:
    reader = csv.reader(f,  delimiter=',',  quotechar='"')
    for line in reader:
      # strip BOM. \ufeff for python3,  \xef\xbb\bf for python2
      line = [i.strip('\ufeff').strip('\xef\xbb\xbf') for i in line]
      
      x1, y1, x2, y2, x3, y3, x4, y4 = list(map(float, line[:8]))
      #cls = 0
      gt_txt = '' 
      delim = ''
      start_idx = 9
      if is_icdar:
        start_idx = 8
      
      for idx in range(start_idx, len(line)):
        gt_txt += delim + line[idx]
        delim = ','
        
      text_polys.append([x4, y4, x1, y1, x2, y2, x3, y3])
      text_line = normalize_text(gt_txt)
        
      
      text_gts.append(text_line) 
     
    return np.array(text_polys, dtype=np.float), text_gts

def ocr_image(net, codec, img, norm_height, args):
          
  scale = norm_height / float(img.shape[0])
  ssf_y = norm_height / 32.0
  width = int(img.shape[1] * scale)
  
  best_diff = width
  bestb = 0
  for b in range(0, len(buckets)):
    if best_diff > abs(width * 1.3 - buckets[b] * ssf_y):
      best_diff = abs(width * 1.3 - buckets[b] * ssf_y)
      bestb = b

  width =  buckets[bestb] 
  
  scaled = cv2.resize(img, (int(buckets[bestb] * ssf_y), norm_height))  
  scaled = np.expand_dims(scaled, axis=2)
  scaled = np.expand_dims(scaled, axis=0)
  
  
  scaled_var = net_utils.np_to_variable(scaled, is_cuda=args.cuda, volatile=True).permute(0, 3, 1, 2)
  ctc_f = net(scaled_var)
  ctc_f = ctc_f.data.cpu().numpy()
  ctc_f = ctc_f.swapaxes(1, 2)
  
  labels = ctc_f.argmax(2)
  ind = np.unravel_index(labels, ctc_f.shape)
  confidence = np.mean( np.exp(ctc_f[ind]) )
  
  det_text, conf, dec_s = print_seq_ext(labels[0, :], codec)  
  
  if len(det_text) > 0 and 'ARABIC' in ud.name(det_text[0]):
    det_text = det_text[::-1]
  
  return det_text, confidence, dec_s

def draw_detections(img, boxes, color = (255, 0, 0)):
  
  draw2 = np.copy(img)
  if len(boxes) == 0:
    return draw2
  for i in range(0, boxes.shape[0]):
    pts = boxes[i]
    pts  = pts[0:8]
    pts = pts.reshape(4, -1)
    pts = np.asarray(pts, dtype=np.int)
    draw_box_points(draw2, pts, color=color, thickness=2)
    
  #cv2.imshow('nms', draw2)
  
  return draw2
  
def get_normalized_image(img, box):
  
  box2 = box[0:8].reshape(4, 2)
  box2 = np.asarray(box2, dtype=np.float32)
  
  dw = box2[2] - box2[1] 
  dh =  box2[0] - box2[1]
    
  w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1]) * 1.1
  h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1]) * 1.1
  center = box2.sum(0) / 4
  angle = math.atan2(dw[1], dw[0])  
  rr = ( (center[0], center[1]), (w + h, h * 1.1), angle * 180 / math.pi )
  #box2 = cv2.boxPoints(rr)
  
  extbox = cv2.boundingRect(box2)

  if extbox[2] *  extbox[3] > img.shape[0] * img.shape[1]:
    print("Too big proposal: {0}x{1}".format(extbox[2], extbox[3]))
    return None, None
  extbox = [extbox[0], extbox[1], extbox[2], extbox[3]]
  extbox[2] += extbox[0]
  extbox[3] += extbox[1]
  extbox = np.array(extbox, np.int)
  
  extbox[0] = max(0, extbox[0])
  extbox[1] = max(0, extbox[1])
  extbox[2] = min(img.shape[1], extbox[2])
  extbox[3] = min(img.shape[0], extbox[3])
  
  tmp = img[extbox[1]:extbox[3], extbox[0]:extbox[2]]
  center = (tmp.shape[1] / 2,  tmp.shape[0] / 2)
  rot_mat = cv2.getRotationMatrix2D( center, rr[2], 1 )
  
  if tmp.shape[0] == 0 or tmp.shape[1] == 0:
    return None, rot_mat
  
  
  rot_mat[0,2] += rr[1][0] /2.0 - center[0]
  rot_mat[1,2] += rr[1][1] /2.0 - center[1]
  try:
    norm_line = cv2.warpAffine( tmp, rot_mat, (int(rr[1][0]), int(rr[1][1])), borderMode=cv2.BORDER_REPLICATE )
  except:
    return None, rot_mat
  return norm_line, rot_mat  
 

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 

import glob


def intersect(a, b):
  '''Determine the intersection of two rectangles'''
  rect = (0,0,0,0)
  r0 = max(a[0],b[0])
  c0 = max(a[1],b[1])
  r1 = min(a[2],b[2])
  c1 = min(a[3],b[3])
  # Do we have a valid intersection?
  if r1 > r0 and  c1 > c0: 
      rect = (r0,c0,r1,c1)
  return rect

def union(a, b):
  r0 = min(a[0],b[0])
  c0 = min(a[1],b[1])
  r1 = max(a[2],b[2])
  c1 = max(a[3],b[3])
  return (r0,c0,r1,c1)

def area(a):
  '''Computes rectangle area'''
  width = a[2] - a[0]
  height = a[3] - a[1]
  return width * height

import editdistance

def evaluate_image(img, detections, gt_rect, gt_txts, iou_th=0.5, iou_th_vis=0.5, iou_th_eval=0.5):
    
  '''
  Summary : Returns end-to-end true-positives, detection true-positives, number of GT to be considered for eval (len > 2).
  Description : For each predicted bounding-box, comparision is made with each GT entry. Values of number of end-to-end true
                positives, number of detection true positives, number of GT entries to be considered for evaluation are computed.
  
  Parameters
  ----------
  iou_th_eval : float
      Threshold value of intersection-over-union used for evaluation of predicted bounding-boxes
  iou_th_vis : float
      Threshold value of intersection-over-union used for visualization when transciption is true but IoU is lesser.
  iou_th : float
      Threshold value of intersection-over-union between GT and prediction.
  word_gto : list of lists
      List of ground-truth bounding boxes along with transcription.
  batch : list of lists
      List containing data (input image, image file name, ground truth).
  detections : tuple of tuples
      Tuple of predicted bounding boxes along with transcriptions and text/no-text score.
  
  Returns
  -------
  tp : int
      Number of predicted bounding-boxes having IoU with GT greater than iou_th_eval.
  tp_e2e : int
      Number of predicted bounding-boxes having same transciption as GT and len > 2.
  gt_e2e : int
      Number of GT entries for which transcription len > 2.
  '''
  
  gt_to_detection = {}
  detection_to_gt = {}
  tp = 0
  tp_e2e = 0
  tp_e2e_ed1 = 0
  gt_e2e = 0
  
  gt_matches = np.zeros(gt_rect.shape[0])
  gt_matches_ed1 = np.zeros(gt_rect.shape[0])
  
  for i in range(0, len(detections)):
      
    det = detections[i]
    box =  det[0] # Predicted bounding-box parameters
    box = np.array(box, dtype="int") # Convert predicted bounding-box to numpy array
    box = box[0:8].reshape(4, 2)
    bbox = cv2.boundingRect(box)
    
    bbox = [bbox[0], bbox[1], bbox[2], bbox[3]]
    bbox[2] += bbox[0] # Convert width to right-coordinate
    bbox[3] += bbox[1] # Convert height to bottom-coordinate
    
    det_text = det[1] # Predicted transcription for bounding-box
    
    for gt_no in range(len(gt_rect)):
        
      gtbox = gt_rect[gt_no]
      txt = gt_txts[gt_no] # GT transcription for given GT bounding-box
      gtbox = np.array(gtbox, dtype="int")
      gtbox = gtbox[0:8].reshape(4, 2)
      rect_gt = cv2.boundingRect(gtbox)
      
      
      rect_gt = [rect_gt[0], rect_gt[1], rect_gt[2], rect_gt[3]]
      rect_gt[2] += rect_gt[0] # Convert GT width to right-coordinate
      rect_gt[3] += rect_gt[1] # Convert GT height to bottom-coordinate 

      inter = intersect(bbox, rect_gt) # Intersection of predicted and GT bounding-boxes
      uni = union(bbox, rect_gt) # Union of predicted and GT bounding-boxes
      ratio = area(inter) / float(area(uni)) # IoU measure between predicted and GT bounding-boxes
      
      # 1). Visualize the predicted-bounding box if IoU with GT is higher than IoU threshold (iou_th) (Always required)
      # 2). Visualize the predicted-bounding box if transcription matches the GT and condition 1. holds
      # 3). Visualize the predicted-bounding box if transcription matches and IoU with GT is less than iou_th_vis and 1. and 2. hold
      if ratio > iou_th:
        if not gt_no in gt_to_detection:
          gt_to_detection[gt_no] = [0, 0]
          
        edit_dist = editdistance.eval(det_text.lower(), txt.lower())
        if edit_dist <= 1:
          gt_matches_ed1[gt_no] = 1
          draw_box_points(img, box, color = (0, 128, 0), thickness=2)
            
        if det_text.lower().find(txt.lower()) != -1:
          draw_box_points(img, box, color = (0, 255, 0), thickness=2)
          gt_matches[gt_no] = 1 # Change this parameter to 1 when predicted transcription is correct.
          
          if ratio < iou_th_vis:
              #draw_box_points(draw, box, color = (255, 255, 255), thickness=2)
              #cv2.imshow('draw', draw) 
              #cv2.waitKey(0)
              pass
                
        tupl = gt_to_detection[gt_no] 
        if tupl[0] < ratio:
          tupl[0] = ratio 
          tupl[1] = i 
          detection_to_gt[i] = [gt_no, ratio, edit_dist]  
                  
  # Count the number of end-to-end and detection true-positives
  for gt_no in range(gt_matches.shape[0]):
    gt = gt_matches[gt_no]
    gt_ed1 = gt_matches_ed1[gt_no]
    txt = gt_txts[gt_no]
    
    gtbox = gt_rect[gt_no]
    gtbox = np.array(gtbox, dtype="int")
    gtbox = gtbox[0:8].reshape(4, 2)
    
    if len(txt) >= eval_text_length and not txt.startswith('###'):
      gt_e2e += 1
      if gt == 1:
        tp_e2e += 1
      if gt_ed1 == 1:
        tp_e2e_ed1 += 1
        
            
    if gt_no in gt_to_detection:
      tupl = gt_to_detection[gt_no] 
      if tupl[0] > iou_th_eval: # Increment detection true-positive, if IoU is greater than iou_th_eval
        if len(txt) >= eval_text_length and not txt.startswith('###'):
          tp += 1   
      #else:
      #  draw_box_points(img, gtbox, color = (255, 255, 255), thickness=2)
        
  for i in range(0, len(detections)):  
    det = detections[i]
    box =  det[0] # Predicted bounding-box parameters
    box = np.array(box, dtype="int") # Convert predicted bounding-box to numpy array
    box = box[0:8].reshape(4, 2)
    
    if not i in detection_to_gt:
      draw_box_points(img, box, color = (0, 0, 255), thickness=2)
    else:
      [gt_no, ratio, edit_dist] = detection_to_gt[i]
      if edit_dist > 0:
        draw_box_points(img, box, color = (255, 0, 0), thickness=2)
            
  #cv2.imshow('draw', draw)             
  return tp, tp_e2e, gt_e2e, tp_e2e_ed1, detection_to_gt 

def load_sc_images(list_images, data_dir, given_scale, train_aug):
  h = int(given_scale.split('x')[1])
  w = int(given_scale.split('x')[0])
  batch_size = len(list_images)
  X = torch.zeros((batch_size, 3, w, h), dtype=torch.float32)
  Y = torch.zeros((batch_size,), dtype=torch.float32)
  for idx in range(0, batch_size):
    im_name = data_dir + list_images[idx][0]
    label = list_images[idx][1]
    im = Image.open(im_name).convert('RGB').resize(size=(w,h), resample=Image.NEAREST)
    im = train_aug(im)
    X[idx, :, :, :] = im.transpose(1,2)
    Y[idx] = int(label)
  return X,Y  

class e2e_mlt_sc(nn.Module):
  def __init__(self, base_model, num_classes=7):
    super(e2e_mlt_sc, self).__init__()

    self.base_model = base_model
    self.sc = nn.Sequential(nn.Linear(out_features=7500, in_features=7500),
      nn.ReLU(),
      nn.Dropout(0.5),
      nn.Linear(out_features=512, in_features=7500),
      nn.ReLU(),
      nn.Dropout(0.5),
      nn.Linear(out_features=num_classes, in_features=512),
      nn.Softmax(dim=1))

  def forward(self, x):
      x = self.base_model.layer0(x)
      x = self.base_model.layer0_1(x)
      x = self.base_model.conv_ocr0(x)
      if x.size(2) > 3:
        center = x.size(2) // 2
        x = x[:, :, (center - 1):(center + 2), :]
      x = self.base_model.conv_ocr[0](x)
      x = self.base_model.conv_ocr[1](x)
      x = self.base_model.conv_ocr[2](x)
      x = self.base_model.conv_ocr[3](x)
      x = self.base_model.conv_ocr[4](x)
      x = self.base_model.conv_ocr[5](x)
      x = self.base_model.conv_ocr[3](x)
      x = self.base_model.conv_ocr[4](x)
      x = self.base_model.conv_ocr[5](x)
      x = self.base_model.conv_ocr[6](x)
      x = self.base_model.conv_ocr[7](x)
      x = self.base_model.conv_ocr[8](x)
      x = self.base_model.conv_ocr[9](x)
      x = self.base_model.conv_ocr[10](x)
      x = self.base_model.conv_ocr[11](x)
      x = self.base_model.drop1(x)
      x = self.base_model.conv_ocr[12](x)
      x = x.squeeze(2)
      x = x.permute(0,2,1)
      y = x
      x = x.contiguous().view(-1,x.data.shape[2])
      #x = LogSoftmax()(x)
      x = x.view_as(y)
      x = x.permute(0,2,1)
      x = torch.mean(x.view(x.size(0), x.size(1), -1), dim=2)
      x = self.sc(x)
      return x

if __name__ == '__main__': 
  
  parser = argparse.ArgumentParser()
  parser.add_argument('-cuda', type=int, default=1)
  parser.add_argument('-model', default='/mnt/textspotter/tmp/DS_CVPR/backup/SemanticTexte2e_396000.h5')
  parser.add_argument('-images_dir', default='/home/busta/data/ch4_test')
  parser.add_argument('-debug', default=0)
  parser.add_argument('-score_map_thresh', default=0.9)
  parser.add_argument('-evaluate', default=1)
  parser.add_argument('-is_icdar', default=1)
  parser.add_argument('-out_dir', default='eval')

  data_dir = '/mnt/textspotter/software/yash_data/ICDAR_cropped/all_images/'
  labels = json.load(open('/mnt/textspotter/software/yash_data/ICDAR_cropped/all_images/gt_all_data.json', 'r'))
  num_epochs = 20
  lr = float(0.001)
  batch_size = int(16)

  args = parser.parse_args()
  
  base_net = ModelResNetSep2()
  net_utils.load_net(args.model, base_net)
  #base_net.train()
  #base_net.cuda()
  
  net = e2e_mlt_sc(base_net, num_classes=7)
  net = net.train()
  net = net.cuda()

  optimizer = torch.optim.SGD(net.parameters(), lr=lr, momentum=0.9)
  criterion = nn.NLLLoss(weight=None, size_average=True).cuda()
  train_aug = transforms.Compose([transforms.RandomRotation(45), transforms.RandomHorizontalFlip(p=0.5), transforms.ToTensor(), transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
  
  for given_epoch in range(0, num_epochs):
    for given_scale in labels.keys():
      scale_list = sorted(labels[given_scale], key=lambda k: random.random())
      num_iterations = int(len(scale_list)/batch_size)
      for given_iter in range(0, num_iterations):
        start_idx = given_iter*batch_size
        end_idx = (given_iter+1)*batch_size
        subset_images = scale_list[start_idx:end_idx]
        X,Y = load_sc_images(subset_images, data_dir, given_scale, train_aug)
        X = torch.autograd.Variable(X.cuda(async=True), requires_grad=True)
        Y = torch.autograd.Variable(Y.cuda(async=True)).long()
        pred_sc = net(X)
        loss = criterion(pred_sc, Y)
        print(loss)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    if given_epoch%2 == 0:
       torch.save(net.state_dict(), 'model_'+str(given_epoch)+'.h5')

    if given_epoch%5 == 0 and given_epoch != 0:
       lr = lr*0.1
       for param_group in optimizer.param_groups:
          param_group['lr'] = lr
