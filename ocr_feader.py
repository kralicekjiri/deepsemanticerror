'''
Created on Mar 10, 2018

@author: busta
'''

import os
import cv2
import random

import numpy as np

class OCRFeeder(object):
  '''
  classdocs
  '''


  def __init__(self, input_file, input_file2 = '/mnt/textspotter/tmp/90kDICT32px/train_mlt_real_synth.txt'):
    '''
    Constructor
    '''
    base_dir = os.path.dirname(input_file)
    files_out = []
    cnt = 0
    with open(input_file) as f:
      while True:
        line = f.readline()
        if not line: 
          break
        line = line.strip()
        if len(line) == 0:
          continue
        if not line[0] == '/':
          line = '{0}/{1}'.format(base_dir, line)
        files_out.append(line)  
        cnt +=1
        #if cnt > 100:
        #  break
    base_dir = os.path.dirname(input_file2)
    with open(input_file2) as f:
      while True:
        line = f.readline()
        if not line: 
          break
        line = line.strip()
        if len(line) == 0:
          continue
        if not line[0] == '/':
          line = '{0}/{1}'.format(base_dir, line)
        files_out.append(line)  
        cnt +=1
        
    self.lines = files_out
  
  def get_sample(self):
    
    while True:
    
      image_name = self.lines[int(random.uniform(0, len(self.lines) - 1))]    
      
      spl = image_name.split(" ")
      if len(spl) == 1:
        spl = image_name.split(",")
      image_name = spl[0].strip()
      gt_txt = ''
      if len(spl) > 1:
        gt_txt = ""
        delim = ""
        for k in range(1, len(spl)):
          gt_txt += delim + spl[k]
          delim =" "
        if len(gt_txt) > 1 and gt_txt[0] == '"' and gt_txt[-1] == '"':
            gt_txt = gt_txt[1:-1]
            
      if not os.path.exists(image_name):
        continue
      img = cv2.imread(image_name)
      if  image_name.find( 'yash_data/chinese_0' ) != -1:
        img =  np.rot90(img)
    
      return img, gt_txt
          
    