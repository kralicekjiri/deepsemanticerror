'''
Created on Sep 3, 2017

@author: Michal.Busta at gmail.com
'''
import os, sys
sys.path.append('./build')

import torch
import numpy as np
import cv2

import net_utils
import argparse
import math

from icdar import draw_box_points
from models import ModelResNetSep2
from ocr_test_utils import print_seq_ext, print_seq2

from text_utils import normalize_text
from demo import resize_image

from eval import get_boxes, area
import torch.nn.functional as F

import csv
import unicodedata as ud

f = open('codec.txt', 'r')
codec = f.readlines()[0]
#codec = u' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~£ÁČĎÉĚÍŇÓŘŠŤÚŮÝŽáčďéěíňóřšťúůýž'
f.close()
print(len(codec))

#import cmp_trie

from eval import evaluate_image

jap_sym = '々乄匂圷圸塰墸妛屶扌𢭏曻膤栃槞樰橳橸檪欟毛毟火熕燵牛牜瓦瓧瓰田畩疂疒癪石硴礀竹筺簗簱籏籗籡笹米籾粭舟艠艸苆𦰩萙蘰虫蟐衣褝襅角鵤足身躮躵軈辵込𨑕遖𨕫邑𨛗金鈩錺錻鎺鑓門閖阜陦風颪食饂𩛰魚魞魸鮖鮗鮱鮲鯎鯏鯑𩸽鯱鰘鰚鰰鱛鱜鱩鱪鱫鱰鳥鳰鴫鵆鵇鶫龜𪚲円'

maping_lang = {}
maping_lang[''] = 'Symbols'
maping_lang['LATIN'] = 'Latin'
maping_lang['DIGIT'] = 'Latin'
maping_lang['ARABIC'] = 'Arabic'
maping_lang['BENGALI'] = 'Bangla'
maping_lang['HANGUL'] = 'Korean'
maping_lang['CJK'] = 'Chinese'
maping_lang['HIRAGANA'] = 'Japanese'
maping_lang['KATAKANA'] = 'Japanese'

scripts = ['', 'DIGIT', 'LATIN', 'ARABIC', 'BENGALI', 'HANGUL', 'CJK', 'HIRAGANA', 'KATAKANA']

eval_text_length = 3

def load_detections(p):
  '''
  load annotation from the text file
  :param p:
  :return:
  '''
  text_polys = []
  if not os.path.exists(p):
    return np.array(text_polys, dtype=np.float32)
  with open(p, 'r') as f:
    reader = csv.reader(f,  delimiter=',',  quotechar='"')
    for line in reader:
      # strip BOM. \ufeff for python3,  \xef\xbb\bf for python2
      line = [i.strip('\ufeff').strip('\xef\xbb\xbf') for i in line]
      
      x2, y2, x3, y3, x4, y4, x1, y1, conf = list(map(float, line[:9]))
      #cls = 0 
      text_polys.append([x1, y1, x2, y2, x3, y3, x4, y4, conf])
     
    return np.array(text_polys, dtype=np.float)
  
def load_gt(p, is_icdar = False):
  '''
  load annotation from the text file, 
  :param p:
  :return:
  '''
  text_polys = []
  text_gts = []
  if not os.path.exists(p):
    return np.array(text_polys, dtype=np.float32)
  with open(p, 'r') as f:
    reader = csv.reader(f,  delimiter=',',  quotechar='"')
    for line in reader:
      # strip BOM. \ufeff for python3,  \xef\xbb\bf for python2
      line = [i.strip('\ufeff').strip('\xef\xbb\xbf') for i in line]
      
      x1, y1, x2, y2, x3, y3, x4, y4 = list(map(float, line[:8]))
      #cls = 0
      gt_txt = '' 
      delim = ''
      start_idx = 9
      if is_icdar:
        start_idx = 8
      
      for idx in range(start_idx, len(line)):
        gt_txt += delim + line[idx]
        delim = ','
        
      text_polys.append([x4, y4, x1, y1, x2, y2, x3, y3])
      text_line = normalize_text(gt_txt)
        
      
      text_gts.append(text_line) 
     
    return np.array(text_polys, dtype=np.float), text_gts



def draw_detections(img, boxes, color = (255, 0, 0)):
  
  draw2 = np.copy(img)
  if len(boxes) == 0:
    return draw2
  for i in range(0, boxes.shape[0]):
    pts = boxes[i]
    pts  = pts[0:8]
    pts = pts.reshape(4, -1)
    pts = np.asarray(pts, dtype=np.int)
    draw_box_points(draw2, pts, color=color, thickness=2)
    
  #cv2.imshow('nms', draw2)
  
  return draw2
  
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 

import glob

def process_splits(im, trans, conf, splits, ctc_f, rot_mat, angle, box_points, w, h, draw, is_dict, debug = False):
  '''
  Summary : Split the transciption and corresponding bounding-box based on spaces predicted by recognizer FCN.
  Description : 

  Parameters
  ----------
  trans : string
      String containing the predicted transcription for the corresponding predicted bounding-box.
  conf : list
      List containing sum of confidence for all the character by recognizer FCN, start and end position in bounding-box for generated transciption.
  splits :  list
      List containing index of position of predicted spaces by the recognizer FCN.
  norm2 : matrix
      Matrix containing the cropped bounding-box predicted by localization FCN in the originial image.
  ctc_f : matrix
      Matrix containing output of recognizer FCN for the given input bounding-box.
  rot_mat : matrix
      Rotation matrix returned by get_normalized_image function.
  boxt : tuple of tuples
      Tuple of tuples containing parametes of predicted bounding-box by localization FCN.
  draw : matrix
      Matrix containing input image.
  is_dict : 
  debug : boolean
      Boolean parameter representing debug mode, if it is True visualization boxes are generated.

  Returns
  -------
  boxes_out : list of tuples
      List of tuples containing predicted bounding-box parameters, predicted transcription and mean confidence score from the recognizer.
  '''
  spl = trans.split(" ")
  boxout = np.copy(box_points)
  #draw_box_points(draw, boxout, color = (0, 255, 0), thickness=2)
  start_f = 0
  mean_conf = conf[0, 0] / max(1, len(trans)) # Overall confidence of recognizer FCN
  boxes_out = []
  y = 0
  for s in range(len(spl)):
    text = spl[s]
    end_f = splits[0, s]
    if s < len(spl) - 1:
        try:
            if splits[0, s] > start_f:
                end_f = splits[0, s] # New ending point of bounding-box transcription
        except IndexError:
            pass
    scalex = w / float(ctc_f.shape[1])
    poss = start_f * scalex
    pose = (end_f + 2) * scalex
    rect = [[poss, h], [poss, y], [pose, y], [pose, h]]
    rect = np.array(rect)
    int_t = rot_mat
    dst_rect = np.copy(rect)
    dst_rect[:,0]  = int_t[0,0]*rect[:,0] + int_t[0,1]*rect[:, 1] + int_t[0,2]
    dst_rect[:,1]  = int_t[1,0]*rect[:,0] + int_t[1,1]*rect[:, 1] + int_t[1,2]
    dst_rect[:,0] += boxout[1, 0] 
    dst_rect[:,1] += boxout[1, 1] 
    
    if debug:
        #draw_box_points(draw, boxout, color = (0, 255, 0), thickness=2)
        draw_box_points(draw, dst_rect, color = (0, 255, 0))
        cv2.imshow('draw', draw)
        cv2.waitKey(0)
    if True or text.isdigit() or cmp_trie.is_dict(text):
        boxes_out.append( (dst_rect, (text, mean_conf, is_dict) ) )
    start_f = end_f + 1
  return boxes_out 



if __name__ == '__main__': 
  
  parser = argparse.ArgumentParser()
  parser.add_argument('-cuda', type=int, default=1)
  #parser.add_argument('-model', default='backup/SemanticTexte2e_142000.h5')
  parser.add_argument('-model', default='/mnt/textspotter/tmp/DS_CVPR/backup/SemanticTexte2e_143000.h5')
  #parser.add_argument('-model', default='/mnt/textspotter/tmp/DS_CVPR/backup2/ICCV_OCRF_ELU_1163000.h5')
  #parser.add_argument('-model', default='/mnt/textspotter/tmp/DS_CVPR/backup/OCRF_2000.h5')
  #parser.add_argument('-model', default='/datagrid/personal/qqbusta/git/DS_CVPR/backup/SSTextResSep50_470000.h5')
  #parser.add_argument('-model', default='SSTextResSep50_190000.h5')
  #parser.add_argument('-images_dir', default='/home/busta/data/icdar2015-Ch4-Train')
  #parser.add_argument('-images_dir', default='/home/busta/data/ch8_validation_e2e')
  parser.add_argument('-images_dir', default='/home/busta/data/ch4_test')
  parser.add_argument('-debug', default=0)
  parser.add_argument('-score_map_thresh', default=0.9)
  parser.add_argument('-evaluate', type=int, default=1)
  parser.add_argument('-is_icdar', type=int, default=1)
  parser.add_argument('-out_dir', default='eval')
  parser.add_argument('-eval_text_length', type=int, default=3)
  
  args = parser.parse_args()
  
  net = ModelResNetSep2()
  net_utils.load_net(args.model, net)
  net = net.eval()
    
  if args.cuda:
    print('Using cuda ...')
    net = net.cuda()
  
  images = glob.glob( os.path.join(args.images_dir, '*.jpg') )
  
  #cmp_trie.load_dict('/home/busta/data/icdar2013-Test/GenericVocabulary.txt')
  #cmp_trie.load_codec('codec.txt')
  
  tp_all = 0  
  gt_all = 0
  tp_e2e_all = 0
  gt_e2e_all = 0
  tp_e2e_ed1_all = 0
  detecitons_all = 0
  
  im_no = 0
  
  min_height = 10
  if args.is_icdar:
    min_height = 12
    
  
  if not os.path.exists('eval'):
    os.mkdir(args.out_dir)
  if not os.path.exists('preview'):
    os.mkdir('preview')
    
  eval_text_length = args.eval_text_length
  
  nums = []
  image_no = 0
  for img_name in sorted(images):
    try:
      num = int( os.path.basename(img_name).replace('ts_', "").replace("img_", "").replace('.jpg', ""))
      nums.append( (num,  img_name) )
    except:
      nums.append( (image_no,  img_name) )
      image_no += 1
    
  for tp in sorted(nums, key=lambda images: images[0]):
    num = tp[0]
    img_name = tp[1]
    base_nam = os.path.basename(img_name)
    
    if args.evaluate == 1:
      res_gt = base_nam.replace(".jpg", '.txt')
      res_gt = '{0}/gt_{1}'.format(args.images_dir, res_gt)
      if not os.path.exists(res_gt):
        res_gt = base_nam.replace(".jpg", '.txt').replace("_", "")
        res_gt = '{0}/gt_{1}'.format(args.images_dir, res_gt)
        if not os.path.exists(res_gt):
          print('missing! {0}'.format(res_gt))
          continue
      gt_rect, gt_txts  = load_gt(res_gt, args.is_icdar==1)
    
    print(img_name)
    
    img = cv2.imread(img_name)
    
    im_resized, (ratio_h, ratio_w) = resize_image(img, max_size=1648*1024, scale_up=True) #1348*1024
    #im_resized = im_resized[:, :, ::-1]
    images = np.asarray([im_resized], dtype=np.float)
    images /= 128
    images -= images.mean()
    im_data = net_utils.np_to_variable(images, is_cuda=args.cuda, volatile=True).permute(0, 3, 1, 2)
    
    iou_pred, rboxs, angle_pred, features = net(im_data) 
    iou = iou_pred.data.cpu()[0].numpy()
    iou = iou.squeeze(0)
    
    #cv2.imshow('iou', iou)
    
    rbox = rboxs.data.cpu()[0].numpy()
    rbox = rbox.swapaxes(0, 1)
    rbox = rbox.swapaxes(1, 2)
    
    detections = get_boxes(iou, rbox, angle_pred.data.cpu()[0].numpy(), args, scale_factor = 4)
    
    im_scalex = im_resized.shape[1] / img.shape[1]
    im_scaley = im_resized.shape[0] / img.shape[0]
    
    detectionso = np.copy(detections)
    if len(detections) > 0:
      detections[:, 0] /= im_scalex
      detections[:, 2] /= im_scalex
      detections[:, 4] /= im_scalex
      detections[:, 6] /= im_scalex
      
      detections[:, 1] /= im_scaley
      detections[:, 3] /= im_scaley
      detections[:, 5] /= im_scaley
      detections[:, 7] /= im_scaley
    
    #detections = load_detections(res_txt)
    draw = draw_detections(img, detections)
    #cv2.imshow('draw', draw)
    
    detetcions_out = []
    
    pil_img = Image.fromarray(draw)
    pil_draw = ImageDraw.Draw(pil_img)
    
    font = ImageFont.truetype("Arial-Unicode-Regular.ttf", 16)
    box_no = 0
    #detections = gt_rect
    
    res_file = os.path.join(args.out_dir,  'res_img_{num:05d}.txt'.format(num=num))
    res_file = open(res_file, 'w')
    
    for bid, box in enumerate(detections):      
      if True:
        boxo = detectionso[bid]
        score = boxo[8]
        boxr = boxo[0:8].reshape(-1, 2)
        box_area = area( boxr.reshape(8) )
        
        conf_factor = score / box_area
        #if conf_factor < 0.1:
        #  continue
        
        boxr2 = box[0:8].reshape(-1, 2)
        if boxr2.min() < 0:
          continue
        if boxr2[:, 0].max() > img.shape[1]:
          continue
        if boxr2[:, 1].max() > img.shape[0]:
          continue
        
        center = (boxr[0, :] + boxr[1, :] + boxr[2, :] + boxr[3, :]) / 4
        
        dw = boxr[2, :] - boxr[1, :]
        dh =  boxr[1, :] - boxr[0, :]
    
        h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1]) + 2
        w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1])
        
        if ( h ) / im_scaley < min_height:
          print('too small detection')
          continue
        
        input_W = im_data.size(3)
        input_H = im_data.size(2)
        target_h = 40  
          
        scale = target_h / h
        target_gw = int(w * scale + target_h)  
        #target_gw = max(2, target_gw // 4 + 1) * 4 
        xc = center[0] 
        yc = center[1] 
        w2 = w 
        h2 = h 
        
        angle = math.atan2((boxr[2][1] - boxr[1][1]), boxr[2][0] - boxr[1][0])
        
        #show pooled image in image layer
      
        scalex = (w2 +  h2) / input_W 
        scaley = h2 / input_H 
      
        th11 =  scalex * math.cos(angle)
        th12 = -math.sin(angle) * scaley
        th13 =  (2 * xc - input_W - 1) / (input_W - 1) #* torch.cos(angle_var) - (2 * yc - input_H - 1) / (input_H - 1) * torch.sin(angle_var)
        
        th21 = math.sin(angle) * scalex 
        th22 =  scaley * math.cos(angle)  
        th23 =  (2 * yc - input_H - 1) / (input_H - 1) #* torch.cos(angle_var) + (2 * xc - input_W - 1) / (input_W - 1) * torch.sin(angle_var)
                  
        t = np.asarray([th11, th12, th13, th21, th22, th23], dtype=np.float)
        t = torch.from_numpy(t).type(torch.FloatTensor)
        t = t.cuda()
        theta = t.view(-1, 2, 3)
        
        grid = F.affine_grid(theta, torch.Size((1, 3, int(target_h), int(target_gw))))
        x = F.grid_sample(im_data, grid)
        
        im = x.data.cpu().numpy()
        im = im.squeeze(0)
        im = im.swapaxes(0, 2)
        im = im.swapaxes(0, 1)
        #cv2.imshow('im', im)
        #cv2.waitKey(0)
        
        
        features = net.forward_features(x)
        labels_pred = net.forward_ocr(features)
        
        ctc_f = labels_pred.data.cpu().numpy()
        ctc_f = ctc_f.swapaxes(1, 2)
    
        labels = ctc_f.argmax(2)
        
        ind = np.unravel_index(labels, ctc_f.shape)
        conf = np.mean( np.exp(ctc_f[ind]) )
        
        if conf < 0.3:
          print('Too low conf!')
          continue 
        
        conf_raw = np.exp(ctc_f[ind])
        
        det_text, conf2, dec_s = print_seq_ext(labels[0, :], codec)  
        det_text = det_text.strip()
        det_text = normalize_text(det_text)
        try:
          if len(det_text) > 0 and 'ARABIC' in ud.name(det_text[0]):
            det_text = det_text[::-1]
        except:
          pass
        
        has_long = False
        if len(det_text) > 0:
          rot_mat = cv2.getRotationMatrix2D( (0, 0), -angle * 180 / math.pi, 1 )
          splits_raw = process_splits(im, det_text, conf_raw, dec_s, ctc_f, rot_mat, angle, boxr, w, h, im_resized, 0) # Process the split and improve the localization
          for spl in splits_raw:
            if len(spl[1][0]) >= eval_text_length:
              has_long = True
              boxw = spl[0]
              boxw[:, 0] /= im_scalex
              boxw[:, 1] /= im_scaley
              draw_box_points(img, boxw, color = (0, 255, 0))
              #cv2.imshow('img', img)
              #cv2.waitKey()
              detetcions_out.append([boxw, spl[1][0]])
              print('{0} - {1}'.format(spl[1][0], conf_factor))
        
        best_dict = ''
        '''
        norm_text = det_text.replace('.','').replace(',','').replace('?','').replace('!','').replace('$','').replace('%','').replace('-','')
        if len(det_text) >= 3 and cmp_trie.is_dict(norm_text) != 1 and not norm_text.isdigit():
          ctc_f = np.copy(ctc_f.reshape(ctc_f.shape[1], ctc_f.shape[2]))
          dec2, conf2, dec_splits = cmp_trie.decode_sofmax(ctc_f)
          if len(dec2) > 0:
            best_dict = print_seq2(dec2[0])
            best_dict = best_dict.replace('\x00', '')
            norm_dict = best_dict.replace('.','').replace(',','').replace('?','').replace('!','')
            print(' Dict: {0} -> {1}'.format(det_text, best_dict))
            #if not cmp_trie.is_dict(norm_dict):
            #  best_dict = ""
            #else:
            #  print(best_dict) 
      #'''  
      
    for box, det_text in detetcions_out:
      if len(det_text) < eval_text_length:
        continue  
      
      det_count =  [0, 0, 0,  0, 0, 0, 0, 0,  0]
      det_count = np.array(det_count)    
      for c_char in det_text:
        assigned = False 
        if c_char in jap_sym:
            print('has jap!')
            det_count[7] += 1
            assigned = True
            continue
        for idx, scr in enumerate(scripts):
          if idx == 0:
            continue
          try:
            symbol_name = ud.name(c_char)
          except:
            pass
          if scr in symbol_name:
            det_count[idx] += 1
            assigned = True
            break
        if not assigned:
          det_count[0] += 1
      
      maximum_indices_det = np.where(det_count==np.max(det_count))
      arg_idx = np.argsort(det_count)[::-1]
      max_idx = 0
      if (arg_idx[max_idx] == 0 or arg_idx[max_idx] == 1) and det_count[arg_idx[max_idx + 1]] > 0 and arg_idx[max_idx + 1] != 0:
        max_idx += 1 
    
      #if len(maximum_indices_det[0]) > 1:
      #  print(det_text) 
      #  max_idx = 1     
      script_det = scripts[arg_idx[max_idx]]   
      lang = maping_lang[script_det]
      if lang == 'Chinese' and (det_count[7] > 0 or det_count[8] > 0  ):
        lang = 'Japanese' 
          
      boxr = box[0:8].reshape(-1, 2)
      if boxr.min() < 0:
        continue
      if boxr[:, 0].max() > img.shape[1]:
        continue
      if boxr[:, 1].max() > img.shape[0]:
        continue
      '''
      if len(best_dict) > 0:
        detetcions_out.append([box, best_dict])
      detetcions_out.append([box, det_text])
      '''
      box = box.reshape(8)
      if args.is_icdar == 1:
        res_file.write('{},{},{},{},{},{},{},{},{},{}\r\n'.format(int(box[2]),int(box[3]), int(box[4]), int(box[5]), int(box[6]), int(box[7]), int(box[0]), int(box[1]), conf, det_text ))
      else:
        res_file.write('{},{},{},{},{},{},{},{},{},{}\r\n'.format(int(box[2]),int(box[3]), int(box[4]), int(box[5]), int(box[6]), int(box[7]), int(box[0]), int(box[1]), conf, lang)) # , det_text ))
        
      
    pix = np.array(pil_img)
    
    if args.evaluate == 1:
      tp, tp_e2e, gt_e2e, tp_e2e_ed1, detection_to_gt  = evaluate_image(pix, detetcions_out, gt_rect, gt_txts, eval_text_length=eval_text_length)
      tp_all += tp 
      gt_all += len(gt_txts)
      tp_e2e_all += tp_e2e
      gt_e2e_all += gt_e2e
      tp_e2e_ed1_all += tp_e2e_ed1
      detecitons_all += len(detetcions_out)
      
      pil_img = Image.fromarray(pix)
      pil_draw = ImageDraw.Draw(pil_img)
      
      det_no = 0
      for box, det_text in detetcions_out:
        
        width, height = pil_draw.textsize(det_text, font=font)
        box = box.reshape(8)
        center =  [box[2] + 3, box[3] - height - 2]
        
        draw_text = det_text
        try:
          if len(det_text) > 0 and 'ARABIC' in ud.name(det_text[0]):
            draw_text = det_text[::-1]
        except:
          pass
        
        pil_draw.text((center[0], center[1]), draw_text, fill = (0,255,0),font=font)
        if det_no in detection_to_gt:
          [gt_no, ratio, edit_dist] = detection_to_gt[det_no]
          if edit_dist > 0:
            center[0] += width + 5
            gt_text = gt_txts[gt_no]
            #pil_draw.text((center[0], center[1]), gt_text, fill = (255,0,0),font=font)
  
        det_no += 1
      pix = np.array(pil_img)
      
      print("  E2E recall {0:.3f} / {1:.3f} / {2:.3f}, precision: {3:.3f}".format( 
        tp_e2e_all / float( max(1, gt_e2e_all) ), 
        tp_all / float( max(1, gt_e2e_all )), 
        tp_e2e_ed1_all / float( max(1, gt_e2e_all )), 
        tp_all /  float( max(1, detecitons_all)) ))
      
      #pix = cv2.cvtColor(pix, cv2.COLOR_BGR2RGB)
      cv2.imwrite('preview/{1:.3f}_{0}'.format(base_nam, tp_e2e / float( max(1, gt_e2e) ) ), pix )
    else:
      cv2.imwrite('preview/{0}'.format(base_nam), pix)
    
    res_file.close()
      
    if im_no > 200:
      break
    im_no += 1
    if args.debug == 1:
      cv2.imshow('pix', pix)
      cv2.waitKey(0)
      
    
    
    
    
    
    
  
