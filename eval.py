'''
Created on Sep 3, 2017

@author: Michal.Busta at gmail.com
'''
import os

import torch
import numpy as np
import cv2

import net_utils

import argparse
import icdar

import time
import math

from icdar import draw_box_points
from ocr_test_utils import print_seq_ext

import lanms, locality_aware_nms

import torch.nn.functional as F
import editdistance

f = open('codec.txt', 'r')
codec = f.readlines()[0]
#codec = u' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~£ÁČĎÉĚÍŇÓŘŠŤÚŮÝŽáčďéěíňóřšťúůýž'
f.close()
print(len(codec))

buckets = [54, 80, 124, 182, 272, 410, 614, 922, 1383, 2212]


def intersect(a, b):
  '''Determine the intersection of two rectangles'''
  rect = (0,0,0,0)
  r0 = max(a[0],b[0])
  c0 = max(a[1],b[1])
  r1 = min(a[2],b[2])
  c1 = min(a[3],b[3])
  # Do we have a valid intersection?
  if r1 > r0 and  c1 > c0: 
      rect = (r0,c0,r1,c1)
  return rect

def union(a, b):
  r0 = min(a[0],b[0])
  c0 = min(a[1],b[1])
  r1 = max(a[2],b[2])
  c1 = max(a[3],b[3])
  return (r0,c0,r1,c1)

def area(a):
  '''Computes rectangle area'''
  width = a[2] - a[0]
  height = a[3] - a[1]
  return abs(width * height)


def evaluate_image(img, detections, gt_rect, gt_txts, iou_th=0.5, iou_th_vis=0.5, iou_th_eval=0.5, eval_text_length = 3):
    
  '''
  Summary : Returns end-to-end true-positives, detection true-positives, number of GT to be considered for eval (len > 2).
  Description : For each predicted bounding-box, comparision is made with each GT entry. Values of number of end-to-end true
                positives, number of detection true positives, number of GT entries to be considered for evaluation are computed.
  
  Parameters
  ----------
  iou_th_eval : float
      Threshold value of intersection-over-union used for evaluation of predicted bounding-boxes
  iou_th_vis : float
      Threshold value of intersection-over-union used for visualization when transciption is true but IoU is lesser.
  iou_th : float
      Threshold value of intersection-over-union between GT and prediction.
  word_gto : list of lists
      List of ground-truth bounding boxes along with transcription.
  batch : list of lists
      List containing data (input image, image file name, ground truth).
  detections : tuple of tuples
      Tuple of predicted bounding boxes along with transcriptions and text/no-text score.
  
  Returns
  -------
  tp : int
      Number of predicted bounding-boxes having IoU with GT greater than iou_th_eval.
  tp_e2e : int
      Number of predicted bounding-boxes having same transciption as GT and len > 2.
  gt_e2e : int
      Number of GT entries for which transcription len > 2.
  '''
  
  gt_to_detection = {}
  detection_to_gt = {}
  tp = 0
  tp_e2e = 0
  tp_e2e_ed1 = 0
  gt_e2e = 0
  
  gt_matches = np.zeros(gt_rect.shape[0])
  gt_matches_ed1 = np.zeros(gt_rect.shape[0])
  
  for i in range(0, len(detections)):
      
    det = detections[i]
    box =  det[0] # Predicted bounding-box parameters
    box = np.array(box, dtype="int") # Convert predicted bounding-box to numpy array
    box = box[0:8].reshape(4, 2)
    bbox = cv2.boundingRect(box)
    
    bbox = [bbox[0], bbox[1], bbox[2], bbox[3]]
    bbox[2] += bbox[0] # Convert width to right-coordinate
    bbox[3] += bbox[1] # Convert height to bottom-coordinate
    
    det_text = det[1] # Predicted transcription for bounding-box
    
    for gt_no in range(len(gt_rect)):
        
      gtbox = gt_rect[gt_no]
      txt = gt_txts[gt_no] # GT transcription for given GT bounding-box
      gtbox = np.array(gtbox, dtype="int")
      gtbox = gtbox[0:8].reshape(4, 2)
      rect_gt = cv2.boundingRect(gtbox)
      
      
      rect_gt = [rect_gt[0], rect_gt[1], rect_gt[2], rect_gt[3]]
      rect_gt[2] += rect_gt[0] # Convert GT width to right-coordinate
      rect_gt[3] += rect_gt[1] # Convert GT height to bottom-coordinate 

      inter = intersect(bbox, rect_gt) # Intersection of predicted and GT bounding-boxes
      uni = union(bbox, rect_gt) # Union of predicted and GT bounding-boxes
      ratio = area(inter) / float(area(uni)) # IoU measure between predicted and GT bounding-boxes
      
      # 1). Visualize the predicted-bounding box if IoU with GT is higher than IoU threshold (iou_th) (Always required)
      # 2). Visualize the predicted-bounding box if transcription matches the GT and condition 1. holds
      # 3). Visualize the predicted-bounding box if transcription matches and IoU with GT is less than iou_th_vis and 1. and 2. hold
      if ratio > iou_th:
        if not gt_no in gt_to_detection:
          gt_to_detection[gt_no] = [0, 0]
          
        edit_dist = editdistance.eval(det_text.lower(), txt.lower())
        if edit_dist <= 1:
          gt_matches_ed1[gt_no] = 1
          draw_box_points(img, box, color = (0, 128, 0), thickness=2)
            
        if edit_dist == 0: #det_text.lower().find(txt.lower()) != -1:
          draw_box_points(img, box, color = (0, 255, 0), thickness=2)
          gt_matches[gt_no] = 1 # Change this parameter to 1 when predicted transcription is correct.
          
          if ratio < iou_th_vis:
              #draw_box_points(draw, box, color = (255, 255, 255), thickness=2)
              #cv2.imshow('draw', draw) 
              #cv2.waitKey(0)
              pass
                
        tupl = gt_to_detection[gt_no] 
        if tupl[0] < ratio:
          tupl[0] = ratio 
          tupl[1] = i 
          detection_to_gt[i] = [gt_no, ratio, edit_dist]  
                  
  # Count the number of end-to-end and detection true-positives
  for gt_no in range(gt_matches.shape[0]):
    gt = gt_matches[gt_no]
    gt_ed1 = gt_matches_ed1[gt_no]
    txt = gt_txts[gt_no]
    
    gtbox = gt_rect[gt_no]
    gtbox = np.array(gtbox, dtype="int")
    gtbox = gtbox[0:8].reshape(4, 2)
    
    if len(txt) >= eval_text_length and not txt.startswith('##'):
      gt_e2e += 1
      if gt == 1:
        tp_e2e += 1
      if gt_ed1 == 1:
        tp_e2e_ed1 += 1
        
            
    if gt_no in gt_to_detection:
      tupl = gt_to_detection[gt_no] 
      if tupl[0] > iou_th_eval: # Increment detection true-positive, if IoU is greater than iou_th_eval
        if len(txt) >= eval_text_length and not txt.startswith('##'):
          tp += 1   
      #else:
      #  draw_box_points(img, gtbox, color = (255, 255, 255), thickness=2)
        
  for i in range(0, len(detections)):  
    det = detections[i]
    box =  det[0] # Predicted bounding-box parameters
    box = np.array(box, dtype="int") # Convert predicted bounding-box to numpy array
    box = box[0:8].reshape(4, 2)
    
    if not i in detection_to_gt:
      draw_box_points(img, box, color = (0, 0, 255), thickness=2)
    else:
      [gt_no, ratio, edit_dist] = detection_to_gt[i]
      if edit_dist > 0:
        draw_box_points(img, box, color = (255, 0, 0), thickness=2)
            
  #cv2.imshow('draw', draw)             
  return tp, tp_e2e, gt_e2e, tp_e2e_ed1, detection_to_gt 
        




def ocr_image(net, codec, img, norm_height, args):
          
  scale = norm_height / float(img.shape[0])
  ssf_y = norm_height / 32.0
  width = int(img.shape[1] * scale)
  
  best_diff = width
  bestb = 0
  for b in range(0, len(buckets)):
    if best_diff > abs(width * 1.3 - buckets[b] * ssf_y):
      best_diff = abs(width * 1.3 - buckets[b] * ssf_y)
      bestb = b

  width =  buckets[bestb] 
  
  scaled = cv2.resize(img, (int(buckets[bestb] * ssf_y), norm_height))  
  scaled = np.expand_dims(scaled, axis=2)
  scaled = np.expand_dims(scaled, axis=0)
  
  
  scaled_var = net_utils.np_to_variable(scaled, is_cuda=args.cuda, volatile=False).permute(0, 3, 1, 2)
  ctc_f = net(scaled_var)
  ctc_f = ctc_f.data.cpu().numpy()
  ctc_f = ctc_f.swapaxes(1, 2)
  
  labels = ctc_f.argmax(2)
  ind = np.unravel_index(labels, ctc_f.shape)
  confidence = np.mean( np.exp(ctc_f[ind]) )
  det_text, conf, dec_s = print_seq_ext(labels[0, :], codec)  
  
  return det_text, confidence, dec_s

def ocr_image2(net, codec, im_data, detection):
  
  boxo = detection
  boxr = boxo[0:8].reshape(-1, 2)
  
  center = (boxr[0, :] + boxr[1, :] + boxr[2, :] + boxr[3, :]) / 4
  
  dw = boxr[2, :] - boxr[1, :]
  dh =  boxr[1, :] - boxr[0, :]

  w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1])
  h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1])
  
  input_W = im_data.size(3)
  input_H = im_data.size(2)
  target_h = 40  
    
  scale = target_h / max(1, h) 
  target_gw = int(w * scale) + target_h 
  target_gw = max(2, target_gw // 32) * 32      
    
  xc = center[0] 
  yc = center[1] 
  w2 = w 
  h2 = h 
  
  angle = math.atan2((boxr[2][1] - boxr[1][1]), boxr[2][0] - boxr[1][0])
  
  #show pooled image in image layer

  scalex = (w2 + h2) / input_W * 1.2
  scaley = h2 / input_H * 1.3

  th11 =  scalex * math.cos(angle)
  th12 = -math.sin(angle) * scaley
  th13 =  (2 * xc - input_W - 1) / (input_W - 1) #* torch.cos(angle_var) - (2 * yc - input_H - 1) / (input_H - 1) * torch.sin(angle_var)
  
  th21 = math.sin(angle) * scalex 
  th22 =  scaley * math.cos(angle)  
  th23 =  (2 * yc - input_H - 1) / (input_H - 1) #* torch.cos(angle_var) + (2 * xc - input_W - 1) / (input_W - 1) * torch.sin(angle_var)
            
  t = np.asarray([th11, th12, th13, th21, th22, th23], dtype=np.float)
  t = torch.from_numpy(t).type(torch.FloatTensor)
  t = t.cuda()
  theta = t.view(-1, 2, 3)
  
  grid = F.affine_grid(theta, torch.Size((1, 3, int(target_h), int(target_gw))))
  
  
  x = F.grid_sample(im_data, grid)
  
  '''
  im = x.data.cpu().numpy()
  im = im.squeeze(0)
  im = im.swapaxes(0, 2)
  im = im.swapaxes(0, 1)
  cv2.imshow('im', im)
  cv2.waitKey(0)
  '''
  
  features = net.forward_features(x)
  labels_pred = net.forward_ocr(features)
  
  ctc_f = labels_pred.data.cpu().numpy()
  ctc_f = ctc_f.swapaxes(1, 2)

  labels = ctc_f.argmax(2)
  
  ind = np.unravel_index(labels, ctc_f.shape)
  conf = np.mean( np.exp(ctc_f[ind]) )
  
  det_text, conf2, dec_s = print_seq_ext(labels[0, :], codec)  
  
  return det_text, conf2, dec_s
  

def draw_detections(draw, draw2, iou_map, rbox, angle_pred, args, color=(1, 0, 0), scale_factor = 4):
  
  xy_text = np.argwhere(iou_map > args.score_map_thresh)
  if xy_text.shape[0] == 0:
    return [], draw, draw 
  polys = []
  ious = []
  for i in range(0, xy_text.shape[0]):
    pos = xy_text[i]
    angle_cos = angle_pred[1, pos[0], pos[1]]
    angle_sin = angle_pred[0, pos[0], pos[1]]
    #cv2.circle(draw, (pos[1] * scale_factor, pos[0] * scale_factor), 2, (0, 0, 255) )
    offset = rbox[pos[0], pos[1]]
    
    pos_g = np.array([(pos[1] - offset[0] * angle_sin) * scale_factor, (pos[0] - offset[0] * angle_cos) * scale_factor ])
    pos_g2 = np.array([ (pos[1] + offset[1] * angle_sin) * scale_factor, (pos[0] + offset[1] * angle_cos) * scale_factor ])
    
    cv2.circle(draw, (int(pos_g[0]), int(pos_g[1])), 2, (0, 255, 0) )
    cv2.circle(draw, (int(pos_g2[0]), int(pos_g2[1])), 2, (0, 255, 0) )
    
    pos_r = np.array([(pos[1] - offset[2] * angle_cos) * scale_factor, (pos[0] - offset[2] * angle_sin) * scale_factor ])
    pos_r2 = np.array([(pos[1] + offset[3] * angle_cos) * scale_factor, (pos[0] + offset[3] * angle_sin) * scale_factor ])
    
    cv2.circle(draw, (int(pos_r[0]), int(pos_r[1])), 2, ( 255, 0, 0) )
    cv2.circle(draw, (int(pos_r2[0]), int(pos_r2[1])), 2, (0, 0, 255) )
    
    

    center = (pos_g + pos_g2 + pos_r + pos_r2) /2 - [scale_factor*pos[1], scale_factor*pos[0]]
    #center = (pos_g + pos_g2 + pos_r + pos_r2) / 4
    dw = pos_r - pos_r2
    dh =  pos_g - pos_g2
    
    w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1])
    h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1])
    
    angle = math.atan2(angle_sin, angle_cos)
    
    rect = ( (center[0], center[1]), (w, h), angle * 180 / math.pi )
    pts = cv2.boxPoints(rect)
    
    polys.append(pts)
    ious.append(iou_map[pos[0], pos[1]])
    #draw_box_points(draw, pts)
    
  polys = np.asarray(polys)
  polys = polys.reshape(polys.shape[0], -1)
  ious = np.asarray(ious)
  ious = ious.reshape(ious.shape[0], 1)
  stacked = np.hstack((polys, ious))
  #boxes = locality_aware_nms.nms_locality(stacked.astype(np.float64), 0.3)
  boxes = lanms.merge_quadrangle_n9(stacked.astype(np.float64), 0.2)
  
  for i in range(0, boxes.shape[0]):
    pts = boxes[i]
    pts  = pts[0:8]
    pts = pts.reshape(4, -1)
    pts = np.asarray(pts, dtype=np.int)
    draw_box_points(draw2, pts, color=color, thickness=1)
    
  
  #cv2.imshow('orig', draw)
  #cv2.imshow('nms', draw2)
  
  return boxes, draw, draw2

def get_boxes(iou_map, rbox, angle_pred, args, scale_factor = 4):
  
  xy_text = np.argwhere(iou_map > args.score_map_thresh)
  if xy_text.shape[0] == 0:
    return [] 
  polys = []
  ious = []

  for i in range(0, xy_text.shape[0]):
    pos = xy_text[i]
    
    angle_cos = angle_pred[1, pos[0], pos[1]]
    angle_sin = angle_pred[0, pos[0], pos[1]]
    offset = rbox[pos[0], pos[1]]
    
    posp = pos + 0.125
    
    pos_g = np.array([(posp[1] - offset[0] * angle_sin) * scale_factor, (posp[0] - offset[0] * angle_cos) * scale_factor ])
    pos_g2 = np.array([ (posp[1] + offset[1] * angle_sin) * scale_factor, (posp[0] + offset[1] * angle_cos) * scale_factor ])
        
    pos_r = np.array([(posp[1] - offset[2] * angle_cos) * scale_factor, (posp[0] - offset[2] * angle_sin) * scale_factor ])
    pos_r2 = np.array([(posp[1] + offset[3] * angle_cos) * scale_factor, (posp[0] + offset[3] * angle_sin) * scale_factor ])
    
    center = (pos_g + pos_g2 + pos_r + pos_r2) / 2 - [scale_factor*pos[1], scale_factor*pos[0]]
    #center = (pos_g + pos_g2 + pos_r + pos_r2) / 4
    dw = pos_r - pos_r2
    dh =  pos_g - pos_g2
    
    w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1])
    h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1])
    
    angle = math.atan2(angle_sin, angle_cos)
    rect = ( (center[0], center[1]), (w, h), angle * 180 / math.pi )
    pts = cv2.boxPoints(rect)
    
    polys.append(pts)
    ious.append(iou_map[pos[0], pos[1]])
    #draw_box_points(draw, pts)
    
  polys = np.asarray(polys)
  polys = polys.reshape(polys.shape[0], -1)
  ious = np.asarray(ious)
  ious = ious.reshape(ious.shape[0], 1)
  stacked = np.hstack((polys, ious))
  #boxes = locality_aware_nms.nms_locality(stacked.astype(np.float64), 0.3)
  boxes = lanms.merge_quadrangle_n9(stacked.astype(np.float64), 0.3, 0.1)
  
  return boxes
  
def get_normalized_image(img, box):
  
  box2 = box[0:8].reshape(4, 2)
  
  dw = box2[2] - box2[1] 
  dh =  box2[0] - box2[1]
    
  w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1]) * 1.2
  h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1])
  center = box2.sum(0) / 4
  angle = math.atan2(dw[1], dw[0])  
  rr = ( (center[0], center[1]), (w, h), angle * 180 / math.pi )
  
  extbox = cv2.boundingRect(box2)

  if extbox[2] *  extbox[3] > img.shape[0] * img.shape[1]:
    print("Too big proposal: {0}x{1}".format(extbox[2], extbox[3]))
    return None, None
  extbox = [extbox[0], extbox[1], extbox[2], extbox[3]]
  extbox[2] += extbox[0]
  extbox[3] += extbox[1]
  extbox = np.array(extbox, np.int)
  
  extbox[0] = max(0, extbox[0])
  extbox[1] = max(0, extbox[1])
  extbox[2] = min(img.shape[1], extbox[2])
  extbox[3] = min(img.shape[0], extbox[3])
  
  tmp = img[extbox[1]:extbox[3], extbox[0]:extbox[2]]
  center = (tmp.shape[1] / 2,  tmp.shape[0] / 2)
  rot_mat = cv2.getRotationMatrix2D( center, rr[2], 1 )
  
  if tmp.shape[0] == 0 or tmp.shape[1] == 0:
    return None, rot_mat
  
  
  rot_mat[0,2] += rr[1][0] /2.0 - center[0]
  rot_mat[1,2] += rr[1][1] /2.0 - center[1]
  try:
    norm_line = cv2.warpAffine( tmp, rot_mat, (int(rr[1][0]), int(rr[1][1])), borderMode=cv2.BORDER_REPLICATE )
  except:
    return None, rot_mat
  return norm_line, rot_mat  
 

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 
          
if __name__ == '__main__': 
  
  parser = argparse.ArgumentParser()
  #parser.add_argument('-valid_list', default='/home/busta/data/ch8_validation_e2e/list.txt')
  parser.add_argument('-valid_list', default='/home/busta/data/CEMI/plzen/list.txt')
  parser.add_argument('-cuda', type=int, default=1)
  parser.add_argument('-model', default='SSTextResSep_661000.h5')
  parser.add_argument('-model_ocr', default='ICCV_OCRF_8859000.h5')
  parser.add_argument('-debug', default=1)
  parser.add_argument('-score_map_thresh', default=0.9)
  parser.add_argument('-small', type=int, default=0)
  
  parser.add_argument('-output_dir', default='valid')
  
  args = parser.parse_args()    
  
  net = ModelResNetSep(attention=True)
  net_utils.load_net(args.model, net)
  orc_net = OCRModel()
  net_utils.load_net(args.model_ocr, orc_net)
    
  if args.cuda:
    print('Using cuda ...')
    net = net.cuda()
    orc_net = orc_net.cuda()
  
  net = net.eval()
  orc_net = orc_net.eval()
  
  im_no = 0
  gen = icdar.generator(input_size=-1, batch_size=1, train_list = args.valid_list, vis=False, in_train = False)
  while True:
    batch = next(gen)
    if batch is None:
      break
    images, image_fns, score_maps, geo_maps, training_masks = batch
    if images is None:
      continue
    print(image_fns)
    
    im_data = net_utils.np_to_variable(images, is_cuda=args.cuda, volatile=True).permute(0, 3, 1, 2)
    start = time.time()
    iou_pred, rbox, angle_pred = net(im_data)
    end = time.time()
    seconds = end - start
    fps = 1 / seconds 
    print("loc fps:{0}".format(fps))
    
    im_o = cv2.imread(image_fns[0])       
      
    rbox = rbox.data.cpu()[0].numpy()
    rbox = rbox.swapaxes(0, 1)
    rbox = rbox.swapaxes(1, 2)
    
    iou = iou_pred.data.cpu()[0].numpy()
    iou = iou.squeeze(0)
    
    boxes, draw2 = draw_detections(images[0], iou, rbox, angle_pred.data.cpu()[0].numpy(), args)
    draw2 = np.asarray((draw2 + 1) * 128 , dtype=np.uint8)
    
    img = Image.fromarray(draw2)
    draw = ImageDraw.Draw(img)
    
    font = ImageFont.truetype("/home/busta/git/DS_CVPR/Arial-Unicode-Regular.ttf", 20)
    
    res_file = os.path.join(args.output_dir,  '{}.txt'.format(os.path.basename(image_fns[0]).split('.')[0]))
    res_file = open(res_file, 'w')
    
    scale_x = im_o.shape[1] / draw2.shape[1]
    scale_y = im_o.shape[0] / draw2.shape[0]
    
    for box in boxes:
      
      box_img, rot_mat = get_normalized_image(images[0], box)
      if box_img is None:
        print('Bad bounding box!')
        continue
      
      ocr_src = box_img.mean(2)
      
      det_text, conf, dec_s = ocr_image(orc_net, codec, ocr_src, 32, args)
      if len(det_text) == 0:
        continue
      
      
      width, height = draw.textsize(det_text, font=font)
      center =  [box[0], box[1]]
      draw.text((center[0], center[1]), det_text, fill = (0,255,0),font=font)
      
      
      res_file.write('{},{},{},{},{},{},{},{},{},{}\r\n'.format(box[2] * scale_x, box[3] * scale_y
                      , box[4] * scale_x, box[5] * scale_y, box[6] * scale_x, box[7] * scale_y, box[0] * scale_x, box[1] * scale_y, 1, det_text ))
      
      print(det_text)
      
    res_file.close()
    
    if True:
      cv2.imshow('iou_map', iou)
      cv2.imshow('score_maps', score_maps[0] * 255)
      cv2.imshow('img', images[0])
      pix = np.array(img)
      pix = cv2.cvtColor(pix, cv2.COLOR_BGR2RGB)
      cv2.imshow('pix', pix)
    #cv2.imwrite('/tmp/{0}'.format(os.path.basename(image_fns[0])), pix)
    cv2.waitKey(0)
    im_no += 1
    if im_no > 200:
      break
    
    
    
    
    
  
