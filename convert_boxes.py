### git @ yash0307 ###

import cv2
import os
import sys
import numpy as np

inp_list = open('/mnt/textspotter/software/yash_data/ICDAR_MLT/icdar_mlt_test.txt', 'r').read().splitlines()
out_dir = 'submission/'

def  get_txt_file_name(img_name):
  if '.png' in img_name:
    txt_file = img_name.replace('.png', '.txt')
  elif '.jpg' in img_name:
    txt_file = img_name.replace('.jpg', '.txt')
  elif '.jpeg' in img_name:
    txt_file = img_name.replace('.jpeg', '.txt')
  elif '.JPG' in img_name:
    txt_file = img_name.replace('.JPG', '.txt')
  elif '.gif' in img_name:
    txt_file = img_name.replace('.gif', '.txt')
  else:
    print(img_name)
  return '/home.guest/patelyas/deepsemantictext/1_c_0.8_0.3/gen/' + txt_file.replace('ts','res')

def draw_box_points(img, points, color = (0, 255, 0), thickness = 2):
  try:
    cv2.line(img, (points[0][0], points[0][1]), (points[1][0], points[1][1]), color, thickness)
    cv2.line(img, (points[2][0], points[2][1]), (points[1][0], points[1][1]), color, thickness)
    cv2.line(img, (points[2][0], points[2][1]), (points[3][0], points[3][1]), color, thickness)
    cv2.line(img, (points[0][0], points[0][1]), (points[3][0], points[3][1]), color, thickness)
  except:
    print('HERE')
    pass

def converted_points(im, box):
	image_size = [(im.shape[1]*1.5) // 32 * 32, (im.shape[0]*1.5) // 32 * 32]
	resize_h = int(image_size[1])
	resize_w = int(image_size[0])
	original_h = im.shape[0]
	original_w = im.shape[1]
	org_box = []
	for i in range(0,len(box)):
		org_box.append([round(box[i][0]*(original_w/resize_w)) , round(box[i][1]*(original_h/resize_h))])
	return org_box

def resize_img(im):
	image_size = [(im.shape[1]*1.5)// 32 * 32, (im.shape[0]*1.5) // 32 * 32]
	resize_h = int(image_size[1])
	resize_w = int(image_size[0])
	im_re = np.copy(im)
	im_re = cv2.resize(im_re, dsize=(resize_w, resize_h))
	return im_re

for given_img in inp_list:
	img_name = given_img.split('/')
	img_name = img_name[7]
	print(img_name)
	txt_file = get_txt_file_name(img_name)
	out_file = open(txt_file.replace('gen','submission_2'), 'w')
	img = cv2.imread(given_img)
	if img is None:
		continue
	if not os.path.isfile(txt_file):
		continue
	boxes = open(txt_file,'r').read().splitlines()
	new_boxes = []
	draw = np.copy(img)
	
	for given_box in boxes:
		box = [int(i) for i in given_box.split(',')[:8]]
		prob = given_box.split(',')[8]
		box = [[box[0], box[1]], [box[2], box[3]], [box[4], box[5]], [box[6], box[7]]]
		pts = converted_points(img,box)
		string_to_write = str(pts[0][0]) + ','+str(pts[0][1]) + ','+str(pts[1][0]) + ','+str(pts[1][1]) + ','+str(pts[2][0]) + ','+str(pts[2][1]) +','+ str(pts[3][0]) + ','+str(pts[3][1]) + ','+str(prob)+'\n'
		out_file.write(string_to_write)
		#draw_box_points(draw, pts)
	#cv2.imshow('img', cv2.resize(draw, (288,288)))
	#cv2.waitKey(50)
	out_file.close()
