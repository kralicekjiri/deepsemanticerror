# coding:utf-8
import csv
import cv2
import time
import os
import numpy as np
import random

from data_util import GeneratorEnqueuer

buckets = [64, 128, 256, 512, 1024, 2048] 

import unicodedata as ud

f = open('codec.txt', 'r')
codec = f.readlines()[0]
codec_rev = {}
index = 4
for i in range(0, len(codec)):
  codec_rev[codec[i]] = index
  index += 1

def get_images(data_path):
  
  base_dir = os.path.dirname(data_path)
  files_out = []
  cnt = 0
  with open(data_path) as f:
    while True:
      line = f.readline()
      if not line: 
        break
      line = line.strip()
      if len(line) == 0:
        continue
      if not line[0] == '/':
        line = '{0}/{1}'.format(base_dir, line)
      files_out.append(line)  
      cnt +=1
      #if cnt > 100:
      #  break
  return files_out



def generator(batch_size=4, train_list='/home/klara/klara/home/DeepSemanticText/resources/ims2.txt', in_train=True, rgb = False, norm_height = 32):
  image_list = np.array(get_images(train_list))
  print('{} training images in {}'.format(image_list.shape[0], train_list))
  index = np.arange(0, image_list.shape[0])
  
  batch_sizes = []
  cb = batch_size
  for i in range(0, len(buckets)):
    batch_sizes.append(cb)
    if i % 10 == 0 and  cb > 2:
      cb /= 2
      
  
  bucket_images = []
  bucket_labels = []
  bucket_label_len = []
  bucket_labels_low = []
  
  for b in range(0, len(buckets)):
    bucket_images.append([])
    bucket_labels.append([])
    bucket_label_len.append([])
    bucket_labels_low.append([])
  
  while True:
    if in_train:
      np.random.shuffle(index)
  
    for i in index:
      try:
        image_name = image_list[0]
        if random.uniform(0, 100) < 50:
          image_name = image_list[int(random.uniform(0, min(3000000,image_list.shape[0] - 1)))]
        image_name = image_list[ int(random.uniform(0, 300))]
        
        #if len(images) == 0:
        #  im_name = '/home/busta/data/Amazon/B000FC0URG.jpg' 
        #im_name = '/home/busta/data/SynthText/Amazon/B009F2PQY4.jpg'
        
        spl = image_name.split(" ")
        if len(spl) == 1:
          spl = image_name.split(",")
        image_name = spl[0].strip()
        gt_txt = ''
        if len(spl) > 1:
          gt_txt = ""
          delim = ""
          for k in range(1, len(spl)):
            gt_txt += delim + spl[k]
            delim =" "
          if len(gt_txt) > 1 and gt_txt[0] == '"' and gt_txt[-1] == '"':
              gt_txt = gt_txt[1:-1]
        
        if len(gt_txt)  == 0:
          continue
              

        if image_name[len(image_name) - 1] == ',':
          image_name = image_name[0:-1]    
        
        if not os.path.exists(image_name):
          continue
        
        if rgb:
          im = cv2.imread(image_name)
        else:
          im = cv2.imread(image_name, cv2.IMREAD_GRAYSCALE)
        if im is None:
          continue
        
        scale = norm_height / float(im.shape[0])
        ccf_y = norm_height / 32.0
        width = int(im.shape[1] * scale)
        #cv2.imshow('im0', im)
            
              
        width =  256
        if im.shape[1] > width:
          im = cv2.resize(im, (width, int(width / im.shape[1] * im.shape[0])) )
          
        if im.shape[0] > width:
          im = cv2.resize(im, (int(width / im.shape[0] * im.shape[1]), width ))
      
        exp_h = abs(width - im.shape[0] )
        exp_w = abs(width - im.shape[1] )
        
        if random.uniform(0, 100) < 50:
          im = cv2.copyMakeBorder(im, exp_h // 2, exp_h // 2, exp_w // 2 , exp_w // 2 ,cv2.BORDER_REPLICATE)
        else:
          im = cv2.copyMakeBorder(im, exp_h // 2, exp_h // 2, exp_w // 2 , exp_w // 2 ,cv2.BORDER_CONSTANT, value=(int(np.mean(im[:, 0])), int(np.mean(im[:, 1])), int(np.mean(im[:, 3])))) 
        
        best_diff = width
        bestb = 0
        for b in range(0, len(buckets)):
          if best_diff > abs(width * 1.3 - buckets[b] * ccf_y):
            best_diff = abs(width * 1.3 * ccf_y - buckets[b] * ccf_y)
            bestb = b
            
        width =  buckets[bestb]
            
        im = cv2.resize(im, (width, width) )
        
        
        #cv2.imshow('im', im)
        #cv2.waitKey(0)
               
        if not rgb:
          im = im.reshape(im.shape[0],im.shape[1], 1) 
        
        if in_train:
          if random.randint(0, 100) < 10:
            im = np.invert(im)
          if random.randint(0, 100) < 20:
            im = cv2.blur(im,(5,5))
            if not rgb:
              im = im.reshape(im.shape[0],im.shape[1], 1) 
              
        
        bucket_images[bestb].append(im[:, :, ::-1].astype(np.float32))
        
        gt_labels = []
        gt_labels_lower = []
        gt_txt_low = gt_txt.lower()
        for k in range(len(gt_txt)):
          if gt_txt[k] in codec_rev:                
            gt_labels.append( codec_rev[gt_txt[k]] )
            gt_labels_lower.append( codec_rev[gt_txt_low[k]] )
          else:
            print('Unknown char: {0}'.format(gt_txt[k]) )
            gt_labels.append( 3 )
            gt_labels_lower.append( 3 )
            
        if 'ARABIC' in ud.name(gt_txt[0]):
          gt_labels = gt_labels[::-1]
          gt_labels_lower = gt_labels_lower[::-1]
          
    
        bucket_labels[bestb].append(gt_labels)
        bucket_labels_low[bestb].append(gt_labels_lower)
        bucket_label_len[bestb].append(len(gt_labels))
        
        if len(bucket_images[bestb]) == batch_sizes[bestb]:
          images = np.asarray(bucket_images[bestb], dtype=np.float)
          images /= 128
          images -= 1

          yield images, bucket_labels[bestb], bucket_label_len[bestb], bucket_labels_low[bestb]
          bucket_images[bestb] = []
          bucket_labels[bestb] = []
          bucket_label_len[bestb]  = []
          bucket_labels_low[bestb] = []
          
      except Exception as e:
        import traceback
        traceback.print_exc()
        continue
      
    if not in_train:
      print("finish")
      yield None
      break    


def get_batch(num_workers, **kwargs):
  try:
    enqueuer = GeneratorEnqueuer(generator(**kwargs), use_multiprocessing=False)
    enqueuer.start(max_queue_size=24, workers=num_workers)
    generator_output = None
    while True:
      while enqueuer.is_running():
        if not enqueuer.queue.empty():
          generator_output = enqueuer.queue.get()
          break
        else:
          time.sleep(0.01)
      yield generator_output
      generator_output = None
  finally:
    if enqueuer is not None:
      enqueuer.stop()

if __name__ == '__main__':
  
  data_generator = get_batch(num_workers=1, batch_size=1)
  while True:
    data = next(data_generator)
  
