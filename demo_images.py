'''
Created on Aug 25, 2017

@author: busta
'''

import cv2
import numpy as np
import os

import logging
import time
from eval import draw_detections

from models import ModelResNetSep2, OCRModel
import net_utils

from eval import get_normalized_image

from eval import ocr_image, ocr_image2

import argparse

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 
from matplotlib.scale import scale_factory

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

f = open('codec.txt', 'r')
codec = f.readlines()[0]
#codec = u' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~£ÁČĎÉĚÍŇÓŘŠŤÚŮÝŽáčďéěíňóřšťúůýž'
f.close()

def resize_image(im, max_size = 1585152, scale_up=True):
  
  if scale_up:
    image_size = [im.shape[1] * 6 // 32 * 32, im.shape[0] * 6 // 32 * 32]
  else:
    image_size = [im.shape[1] // 32 * 32, im.shape[0] // 32 * 32]
  while image_size[0] * image_size[1] > max_size:
    image_size[0] /= 1.2        
    image_size[1] /= 1.2   
    image_size[0] = int(image_size[0] // 32) * 32
    image_size[1] = int(image_size[1] // 32) * 32
      
   
  resize_h = int(image_size[1])
  resize_w = int(image_size[0])
              
  
  scaled = cv2.resize(im, dsize=(resize_w, resize_h))
  return scaled, (resize_h, resize_w)

def resize_image_up(im):

  image_size = [im.shape[1] // 32 * 32, im.shape[0] // 32 * 32] 
  while image_size[0] * image_size[1] < 1024 * 1024:
    image_size[0] *= 1.2        
    image_size[1] *= 1.2  
    image_size[0] = int(image_size[0] // 32) * 32
    image_size[1] = int(image_size[1] // 32) * 32
      
  resize_h = int(image_size[1])
  resize_w = int(image_size[0])
              
  
  scaled = cv2.resize(im, dsize=(resize_w, resize_h))
  return scaled, (resize_h, resize_w)
  

def draw_illu(illu, rst):
  for t in rst['text_lines']:
    d = np.array([t['x0'], t['y0'], t['x1'], t['y1'], t['x2'],
                  t['y2'], t['x3'], t['y3']], dtype='int32')
    d = d.reshape(-1, 2)
    cv2.polylines(illu, [d], isClosed=True, color=(255, 255, 0))
  return illu

if __name__ == '__main__':
  
  parser = argparse.ArgumentParser()
  parser.add_argument('-cuda', type=int, default=1)
  parser.add_argument('-model', default='/opt/ocr/care_labels/SemanticTexte2e_143000.h5')
  #parser.add_argument('-model', default='SSTextResSep50_185000.h5')
  parser.add_argument('-debug', default=0)
  parser.add_argument('-score_map_thresh', default=0.9)
  
  font2 = ImageFont.truetype("/opt/ocr/care_labels/Deepsemantictext/Arial-Unicode-Regular.ttf", 18)
  
  args = parser.parse_args()
  
  
  print("Using resnet Sep")
  net = ModelResNetSep2()
  
  
  net_utils.load_net(args.model, net)
  net = net.eval()
  
  if args.cuda:
    print('Using cuda ...')
    net = net.cuda()
     
  #cap = cv2.VideoCapture('/mnt/textspotter/evaluation-sets/icdar2013-video-Test/Video_35_2_3.mp4')

  carelabel_list_path = '/opt/ocr/care_labels/carelabels_list.txt'

  carelabels = []
  with open(carelabel_list_path, "r") as crlbs:
    for carelabel in crlbs:
      print(carelabel)
      #print(carelabel.replace('../carelabels_annotated/', ''))
      carelabels.append(carelabel.strip())
      
      #if len(images) > 1000:
      #  break


  print(len(carelabels))

#  image_path = "/opt/ocr/care_labels/carelabels_annotated/GP_1a.jpg"
  #im = cv2.imread(image_path)

  ##cap = cv2.VideoCapture('/opt/ocr/e2e/data/Video_35_2_3.mp4')
#  cap = cv2.VideoCapture(0)
  #cap.set(3,1024)
  #cap.set(4,800)
  ##cap.set(cv2.CAP_PROP_AUTOFOCUS, 1)
  ##ret, im = cap.read()
  
  #cv2.namedWindow("img", cv2.WND_PROP_FULLSCREEN)          
 

  ##print("Hodnota")
  ##print(ret) 

  ret = True
  frame_no = 0
  i = 0
  ##while ret:
  while i < len(carelabels):
    ##ret, im = cap.read()
    im = cv2.imread(carelabels[i]) 
    if ret==True:
      im_resized, (ratio_h, ratio_w) = resize_image(im, scale_up=False)
      images = np.asarray([im_resized], dtype=np.float)
      images /= 128
      images -= 1 
      im_data = net_utils.np_to_variable(images, is_cuda=args.cuda, volatile=True).permute(0, 3, 1, 2)
      start = time.time()
      iou_pred, rboxs, angle_pred, features = net(im_data)
      end = time.time()
      seconds = end - start
      fps = 1 / seconds 
      rbox = rboxs.data.cpu()[0].numpy()
      rbox = rbox.swapaxes(0, 1)
      rbox = rbox.swapaxes(1, 2)
    
      
      if True:
        iou = iou_pred.data.cpu()[0].numpy()
        iou = iou.squeeze(0)
        
        draw = np.copy(im_resized)
        draw2 = np.copy(im_resized)
#        cv2.imwrite('image.png', draw)       
 
        boxes, _, _ = draw_detections(draw, draw2, iou, rbox, angle_pred.data.cpu()[0].numpy(), args, color=(255, 0, 0))
        
#        cv2.imwrite('image_detiection.png', draw2)
        
 
        cv2.imshow('boxes', draw)
        img = Image.fromarray(draw2)
        draw = ImageDraw.Draw(img)
       # cv2.imwrite('image.png', draw)      
  
        if len(boxes) > 10:
          boxes = boxes[0:10]
        
        
        for box in boxes:
          if True:
            det_text, conf, dec_s = ocr_image2(net, codec, im_data, box)
          
            if len(det_text) == 0:
              continue
          
          
          width, height = draw.textsize(det_text, font=font2)
          center =  [box[0], box[1]]
          draw.text((center[0], center[1]), det_text, fill = (0,255,0),font=font2)
          #res = np.copy(draw)
          #asijko = Image.fromarray(res) 
          #cv2.imwrite('draw.png', asijko)
#          img_labeled_save = "results_startPoint/" + carelabels[i].replace('.jpg', '_result.jpg')
#          img.save(img_labeled_save)       


          #substring =
          sub = (carelabels[i].replace('../carelabels_annotated/', ''))
          substring = (sub.replace('.jpg', '.txt'))
          print("substring: %s" % substring)
          fout_subpath='/opt/ocr/care_labels/results/startPoint/'
          fout_path = fout_subpath + substring 
          print("fout_path")
          print(fout_path)
          print("------------")
          fout = open(fout_path, 'a')
          fout.write(det_text + "\n")
          fout.close()
          print(det_text)
 
          img_labeled_save = fout_subpath + sub
          print(img_labeled_save)
          #time.sleep(10)
          img_labeled_save=''.join(img_labeled_save.split())
          img.save(img_labeled_save)

          
        cv2.imshow('iou', iou)
        cv2.imwrite('iou.png', iou)

      draw.text((10, 10), 'FPS: {0:.2f}'.format(fps),(0,255,0),font=font2) 
       
      
      #draw_illu(im, rst)
      im = np.array(img)
      #im = cv2.resize(im, (im.shape[1] * 2, im.shape[0] * 2))   
      cv2.imshow('img', im)
      k = cv2.waitKey(10)
      print('k')
      print(k)
      if k == ord('w'):
        cv2.imwrite(draw, '/tmp/out.png')
      cv2.imwrite('image.png', im)
 
      #time.sleep(1)
      i += 1
      print("i")
      print(i) 
