'''
Created on Mar 9, 2018

@author: Michal.Busta at gmail.com
'''

def normalize_text(text_line):
  text_line = text_line.replace("β", 'ß')
  text_line = text_line.replace("Ａ", 'A')
  text_line = text_line.replace("：", ':')
  text_line = text_line.replace("Ｃ", 'C')
  text_line = text_line.replace("Ｂ", 'B')
  text_line = text_line.replace("Ｄ", 'D')
  text_line = text_line.replace("Ｅ", 'E')
  text_line = text_line.replace("Ｆ", 'F')
  text_line = text_line.replace("Ｇ", 'G')
  text_line = text_line.replace("Ｈ", 'H')
  text_line = text_line.replace("Ｉ", 'I')
  text_line = text_line.replace("Ｊ", 'J')
  text_line = text_line.replace("Ｋ", 'K')
  text_line = text_line.replace("Ｌ", 'L')
  text_line = text_line.replace("Ｍ", 'M')
  text_line = text_line.replace("Ｎ", 'N')
  text_line = text_line.replace("Ｏ", 'O')
  text_line = text_line.replace("Ｐ", 'P')
  text_line = text_line.replace("Ｒ", 'R')
  text_line = text_line.replace("Ｓ", 'S')
  text_line = text_line.replace("Ｔ", 'T')
  text_line = text_line.replace("Ｕ", 'U')
  text_line = text_line.replace("Ｖ", 'V')
  text_line = text_line.replace("Ｗ", 'W')
  text_line = text_line.replace("Ｘ", 'X')
  text_line = text_line.replace("Ｙ", 'Y')
  text_line = text_line.replace("Ｚ", 'Z')
  
  text_line = text_line.replace("０", '0')
  text_line = text_line.replace("１", '1')
  text_line = text_line.replace("２", '2')
  text_line = text_line.replace("３", '3')
  text_line = text_line.replace("４", '4')
  text_line = text_line.replace("５", '5')
  text_line = text_line.replace("６", '6')
  text_line = text_line.replace("７", '7')
  text_line = text_line.replace("８", '8')
  text_line = text_line.replace("９", '9')
  return text_line