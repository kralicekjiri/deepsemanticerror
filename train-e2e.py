'''
Created on Sep 3, 2017

@author: Michal.Busta at gmail.com
'''

import torch, sys, os
from torch.optim import SGD
import numpy as np
import cv2

import net_utils
import icdar
from icdar import draw_box_points
import timeit

import math
import random

from models import ModelResNetSep50, box2affine, ModelResNetSep2
import torch.autograd as autograd
import torch.nn.functional as F

from warpctc_pytorch import CTCLoss
from ocr_test_utils import print_seq_ext

import unicodedata as ud

base_lr = 0.0001
lr_decay = 0.99
momentum = 0.9
weight_decay = 0.0005
batch_per_epoch = 1000
disp_interval = 10

f = open('codec.txt', 'r')
codec = f.readlines()[0]
#codec = u' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~£ÁČĎÉĚÍŇÓŘŠŤÚŮÝŽáčďéěíňóřšťúůýž'
codec_rev = {}
index = 4
for i in range(0, len(codec)):
  codec_rev[codec[i]] = index
  index += 1
f.close()

def intersect(a, b):
  '''Determine the intersection of two rectangles'''
  rect = (0,0,0,0)
  r0 = max(a[0],b[0])
  c0 = max(a[1],b[1])
  r1 = min(a[2],b[2])
  c1 = min(a[3],b[3])
  # Do we have a valid intersection?
  if r1 > r0 and  c1 > c0: 
      rect = (r0,c0,r1,c1)
  return rect

def union(a, b):
  r0 = min(a[0],b[0])
  c0 = min(a[1],b[1])
  r1 = max(a[2],b[2])
  c1 = max(a[3],b[3])
  return (r0,c0,r1,c1)

def area(a):
  '''Computes rectangle area'''
  width = a[2] - a[0]
  height = a[3] - a[1]
  return width * height


def validate(list_file, net):
  
  net = net.eval()
  
  dir_name = os.path.dirname(list_file)
  images = []
  with open(list_file, "r") as ins:
    for line in ins:
      images.append(line.strip())
      
  for im_name in images:
    name = os.path.basename(im_name)
    name = name[:-4]  

    # print im_fn
    h, w, _ = im.shape
    txt_fn = im_name.replace(os.path.basename(im_name).split('.')[1], 'txt')
    if not os.path.exists(txt_fn) and not allow_empty:
      continue
    
    text_polys, text_tags, labels_txt = load_annoataion(txt_fn)
    
    im = cv2.imread(im_name) 
    
    
    im_data = net_utils.np_to_variable(images, is_cuda=opts.cuda, volatile=False).permute(0, 3, 1, 2)
    start = timeit.timeit()
    iou_pred, roi_pred, angle_pred, features = net(im_data)
    end = timeit.timeit()
    
    
  
  net = net.train()
  

def process_boxes(images, im_data, iou_pred, roi_pred, angle_pred, score_maps, score_maps_full, gt_idxs, gtso, lbso, features, net, ctc_loss, debug = False):
  
  for bid in range(iou_pred.size(0)):
    iou_pred_np = iou_pred[bid].data.cpu().numpy()
    iou_map = score_maps_full[bid]
    to_walk = iou_pred_np.squeeze(0) * iou_map.squeeze(2) * (iou_pred_np.squeeze(0) > 0.5)
    
    angle_p_bid = angle_pred[bid].data.cpu().numpy()[0]
    roi_p_bid = roi_pred[bid].data.cpu().numpy()
    gt_idx = gt_idxs[bid]
    gts = gtso[bid]
    lbs = lbso[bid]
    img = images[bid]
    img += 1
    img *= 128
    img = np.asarray(img, dtype=np.uint8)
    ctc_loss_val = 0
    ctc_loss_count = 0
    
    xy_text = np.argwhere(to_walk > 0)
    
    random.shuffle(xy_text)
    xy_text = xy_text[0:min(xy_text.shape[0], 500)]
    loss = None
    gt_proc = 0
    gt_good = 0
    
    gts_count = {}
    
    for i in range(0, xy_text.shape[0]):
      pos = xy_text[i, :]
      
      gt_id = gt_idx[pos[0], pos[1]]
      
      if not gt_id in gts_count:
        gts_count[gt_id] = 0
        
      if gts_count[gt_id] > 2:
        continue
      
      gt = gts[gt_id]
      gt_txt = lbs[gt_id]
      gt_txt_low = gt_txt.lower()
      if gt_txt.startswith('##'):
        continue
      
      angle = angle_p_bid[pos[0], pos[1]]
      angle_gt = ( math.atan2((gt[2][1] - gt[1][1]), gt[2][0] - gt[1][0]) + math.atan2((gt[3][1] - gt[0][1]), gt[3][0] - gt[0][0]) ) / 2
      
      if math.fabs(angle_gt - angle) > math.pi / 8:
        continue
      
      offset = roi_p_bid[:, pos[0], pos[1]]
      pos_g = np.array([(pos[1] - offset[0] * math.sin(angle)) * 4, (pos[0] - offset[0] * math.cos(angle)) * 4 ])
      pos_g2 = np.array([ (pos[1] + offset[1] * math.sin(angle)) * 4, (pos[0] + offset[1] * math.cos(angle)) * 4 ])
    
      pos_r = np.array([(pos[1] - offset[2] * math.cos(angle)) * 4, (pos[0] - offset[2] * math.sin(angle)) * 4 ])
      pos_r2 = np.array([(pos[1] + offset[3] * math.cos(angle)) * 4, (pos[0] + offset[3] * math.sin(angle)) * 4 ])
    
      center = (pos_g + pos_g2 + pos_r + pos_r2) / 4
      dw = pos_r - pos_r2
      dh =  pos_g - pos_g2
    
      w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1])
      h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1])
      
      dhgt =  gt[1] - gt[0]
      
      h_gt = math.sqrt(dhgt[0] * dhgt[0] + dhgt[1] * dhgt[1])
      if h_gt < 12:
        continue
    
      rect = ( (center[0], center[1]), (w, h), angle * 180 / math.pi )
      pts = cv2.boxPoints(rect)
      
      pred_bbox = cv2.boundingRect(pts)
      pred_bbox = [pred_bbox[0], pred_bbox[1], pred_bbox[2], pred_bbox[3]]
      pred_bbox[2] += pred_bbox[0]
      pred_bbox[3] += pred_bbox[1]
      
      if gt[:, 0].max() > im_data.size(3) or gt[:, 1].max() > im_data.size(3):
        continue 
      
      gt_bbox = [gt[:, 0].min(), gt[:, 1].min(), gt[:, 0].max(), gt[:, 1].max()]
      inter = intersect(pred_bbox, gt_bbox)
      
      uni = union(pred_bbox, gt_bbox)
      ratio = area(inter) / float(area(uni)) 
      
      if ratio < 0.8:
        continue
      
      hratio = min(h, h_gt) / max(h, h_gt)
      if hratio < 0.4:
        continue
      
      
      if debug:
        
        input_W = im_data.size(3)
        input_H = im_data.size(2)
        target_h = 32  
        
        scale = target_h / h 
        target_gw = int(w * scale) + target_h 
        
        angle_var = angle_pred[bid, 0, pos[0], pos[1]]
      
        
        xc =  - roi_pred[bid, 0, pos[0], pos[1]] * torch.sin(angle_var) + float(pos[1]) + roi_pred[bid, 1, pos[0], pos[1]] * torch.sin(angle_var) + float(pos[1]) \
           - roi_pred[bid, 2, pos[0], pos[1]] * torch.cos(angle_var) + float(pos[1]) + roi_pred[bid, 3, pos[0], pos[1]] * torch.cos(angle_var) + float(pos[1])
        
          
        yc = - roi_pred[bid, 0, pos[0], pos[1]] * torch.cos(angle_var) + float(pos[0]) + roi_pred[bid, 1, pos[0], pos[1]] * torch.cos(angle_var) + float(pos[0]) + \
          - roi_pred[bid, 2, pos[0], pos[1]] * torch.sin(angle_var) + float(pos[0]) + roi_pred[bid, 3, pos[0], pos[1]] * torch.sin(angle_var) + float(pos[0])
        w2 = (roi_pred[bid, 2, pos[0], pos[1]] + roi_pred[bid, 3, pos[0], pos[1]] ) * 4
        h2 = (roi_pred[bid, 0, pos[0], pos[1]] + roi_pred[bid, 1, pos[0], pos[1]] ) * 4 
        
        #show pooled image in image layer
      
        scalex = w2 / input_W
        scaley = h2 / input_H
      
        th11 =  scalex * torch.cos(angle_var)
        th12 = -torch.sin(angle_var) * scaley
        th13 =  (2 * xc - input_W - 1) / (input_W - 1) #* torch.cos(angle_var) - (2 * yc - input_H - 1) / (input_H - 1) * torch.sin(angle_var)
        
        th21 = torch.sin(angle_var) * scalex 
        th22 =  scaley * torch.cos(angle_var)  
        th23 =  (2 * yc - input_H - 1) / (input_H - 1) #* torch.cos(angle_var) + (2 * xc - input_W - 1) / (input_W - 1) * torch.sin(angle_var)
        
        
        t = torch.stack((th11, th12, th13, th21, th22, th23), dim=1)
        theta = t.view(-1, 2, 3)
        
        grid = F.affine_grid(theta, torch.Size((1, 3, int(target_h), int(target_gw))))
        
        imv = im_data[bid]
        
        x = F.grid_sample(im_data[bid].unsqueeze(0), grid)
        
        x_data = x.data.cpu().numpy()[0]
        x_data = x_data.swapaxes(0, 2)
        x_data = x_data.swapaxes(0, 1)
        
        x_data += 1
        x_data *= 128
        x_data = np.asarray(x_data, dtype=np.uint8)
        x_data = x_data[:, :, ::-1]
        
        cv2.circle(img, (int(xc[0].data.cpu()[0]), int(yc[0].data.cpu()[0])), 5, (0, 255, 0))      
        cv2.imshow('im_data', x_data)
        
        draw_box_points(img, pts)
        draw_box_points(img, gt, color=(0, 0, 255))
        
        cv2.imshow('img', img)
        cv2.waitKey(100)
        
      input_W = features.size(3)
      input_H = features.size(2)
      target_h = 32  
      
      scale = target_h / h 
      target_gw = int(w * scale) + target_h 
      
      
      angle_var = angle_pred[bid, 0, pos[0], pos[1]]
    
      
      xc =  - roi_pred[bid, 0, pos[0], pos[1]] * torch.sin(angle_var) + float(pos[1]) + roi_pred[bid, 1, pos[0], pos[1]] * torch.sin(angle_var) + float(pos[1]) \
         - roi_pred[bid, 2, pos[0], pos[1]] * torch.cos(angle_var) + float(pos[1]) + roi_pred[bid, 3, pos[0], pos[1]] * torch.cos(angle_var) + float(pos[1])
      
        
      yc = - roi_pred[bid, 0, pos[0], pos[1]] * torch.cos(angle_var) + float(pos[0]) + roi_pred[bid, 1, pos[0], pos[1]] * torch.cos(angle_var) + float(pos[0]) + \
        - roi_pred[bid, 2, pos[0], pos[1]] * torch.sin(angle_var) + float(pos[0]) + roi_pred[bid, 3, pos[0], pos[1]] * torch.sin(angle_var) + float(pos[0])
      w2 = (roi_pred[bid, 2, pos[0], pos[1]] + roi_pred[bid, 3, pos[0], pos[1]] ) * 4
      h2 = (roi_pred[bid, 0, pos[0], pos[1]] + roi_pred[bid, 1, pos[0], pos[1]] ) * 4 
        
      #scale original coords to feature space
      xc = xc / 4
      yc = yc / 4
      w2 = w2 / 4
      h2 = h2 / 4 
        
      scalex = w2 / (input_W)
      scaley = h2 / (input_H)
    
      th11 =  scalex * torch.cos(angle_var)
      th12 = -torch.sin(angle_var) * scaley
      th13 =  (2 * xc - input_W - 1) / (input_W - 1) 
      
      th21 = torch.sin(angle_var) * scalex 
      th22 =  scaley * torch.cos(angle_var)  
      th23 =  (2 * yc - input_H - 1) / (input_H - 1)  
      
      
      t = torch.stack((th11, th12, th13, th21, th22, th23), dim=1)
      theta = t.view(-1, 2, 3)
      
      target_h = 8
      target_gw = int(w / h * target_h)
      if target_gw < 8:
        target_gw = 8
      grid = F.affine_grid(theta, torch.Size((1, 3, int(target_h), int(target_gw))))
      
      
      x = F.grid_sample(features[bid].unsqueeze(0), grid)
      #score_sampled = F.grid_sample(iou_pred[bid].unsqueeze(0), grid)
      
      gt_labels = []
      gt_labels_low = []
      for k in range(len(gt_txt)):
        if gt_txt[k] in codec_rev:                
          gt_labels.append( codec_rev[gt_txt[k]] )
          gt_labels_low.append( codec_rev[gt_txt_low[k]] )
        else:
          print('Unknown char: {0}'.format(gt_txt[k]) )
          gt_labels.append( 3 )
          gt_labels_low.append( 3 )
          
      if 'ARABIC' in ud.name(gt_txt[0]):
          gt_labels = gt_labels[::-1]
      
      labels_pred = net.forward_ocr(x)
      
      label_length = []
      label_length.append(len(gt_labels))
      probs_sizes =  autograd.Variable(torch.IntTensor( [(labels_pred.permute(2,0,1).size()[0])] * (labels_pred.permute(2,0,1).size()[1]) ))
      label_sizes = autograd.Variable(torch.IntTensor( torch.from_numpy(np.array(label_length)).int() ))
      labels = autograd.Variable(torch.IntTensor( torch.from_numpy(np.array(gt_labels)).int() ))
      labels_low = autograd.Variable(torch.IntTensor( torch.from_numpy(np.array(gt_labels_low)).int() ))
      
      if loss is None:
        loss = ctc_loss(labels_pred.permute(2,0,1), labels, probs_sizes, label_sizes)
        loss += ctc_loss(labels_pred.permute(2,0,1), labels_low, probs_sizes, label_sizes) * 0.1
      else:
        loss += ctc_loss(labels_pred.permute(2,0,1), labels, probs_sizes, label_sizes)
        loss += ctc_loss(labels_pred.permute(2,0,1), labels_low, probs_sizes, label_sizes) * 0.1
      ctc_loss_count += 1
      
      if True:
        ctc_f = labels_pred.data.cpu().numpy()
        ctc_f = ctc_f.swapaxes(1, 2)
    
        labels = ctc_f.argmax(2)
        det_text, conf, dec_s = print_seq_ext(labels[0, :], codec)  
        
        print('{0} \t {1}'.format(det_text, gt_txt))
        
      gts_count[gt_id] += 1
        
      if ctc_loss_count > 128:
        break
      
    for gt_id in range(0, len(gts)):
      
      gt = gts[gt_id]
      gt_txt = lbs[gt_id]
      gt_txt_low = gt_txt.lower()
      if gt_txt.startswith('##'):
        continue
      
      angle_gt = ( math.atan2((gt[2][1] - gt[1][1]), gt[2][0] - gt[1][0]) + math.atan2((gt[3][1] - gt[0][1]), gt[3][0] - gt[0][0]) ) / 2
    
      center = (gt[0, :] + gt[1, :] + gt[2, :] + gt[3, :]) / 4
      dw = gt[2, :] - gt[1, :]
      dh =  gt[1, :] - gt[0, :]
    
      w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1])
      h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1])
      
      if gt[:, 0].max() > im_data.size(3) or gt[:, 1].max() > im_data.size(3) :
        continue 
      
      if gt.min() < 0:
        continue
      
      if h < 12:
        #print('too small h!')
        continue
           
      input_W = features.size(3)
      input_H = features.size(2)
      target_h = 32  
      
      scale = target_h / h 
      target_gw = int(w * scale) + target_h 
      
        
      #scale original coords to feature space
      xc = center[0] / 4
      yc = center[1] / 4
      w2 = w / 4
      h2 = h / 4 
      
      if w < h and len(gt_txt) > 3:
        print('WTF!')
        continue
        
      scalex = w2 / (input_W)
      scaley = h2 / (input_H)
    
      th11 =  scalex * math.cos(angle_gt)
      th12 = -math.sin(angle_gt) * scaley
      th13 =  (2 * xc - input_W - 1) / (input_W - 1) 
      
      th21 = math.sin(angle_gt) * scalex 
      th22 =  scaley * math.cos(angle_gt)  
      th23 =  (2 * yc - input_H - 1) / (input_H - 1)  
      
      
      t = np.asarray([th11, th12, th13, th21, th22, th23], dtype=np.float)
      t = torch.from_numpy(t).type(torch.FloatTensor)
      t = t.cuda()
      theta = t.view(-1, 2, 3)
      
      target_h = 8
      target_gw = int(w / h * target_h)
      if target_gw < 8:
        target_gw = 8
      grid = F.affine_grid(theta, torch.Size((1, 3, int(target_h), int(target_gw))))
      
      
      x = F.grid_sample(features[bid].unsqueeze(0), grid)
      #score_sampled = F.grid_sample(iou_pred[bid].unsqueeze(0), grid)
      
      gt_labels = []
      gt_labels_low = []
      for k in range(len(gt_txt)):
        if gt_txt[k] in codec_rev:                
          gt_labels.append( codec_rev[gt_txt[k]] )
          gt_labels_low.append( codec_rev[gt_txt_low[k]] )
        else:
          print('Unknown char: {0}'.format(gt_txt[k]) )
          gt_labels.append( 3 )
          gt_labels_low.append( 3 )
          
      if 'ARABIC' in ud.name(gt_txt[0]):
          gt_labels = gt_labels[::-1]
      
      labels_pred = net.forward_ocr(x)
      
      label_length = []
      label_length.append(len(gt_labels))
      probs_sizes =  autograd.Variable(torch.IntTensor( [(labels_pred.permute(2,0,1).size()[0])] * (labels_pred.permute(2,0,1).size()[1]) ))
      label_sizes = autograd.Variable(torch.IntTensor( torch.from_numpy(np.array(label_length)).int() ))
      labels = autograd.Variable(torch.IntTensor( torch.from_numpy(np.array(gt_labels)).int() ))
      labels_low = autograd.Variable(torch.IntTensor( torch.from_numpy(np.array(gt_labels_low)).int() ))
      
      if loss is None:
        loss = ctc_loss(labels_pred.permute(2,0,1), labels, probs_sizes, label_sizes)
        loss += ctc_loss(labels_pred.permute(2,0,1), labels_low, probs_sizes, label_sizes) * 0.1
      else:
        loss += ctc_loss(labels_pred.permute(2,0,1), labels, probs_sizes, label_sizes)
        loss += ctc_loss(labels_pred.permute(2,0,1), labels_low, probs_sizes, label_sizes) * 0.1
      ctc_loss_count += 1
      
      gt_proc += 1
      if True:
        ctc_f = labels_pred.data.cpu().numpy()
        ctc_f = ctc_f.swapaxes(1, 2)
    
        labels = ctc_f.argmax(2)
        det_text, conf, dec_s = print_seq_ext(labels[0, :], codec)  
        
        print('{0} \t {1}'.format(det_text, gt_txt))
        if det_text.lower() == gt_txt.lower():
          gt_good += 1
        
      if ctc_loss_count > 64 or debug:
        break  
    
    if loss is not None:
      loss /= ctc_loss_count
      loss.backward(retain_graph=True) 
      ctc_loss_val += loss.data.cpu()[0] 
      
    return ctc_loss_val / max(1, ctc_loss_count), gt_good , gt_proc
  
     
def main(opts):
  
  model_name = 'SemanticTexte2e'
  
  if opts.small == 0:
    net = ModelResNetSep2()
    model_name = 'SSTextResSep50'
    print("Using Resnet Sep 50")
  else:
    net = OCRModel()
    model_name = 'OCRModel'
    print("Using Resnet Sep 18 Dil")
  
  if opts.cuda:
    net.cuda()
    
  bad_images = open('bad_images.txt', 'w')  
  if not os.path.exists('bad_images'):
    os.mkdir('bad_images')
  
  step_start = 0  
  if os.path.exists(opts.model):
    print('loading model from %s' % args.model)
    step_start, learning_rate = net_utils.load_net(args.model, net)
  else:
    learning_rate = base_lr
  
  learning_rate = base_lr
  net.train()
  optimizer = torch.optim.Adam(net.parameters(), lr=base_lr)
  
  data_generator = icdar.get_batch(num_workers=opts.num_readers, 
           input_size=opts.input_size, batch_size=opts.batch_size, 
           train_list=opts.train_list)
  
  train_loss = 0
  bbox_loss, iou_loss, angle_loss = 0., 0., 0.
  cnt = 0
  ctc_loss = CTCLoss()
  
  ctc_loss_val = 0
  good_all = 0
  gt_all = 0
  
  for step in range(step_start, 10000000):
    # batch
    images, image_fns, score_maps, geo_maps, training_mask, gtso, lbso, gt_idxs, score_maps_full = next(data_generator)
    im_data = net_utils.np_to_variable(images, is_cuda=opts.cuda, volatile=False).permute(0, 3, 1, 2)
    start = timeit.timeit()
    iou_pred, roi_pred, rbox_prob, angle_pred, features = net(im_data)
    end = timeit.timeit()
    
    # backward
    smaps_vars = []
    smaps_vars_full = []
    training_masks = []
    angles_gt = []
    geos_gt = []
    for s in range(0, len(score_maps)):
      smaps_var = net_utils.np_to_variable(score_maps[s].squeeze(3), is_cuda=opts.cuda, volatile=False)
      smaps_vars.append(smaps_var)
      smaps_var_full = net_utils.np_to_variable(score_maps_full[s].squeeze(3), is_cuda=opts.cuda, volatile=False)
      smaps_vars_full.append(smaps_vars_full)
      training_mask = net_utils.np_to_variable(training_mask[s], is_cuda=opts.cuda, volatile=False)
      training_masks.append(training_mask)
      angle_gt = net_utils.np_to_variable(geo_maps[s][:, :, :, 4], is_cuda=opts.cuda, volatile=False)
      angles_gt.append(angle_gt)
      geo_gt = net_utils.np_to_variable(geo_maps[s][:, :, :, [0, 1, 2, 3]], is_cuda=opts.cuda, volatile=False)
      geos_gt.append(geo_gt)
    try:
      loss = net.loss(iou_pred, smaps_vars, smaps_vars_full, training_masks, angle_pred, angles_gt, roi_pred, rbox_prob,  geos_gt)
    except:
      import sys, traceback
      traceback.print_exc(file=sys.stdout)
      continue
      
    bbox_loss += net.box_loss_value.data.cpu().numpy()[0] #net.bbox_loss.data.cpu().numpy()[0]
    iou_loss += net.iou_loss_value.data.cpu().numpy()[0]
    angle_loss += net.angle_loss_value.data.cpu().numpy()[0]
    if False and net.angle_loss_value.data.cpu().numpy()[0] > 0.500 and image_fns[0].find('/done') == -1 and image_fns[0].find('/icdar-2015-Ch4') == -1:
      
      bad_images.write('{0}\n'.format(image_fns[0]))
      bad_images.flush()
      
      new_name = 'bad_images/{0}'.format(os.path.basename(image_fns[0]))
      
      os.rename(image_fns[0], new_name)
      annfile = image_fns[0][:-3] + 'txt'
      new_name = 'bad_images/{0}'.format(os.path.basename(annfile))
      os.rename(annfile, new_name)
      
      gt_file = '{0}/gt_{1}'.format(os.path.dirname(os.path.basename(image_fns[0])),  os.path.basename(annfile))
      if os.path.exists(gt_file):
        gt_file_dst = 'bad_images/gt_{1}'.format(os.path.dirname(os.path.basename(image_fns[0])),  os.path.basename(annfile))
        os.rename(gt_file, gt_file_dst)
        
      
      print('bad angle {0}'.format(image_fns[0]))
      
      
      
    train_loss += loss.data.cpu().numpy()[0]
    optimizer.zero_grad()
    
    try:
      ctc_b_loss_val, gt_b_good, gt_b_all = process_boxes(images, im_data, iou_pred, roi_pred, angle_pred, score_maps, score_maps_full, gt_idxs, gtso, lbso, features, net, ctc_loss, debug=opts.debug)
      ctc_loss_val += ctc_b_loss_val
      gt_all += gt_b_all
      good_all += gt_b_good
      #pass
    except:
      import sys, traceback
      traceback.print_exc(file=sys.stdout)
      continue
    
    try:
      loss.backward()
      optimizer.step()
    except:
      import sys, traceback
      traceback.print_exc(file=sys.stdout)
      pass
    cnt += 1
    if step % disp_interval == 0:
      
      if opts.debug:
        
        iou = iou_pred.data.cpu()[0].numpy()
        iou = iou.squeeze(0)
        cv2.imshow('iou_map', iou)
        iou_res = cv2.resize(score_maps[0], (images.shape[2], images.shape[1]))
        iou_res = cv2.resize(score_maps[0], (images.shape[2], images.shape[1]))
        mask = np.argwhere(iou_res > 0)
        
        x_data = im_data.data.cpu().numpy()[0]
        x_data = x_data.swapaxes(0, 2)
        x_data = x_data.swapaxes(0, 1)
        
        x_data += 1
        x_data *= 128
        x_data = np.asarray(x_data, dtype=np.uint8)
        x_data = x_data[:, :, ::-1]
        
        im_show = x_data
        im_show[mask[:, 0], mask[:, 1], 1] = 255 
        im_show[mask[:, 0], mask[:, 1], 0] = 0 
        im_show[mask[:, 0], mask[:, 1], 2] = 0
        cv2.imshow('img0', im_show) 
        cv2.imshow('score_maps', score_maps[0] * 255)
        cv2.imshow('train_mask', training_masks.data.cpu()[0].numpy())
        cv2.waitKey(1000)
      
      train_loss /= cnt
      bbox_loss /= cnt
      iou_loss /= cnt
      angle_loss /= cnt
      ctc_loss_val /= cnt
      try:
        print('epoch %d[%d], loss: %.3f, bbox_loss: %.3f, iou_loss: %.3f, ang_loss: %.3f, ctc_loss: %.3f, rec: %.5f in %.3f' % (
          step / batch_per_epoch, step, train_loss, bbox_loss, iou_loss, angle_loss, ctc_loss_val, good_all / gt_all, end - start))
      except:
        import sys, traceback
        traceback.print_exc(file=sys.stdout)
        pass
      
      

      train_loss = 0
      bbox_loss, iou_loss, angle_loss = 0., 0., 0.
      cnt = 0
      ctc_loss_val = 0
      good_all = 0
      gt_all = 0

    
    #if step % valid_interval == 0:
    #  validate(opts.valid_list, net)
    if step > step_start and (step % batch_per_epoch == 0):
      save_name = os.path.join(opts.save_path, '{}_{}.h5'.format(model_name, step))
      state = {'step': step,
               'learning_rate': learning_rate,
              'state_dict': net.state_dict(),
              'optimizer': optimizer.state_dict()}
      torch.save(state, save_name)
      print('save model: {}'.format(save_name))


import argparse

if __name__ == '__main__': 
  
  parser = argparse.ArgumentParser()
  #parser.add_argument('-train_list', default='/mnt/textspotter/tmp/SynthText/trainMLT_Synth.txt')
  parser.add_argument('-train_list', default='/home/busta/data/SynthText/ti.txt')
  parser.add_argument('-valid_list', default='/home/busta/data/SynthText/ti.txt')
  parser.add_argument('-save_path', default='backup')
  parser.add_argument('-model', default='SSTextResSep50_440000.h5')
  parser.add_argument('-debug', type=int, default=1)
  parser.add_argument('-batch_size', type=int, default=1)
  parser.add_argument('-num_readers', type=int, default=1)
  parser.add_argument('-cuda', type=bool, default=True)
  parser.add_argument('-input_size', type=int, default=512)
  parser.add_argument('-small', type=int, default=0)
  
  args = parser.parse_args()  
  main(args)
  
