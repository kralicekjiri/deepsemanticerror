'''
Created on Sep 3, 2017

@author: Michal.Busta at gmail.com
'''

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from torch.nn import LeakyReLU, Conv2d, Dropout2d, LogSoftmax, InstanceNorm2d

import torch.autograd as autograd

import math

class Identity(nn.Module):
    def forward(self, inputs):
        return inputs

def box2affine(input, target_w, target_h):
  
  batch_size = input.size(0)
  
  xc = input[:, 0]
  yc = input[:, 1]
  w = input[:, 2]
  h = input[:, 3]

  th23 =  xc * 2 + (-1 - target_w)  / (target_w - 1)
  th13 =  yc * 2  + (-1 - target_h)  / (target_h - 1)
  th22 = w / target_w
  th11 = h / target_h
  
  th12 = autograd.Variable(torch.zeros(batch_size, 1))
  th21 = autograd.Variable(torch.zeros(batch_size, 1))

  return torch.stack((th11, th12, th13, th21, th22, th23), dim=1)

def stn(x, theta):
  grid = F.affine_grid(theta, x.size())
  x = F.grid_sample(x, grid)
  
class BIN(nn.InstanceNorm2d):
  
  def __init__(self, num_features, eps=1e-5, momentum=0.1):
    super(BIN, self).__init__(num_features, eps, momentum, False)
    
    self.bn = nn.BatchNorm2d(num_features, eps=eps, momentum=momentum, affine=False)
    self.scale = torch.nn.parameter.Parameter(torch.Tensor(num_features).fill_(1))
    self.bias = torch.nn.parameter.Parameter(torch.Tensor(num_features).fill_(0))
    self.rho = torch.nn.parameter.Parameter(torch.Tensor(num_features).fill_(0.5))
    
  def forward(self, x):
    
    rho = F.sigmoid(self.rho) 
    x_in = super(BIN, self).forward(x)
    x_bn = self.bn.forward(x)
    
    out = (rho.unsqueeze(1).unsqueeze(1).expand_as(x_bn) * x_bn + (1 - rho.unsqueeze(1).unsqueeze(1).expand_as(x_bn)) * x_in) * self.scale.unsqueeze(1).unsqueeze(1).expand_as(x_bn) + self.bias.unsqueeze(1).unsqueeze(1).expand_as(x_bn)
    return out

class Conv2dCW(nn.Conv2d):
  
  def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True):
    super(Conv2dCW, self).__init__(in_channels, out_channels, kernel_size, stride=stride,
                 padding=padding, dilation=dilation, groups=groups, bias=bias)
    
    self.in_channels = in_channels 
    self.out_channels = out_channels
    self.kernel_size = kernel_size
    self.stride = stride
    self.padding = padding
    self.dilation = dilation
    self.groups = groups
  
  def forward(self, x):
    
    m = self.weight.view(self.weight.size(0), -1).mean(1)
    m = m.unsqueeze(1).expand(self.weight.size(0), self.weight.size(1))
    m = m.unsqueeze(2).expand(self.weight.size(0), self.weight.size(1), self.weight.size(2))
    m = m.unsqueeze(3).expand(self.weight.size(0), self.weight.size(1), self.weight.size(2), self.weight.size(3))
    
    v_center = (self.weight - m) 
    norm = v_center.view(self.weight.size(0), -1).norm(2, 1).pow(-1)
    norm = norm.unsqueeze(1).expand(self.weight.size(0), self.weight.size(1))
    norm = norm.unsqueeze(2).expand(self.weight.size(0), self.weight.size(1), self.weight.size(2))
    norm = norm.unsqueeze(3).expand(self.weight.size(0), self.weight.size(1), self.weight.size(2), self.weight.size(3))
    w2 = v_center * norm
    x =  F.conv2d(x, w2, bias=self.bias, stride=self.stride, padding=self.padding, dilation=self.dilation, groups=self.groups)
    return x
    

class CReLU(nn.Module):
  def __init__(self):
    super(CReLU, self).__init__()
  def forward(self, x):
    return torch.cat((F.leaky_relu(x, 0.01), F.leaky_relu(-x, 0.01)), 1)

class CReLU_BN(nn.Module):
  def __init__(self, channels):
    super(CReLU_BN, self).__init__()
    self.bn = nn.InstanceNorm2d(channels * 2, eps=1e-05, momentum=0.1, affine=True)
  def forward(self, x):
    cat = torch.cat((x, -x), 1)
    x = self.bn(cat)
    return F.leaky_relu(x, 0.01)



def conv_bn(inp, oup, stride):
    return nn.Sequential(
      nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
      nn.BatchNorm2d(oup),
      nn.ReLU(inplace=True)
    )

def conv_dw(inp, oup, stride, dilation=1):
  return nn.Sequential(
    nn.Conv2d(inp, inp, 3, stride, 1 + (dilation > 0) * (dilation -1), dilation=dilation, groups=inp, bias=False),
    nn.BatchNorm2d(inp),
    nn.LeakyReLU(inplace=True, negative_slope=0.01),

    nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
    nn.BatchNorm2d(oup),
    nn.LeakyReLU(inplace=True, negative_slope=0.01),
  )
  
def conv_dw_plain(inp, oup, stride, dilation=1):
  return nn.Sequential(
    nn.Conv2d(inp, inp, 3, stride, 1 + (dilation > 0) * (dilation -1), dilation=dilation, groups=inp, bias=False),
    nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
  )
  
def conv_dw_res(inp, oup, stride):
  return nn.Sequential(
    nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
    nn.BatchNorm2d(inp),
    nn.LeakyReLU(inplace=True, negative_slope=0.01),

    nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
    nn.BatchNorm2d(oup),
  )
  
def conv_dwc(inp, oup, stride, dilation=1):
  return nn.Sequential(
    Conv2dCW(inp, inp, 3, stride, 1 + (dilation > 0) * (dilation -1), dilation=dilation, groups=inp, bias=False),
    nn.BatchNorm2d(inp, affine=True),
    nn.LeakyReLU(inplace=True, negative_slope=0.1),

    Conv2dCW(inp, oup, 1, 1, 0, bias=False),
    nn.InstanceNorm2d(oup, affine=True),
    nn.LeakyReLU(inplace=True, negative_slope=0.1),
  )
  
def conv_dwc_res(inp, oup, stride):
  return nn.Sequential(
    Conv2dCW(inp, inp, 3, stride, 1, groups=inp, bias=False),
    nn.BatchNorm2d(inp),
    nn.LeakyReLU(inplace=True, negative_slope=0.01),

    Conv2dCW(inp, oup, 1, 1, 0, bias=False),
    nn.InstanceNorm2d(oup)
  )

def conv_dw_in(inp, oup, stride, dilation=1):
  return nn.Sequential(
    nn.Conv2d(inp, inp, 3, stride, 1 + (dilation > 0) * (dilation -1), dilation=dilation, groups=inp, bias=False),
    nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
    BIN(oup, eps=1e-05, momentum=0.1),
    nn.LeakyReLU(inplace=True, negative_slope=0.01),
  )

def conv_dw_in_flat(inp, oup, stride=1, dilation=1):
  return nn.Sequential(
    nn.Conv2d(inp, inp, 3, stride, 1 + (dilation > 0) * (dilation -1), dilation=dilation, groups=inp, bias=False),
    nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
    BIN(oup, eps=1e-05, momentum=0.1),
    nn.LeakyReLU(inplace=True, negative_slope=0.01),
  )

def conv_dw_res_in(inp, oup, stride):
  return nn.Sequential(
    nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
    nn.InstanceNorm2d(inp, eps=1e-05, momentum=0.1, affine=True),
    nn.LeakyReLU(inplace=True, negative_slope=0.01),

    nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
    nn.InstanceNorm2d(oup, eps=1e-05, momentum=0.1, affine=True)
  )
    
def dice_loss(inp, target):
    
    smooth = 1.
    iflat = inp.view(-1)
    tflat = target.view(-1)
    intersection = (iflat * tflat).sum()
    
    return - ((2. * intersection + smooth) /
              (iflat.sum() + tflat.sum() + smooth))
    
def calc_distances(v1, v2):
    distances = []
    for i in range(len(v1)):
      d_test = torch.dist(v1[i].cpu(), v2[i].cpu(), 2)
      d =torch.dist(v1[i], v2[i], 2)

      if len(distances) == 0:
        distances = d
      else:
        distances = torch.cat((distances, d), 0)

    distances = torch.exp(distances)
    distances = 2.0 / (1 + distances)
    return distances
  

  

class BasicBlockSep(nn.Module):
  expansion = 1

  def __init__(self, inplanes, planes, stride=1, downsample=None, dilation=1):
    super(BasicBlockSep, self).__init__()
    
    self.conv_sep1 = conv_dw(inplanes, planes, stride, dilation=dilation)
    
    
    self.conv2 = conv_dw_res(planes, planes, 1)
    self.downsample = downsample
    self.stride = stride
    self.relu = LeakyReLU(negative_slope=0.01, inplace=True)

  def forward(self, x):
    residual = x

    out = self.conv_sep1(x)

    out = self.conv2(out)

    if self.downsample is not None:
      residual = self.downsample(x)

    out += residual
    out = self.relu(out)

    return out  
      
class BasicBlockIn(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None, dilation=1):
        super(BasicBlockIn, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = BIN(planes, eps=1e-05, momentum=0.1)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = BIN(planes, eps=1e-05, momentum=0.1)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out
  
class BasicBlockSepIn(nn.Module):
  expansion = 1

  def __init__(self, inplanes, planes, stride=1, downsample=None, dilation=1):
    super(BasicBlockSepIn, self).__init__()
    
    self.conv_sep1 = conv_dw_in(inplanes, planes, stride, dilation=dilation)
    
    
    self.conv2 = conv_dw_res_in(planes, planes, 1)
    self.downsample = downsample
    self.stride = stride
    self.relu = LeakyReLU(negative_slope=0.01, inplace=True)

  def forward(self, x):
    residual = x

    out = self.conv_sep1(x)

    out = self.conv2(out)

    if self.downsample is not None:
      residual = self.downsample(x)

    out += residual
    out = self.relu(out)

    return out  
  
class ModelResNetSep2(nn.Module):
            
  def __init__(self, attention = False):
    super(ModelResNetSep2, self).__init__()
    
    self.inplanes = 64
    
    self.layer0 = nn.Sequential(
      Conv2dCW(3, 16, 3, stride=1, padding=1, bias=False),
      CReLU_BN(16),
      Conv2dCW(32, 32, 3, stride=2, padding=1, bias=False),
      CReLU_BN(32)
    )
    
    self.layer0_1 = nn.Sequential(
      Conv2dCW(64, 64, 3, stride=1, padding=1, bias=False),
      nn.ReLU(),
      Conv2dCW(64, 64, 3, stride=2, padding=1, bias=False),
      nn.ReLU()
    )
    
    self.conv5 = Conv2d(64, 128, (3,3), padding=(1, 1), bias=False)
    self.conv6 = Conv2d(128, 128, (3,3), padding=1, bias=False)
    self.conv7 = Conv2d(128,256, (3,3), padding=1, bias=False)
    self.conv8 = Conv2d(256, 256, (3,3), padding=1, bias=False)
    self.conv9 = Conv2d(256, 256, (3,3), padding=(1, 1), bias=False)
    self.conv10_s = Conv2d(256, 256, (2, 3), padding=(0, 1), bias=False)
    self.conv11 = Conv2d(256, 7500, (1, 1), padding=(0,0))
    
    self.batch5 = InstanceNorm2d(128, eps=1e-05, momentum=0.1, affine=True)
    self.batch7 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.batch8 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.batch9 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.batch10_s = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.max2 = nn.MaxPool2d((2, 1), stride=(2,1))
    self.leaky = LeakyReLU(negative_slope=0.01, inplace=True)
    
    
    
    self.layer1 = self._make_layer(BasicBlockIn, 64, 3, stride=1)
    self.inplanes = 64
    self.layer2 = self._make_layer(BasicBlockIn, 128, 4, stride=2)
    self.layer3 = self._make_layer(BasicBlockSepIn, 256, 6, stride=2)
    self.layer4 = self._make_layer(BasicBlockSepIn, 512, 4, stride=2, dilation=1)
    
    self.up1 = nn.Upsample(scale_factor=2, mode='bilinear')
    self.feature4 = nn.Conv2d(512, 256, 1, stride=1, padding=0, bias=False)
    
    self.up2 = nn.Upsample(scale_factor=2, mode='bilinear')
    self.feature3 = nn.Conv2d(256, 256, 1, stride=1, padding=0, bias=False)
    
    self.up3 = nn.Upsample(scale_factor=2, mode='bilinear')
    self.feature2 = nn.Conv2d(128, 256, 1, stride=1, padding=0, bias=False)
    
    self.upconv2 = conv_dw_plain(256, 256, stride=1)
    self.upconv1 = conv_dw_plain(256, 256, stride=1)
    
    self.feature1 = nn.Conv2d(64, 256, 1, stride=1, padding=0, bias=False)
    
    self.act = Conv2d(128, 1, (1,1), padding=0, stride=1)
    self.rbox = Conv2d(128, 4, (1,1), padding=0, stride=1)
    
    self.rbox_prob = Conv2d(128, 1, (1,1), padding=0, stride=1)
    
    self.angle = Conv2d(128, 2, (1,1), padding=0, stride=1)
    self.drop1 = Dropout2d(p=0.2, inplace=False)
    
    self.angle_loss = nn.MSELoss(size_average=True)
    self.h_loss = nn.SmoothL1Loss(size_average=True)
    self.w_loss = nn.SmoothL1Loss(size_average=True)
    
    self.eloss = nn.MSELoss(size_average=False, reduce=False)
    
    self.attention = attention
  
    if self.attention:
      self.conv_attenton = nn.Conv2d(256, 1, kernel_size=1, stride=1, padding=0, bias=True) 
  
  def _make_layer(self, block, planes, blocks, stride=1, dilation=1):
    
    downsample = None
    if stride != 1 or self.inplanes != planes * block.expansion:
      downsample = nn.Sequential(
  
        nn.Conv2d(self.inplanes, planes * block.expansion,
                  kernel_size=1, stride=stride, bias=False),
        nn.BatchNorm2d(planes * block.expansion),
      )

    layers = []
    layers.append(block(self.inplanes, planes, stride, downsample, dilation=dilation))
    self.inplanes = planes * block.expansion
    for i in range(1, blocks):
      layers.append(block(self.inplanes, planes))

    return nn.Sequential(*layers)
  
  def forward_ocr(self, x):
    
    x = self.conv5(x)
    x = self.batch5(x)
    x = self.leaky(x)
    
    x = self.conv6(x)
    x = self.leaky(x)
    x = self.conv6(x)
    x = self.leaky(x)
    
    x = self.max2(x)
    x = self.conv7(x)
    x = self.batch7(x)
    x = self.leaky(x)
    
    x = self.conv8(x)
    x = self.leaky(x)
    x = self.conv8(x)
    x = self.leaky(x)
    
    
    x = self.max2(x)
    
    x = self.conv9(x)
    x = self.leaky(x)
    x = self.conv9(x)
    x = self.leaky(x)
    
    x = self.conv10_s(x)
    x = self.batch10_s(x)
    x = self.leaky(x)
    
    
    x = self.drop1(x)
    x = self.conv11(x)
    x = x.squeeze(2)

    x = x.permute(0,2,1)
    y = x
    x = x.contiguous().view(-1,x.data.shape[2])
    x = LogSoftmax()(x)
    x = x.view_as(y)
    x = x.permute(0,2,1)
    
    return x   
  
  def forward_features(self, x):
    
    x = self.layer0(x)
    focr = self.layer0_1(x)
    return focr

  def forward(self, x):
    
    x = self.layer0(x)
    x = self.layer0_1(x)
    
    x = self.drop1(x)
    su3 = self.layer1(x)
    features1 = self.feature1(su3)
    su2 = self.layer2(su3)
    features2 = self.feature2(su2)
    su1 = self.layer3(su2)
    features3 = self.feature3(su1)
    x = self.layer4(su1)
    
    features4 = self.feature4(x)
    
    x = self.up1(features4)
    features3 = x + features3
    x = features3
    
    x = self.up2(x)
    x = self.upconv1(x)
    features2 = x + features2  
    x = features2
        
    x = self.up3(x)
    x = self.upconv2(x)
    
    x += features1
    
    x = self.drop1(x)
    
    f1 = x[:, 0:128, :, :]
    f2 = x[:, 128:256, :, :]
    
    iou_pred = F.sigmoid(self.act(f1))
    rbox = F.sigmoid(self.rbox(f2)) * 128
    angle = F.sigmoid(self.angle(f1)) * 2 - 1 
    
    return iou_pred, rbox, angle, x

  def loss(self, iou_preds, iou_gt, iou_mask, angle_preds, angle_gt, roi_pred, roi_gt, angle_masks):
    
    self.box_loss_value =  torch.autograd.Variable(torch.from_numpy(np.asarray([0])).type(torch.FloatTensor), volatile=False).cuda()
    self.angle_loss_value =  torch.autograd.Variable(torch.from_numpy(np.asarray([0])).type(torch.FloatTensor), volatile=False).cuda()
  
    iou_pred = iou_preds.squeeze(1)
    angle_pred = angle_preds
    self.iou_loss_value = dice_loss(iou_pred * iou_mask, iou_gt * iou_mask)
    
    byte_mask = torch.gt(iou_gt.data * angle_masks.data, 0.5)
    
    if byte_mask.sum() > 0:
      
      gt_sin = torch.sin(angle_gt[byte_mask])
      gt_cos = torch.cos(angle_gt[byte_mask])
      
      sin_val = self.angle_loss(angle_pred[:, 0, :, :][byte_mask], gt_sin)
      if sin_val.data[0] < 0.15: 
      
        self.angle_loss_value += sin_val
        self.angle_loss_value += self.angle_loss(angle_pred[:, 1, :, :][byte_mask], gt_cos)
        
        d1_gt = roi_gt[:, :, :, 0][byte_mask]
        d2_gt = roi_gt[:, :, :, 1][byte_mask] 
        d3_gt = roi_gt[:, :, :, 2][byte_mask]
        d4_gt = roi_gt[:, :, :, 3][byte_mask] 
        
        mask3 = torch.gt(d3_gt, 0)   
        mask4 = torch.gt(d4_gt, 0)   
        d3_gt = d3_gt[mask3]
        d4_gt = d4_gt[mask4] 
        
        
        d1_pred = roi_pred[:, 0, :, :][byte_mask]
        d2_pred = roi_pred[:, 1, :, :][byte_mask]
        d3_pred = roi_pred[:, 2, :, :][byte_mask]
        d3_pred = d3_pred[mask3]
        d4_pred = roi_pred[:, 3, :, :][byte_mask]
        d4_pred = d4_pred[mask4]
        
        area_gt_l = (d1_gt[mask3] + d2_gt[mask3]) * (d3_gt)
        area_pred_l = (d1_pred[mask3] + d2_pred[mask3]) * (d3_pred)
        w_union_l = torch.min(d3_gt, d3_pred)
        h_union_l = torch.min(d1_gt[mask3], d1_pred[mask3]) + torch.min(d2_gt[mask3], d2_pred[mask3])
        area_intersect_l = w_union_l * h_union_l
        area_union_l = area_gt_l + area_pred_l - area_intersect_l
        AABB_l = - torch.log((area_intersect_l + 1.0)/(area_union_l + 1.0))
        
        if AABB_l.dim() > 0:
          self.box_loss_value += torch.mean(AABB_l)
        
        area_gt_r = (d1_gt[mask4] + d2_gt[mask4]) * (d4_gt)
        area_pred_r = (d1_pred[mask4] + d2_pred[mask4]) * (d4_pred)
        w_union_r = torch.min(d4_gt, d4_pred)
        h_union_r = torch.min(d1_gt[mask4], d1_pred[mask4]) + torch.min(d2_gt[mask4], d2_pred[mask4])
        area_intersect_r = w_union_r * h_union_r
        area_union_r = area_gt_r + area_pred_r - area_intersect_r
        AABB_r = - torch.log((area_intersect_r + 1.0)/(area_union_r + 1.0))
        if AABB_r.dim() > 0:
          self.box_loss_value += torch.mean(AABB_r)
          
      #else:
      #  print('too big angle!')
      
    return self.iou_loss_value +  self.angle_loss_value * 2 + 0.5 * self.box_loss_value  
  
  def freeze_features(self):
    
    self.layer0[0].weight.reqires_grad = False
    self.layer0[2].weight.reqires_grad = False
    self.layer0_1[0].weight.reqires_grad = False
    self.layer0_1[2].weight.reqires_grad = False

  


    
    
class OCRModel(nn.Module):
  def __init__(self, activation='Relu'):
    super(OCRModel, self).__init__()
    self.conv1 = Conv2d(3, 32, (3,3), padding=1, bias=False)
    self.conv2 = Conv2d(32, 32, (3,3), padding=1, bias=False)
    self.conv3 = Conv2d(32, 64, (3,3), padding=1, bias=False)
    self.conv4 = Conv2d(64, 64, (3,3), padding=1, bias=False)
    self.conv5 = Conv2d(64, 128, (3,3), padding=(1, 2), bias=False)
    self.conv6 = Conv2d(128, 128, (3,3), padding=1, bias=False)
    self.conv7 = Conv2d(128,256, (3,3), padding=1, bias=False)
    self.conv8 = Conv2d(256, 256, (3,3), padding=1, bias=False)
    self.conv9 = Conv2d(256, 256, (3,3), padding=(1, 2), bias=False)
    self.conv10_s = Conv2d(256, 256, (2, 3), padding=(0, 1), bias=False)
    self.conv11 = Conv2d(256, 7500, (1, 1), padding=(0,0))
    

    # self.batch1 = BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=False)
    # self.batch2 = BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=False)
    # self.batch3 = BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=False)
    self.batch1 = InstanceNorm2d(32, eps=1e-05, momentum=0.1, affine=True)
    self.batch2 = InstanceNorm2d(32, eps=1e-05, momentum=0.1, affine=True)
    self.batch3 = InstanceNorm2d(64, eps=1e-05, momentum=0.1, affine=True)
    self.batch5 = InstanceNorm2d(128, eps=1e-05, momentum=0.1, affine=True)
    self.batch7 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.batch8 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.batch9 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.batch10_s = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.drop1 = Dropout2d(p=0.2, inplace=False)
    
    if activation == 'ELU':
      self.leaky = nn.ELU()
    else:
      self.leaky = LeakyReLU(negative_slope=0.01, inplace=False)
    self.max1 = nn.MaxPool2d((2, 2), stride=None)
    self.max2 = nn.MaxPool2d((2, 1), stride=(2,1))
    
    #and detector definition 
    self.inplanes = 64
    self.layer1 = self._make_layer(BasicBlockIn, 64, 3, stride=1)
    self.inplanes = 64
    self.layer2 = self._make_layer(BasicBlockIn, 128, 4, stride=2)
    self.layer3 = self._make_layer(BasicBlockSepIn, 256, 6, stride=2)
    self.layer4 = self._make_layer(BasicBlockSepIn, 512, 4, stride=2, dilation=1)
    
    self.up1 = nn.Upsample(scale_factor=2, mode='bilinear')
    self.feature4 = nn.Conv2d(512, 256, 1, stride=1, padding=0, bias=False)
    
    self.up2 = nn.Upsample(scale_factor=2, mode='bilinear')
    self.feature3 = nn.Conv2d(256, 256, 1, stride=1, padding=0, bias=False)
    
    self.up3 = nn.Upsample(scale_factor=2, mode='bilinear')
    self.feature2 = nn.Conv2d(128, 256, 1, stride=1, padding=0, bias=False)
    
    self.upconv2 = conv_dw_plain(256, 256, stride=1)
    self.upconv1 = conv_dw_plain(256, 256, stride=1)
    
    self.feature1 = nn.Conv2d(64, 256, 1, stride=1, padding=0, bias=False)
    
    self.act = Conv2d(128, 1, (1,1), padding=0, stride=1)
    self.rbox = Conv2d(128, 4, (1,1), padding=0, stride=1)
    
    self.rbox_prob = Conv2d(128, 1, (1,1), padding=0, stride=1)
    
    self.angle = Conv2d(128, 2, (1,1), padding=0, stride=1)
    self.drop1 = Dropout2d(p=0.2, inplace=False)
    
    self.angle_loss = nn.MSELoss(size_average=True)
      
      
  def forward_ocr(self, x):
    try: x = x.cuda(0)
    except: pass
    x = self.conv5(x)
    x = self.batch5(x)
    x = self.leaky(x)
    
    x = self.conv6(x)
    x = self.leaky(x)
    x = self.conv6(x)
    x = self.leaky(x)
    
    x = self.max2(x)
    x = self.conv7(x)
    x = self.batch7(x)
    x = self.leaky(x)
    
    x = self.conv8(x)
    x = self.leaky(x)
    x = self.conv8(x)
    x = self.leaky(x)
    
    x = self.max2(x)
    x = self.conv9(x)
    x = self.batch9(x)
    x = self.leaky(x)
    
    x = self.conv10_s(x)
    x = self.batch10_s(x)
    x = self.leaky(x)
    
    
    x = self.drop1(x)
    x = self.conv11(x)
    x = x.squeeze(2)

    
    x = x.permute(0,2,1)
    y = x
    x = x.contiguous().view(-1,x.data.shape[2])
    x = LogSoftmax()(x)
    x = x.view_as(y)
    x = x.permute(0,2,1)

    return x
  
  def forward_features(self, x):
    
    x = self.conv1(x)
    x = self.batch1(x)
    x = self.leaky(x)
    
    x = self.conv2(x)
    x = self.batch2(x)
    x = self.leaky(x)
    x = self.max1(x)
    
    x = self.conv3(x)
    x = self.batch3(x)
    x = self.leaky(x)
    
    x = self.conv4(x)
    x = self.leaky(x)
    
    x = self.max1(x)
    
    return x
  
  def forward(self, x):
    
    x = self.forward_features(x)
    
    su3 = self.layer1(x)
    features1 = self.feature1(su3)
    su2 = self.layer2(su3)
    features2 = self.feature2(su2)
    su1 = self.layer3(su2)
    features3 = self.feature3(su1)
    x = self.layer4(su1)
    
    features4 = self.feature4(x)
    
    x = self.up1(features4)
    features3 = x + features3
    x = features3
    
    x = self.up2(x)
    x = self.upconv1(x)
    features2 = x + features2  
    x = features2
        
    x = self.up3(x)
    x = self.upconv2(x)
    
    x += features1
    
    x = self.drop1(x)
    
    f1 = x[:, 0:128, :, :]
    f2 = x[:, 128:256, :, :]
    
    iou_pred = F.sigmoid(self.act(f1))
    rbox = F.sigmoid(self.rbox(f2)) * 512
    angle = F.sigmoid(self.angle(f1)) * 2 - 1 
    
    return iou_pred, rbox, angle, x
  
  
  def loss(self, iou_preds, iou_gt, iou_mask, iou_mask2, angle_preds, angle_gt, roi_pred, roi_gt):
    
    self.iou_loss_value = 0
    self.box_loss_value =  torch.autograd.Variable(torch.from_numpy(np.asarray([0])).type(torch.FloatTensor), volatile=False).cuda()
    self.angle_loss_value =  torch.autograd.Variable(torch.from_numpy(np.asarray([0])).type(torch.FloatTensor), volatile=False).cuda()
  
    iou_pred = iou_preds.squeeze(1)
    angle_pred = angle_preds
    self.iou_loss_value += dice_loss(iou_pred * iou_mask, iou_gt * iou_mask)
    
    byte_mask = torch.gt(iou_gt.data * iou_mask.data, 0.5)
    
    if byte_mask.sum() > 0:
      
      gt_sin = torch.sin(angle_gt[byte_mask])
      gt_cos = torch.cos(angle_gt[byte_mask])
      
      sin_val = self.angle_loss(angle_pred[:, 0, :, :][byte_mask], gt_sin)
      if sin_val.data[0] < 3: 
      
        self.angle_loss_value += sin_val
        self.angle_loss_value += self.angle_loss(angle_pred[:, 1, :, :][byte_mask], gt_cos)
        
        d1_gt = roi_gt[:, :, :, 0][byte_mask]
        d2_gt = roi_gt[:, :, :, 1][byte_mask] 
        d3_gt = roi_gt[:, :, :, 2][byte_mask]
        d4_gt = roi_gt[:, :, :, 3][byte_mask] 
        
        mask3 = torch.gt(d3_gt, 0)   
        mask4 = torch.gt(d4_gt, 0)   
        d3_gt = d3_gt[mask3]
        d4_gt = d4_gt[mask4] 
        
        
        d1_pred = roi_pred[:, 0, :, :][byte_mask]
        d2_pred = roi_pred[:, 1, :, :][byte_mask]
        d3_pred = roi_pred[:, 2, :, :][byte_mask]
        d3_pred = d3_pred[mask3]
        d4_pred = roi_pred[:, 3, :, :][byte_mask]
        d4_pred = d4_pred[mask4]
        
        area_gt_l = (d1_gt[mask3] + d2_gt[mask3]) * (d3_gt)
        area_pred_l = (d1_pred[mask3] + d2_pred[mask3]) * (d3_pred)
        w_union_l = torch.min(d3_gt, d3_pred)
        h_union_l = torch.min(d1_gt[mask3], d1_pred[mask3]) + torch.min(d2_gt[mask3], d2_pred[mask3])
        area_intersect_l = w_union_l * h_union_l
        area_union_l = area_gt_l + area_pred_l - area_intersect_l
        AABB_l = - torch.log((area_intersect_l + 1.0)/(area_union_l + 1.0))
        
        if AABB_l.dim() > 0:
          self.box_loss_value += torch.mean(AABB_l)
        
        area_gt_r = (d1_gt[mask4] + d2_gt[mask4]) * (d4_gt)
        area_pred_r = (d1_pred[mask4] + d2_pred[mask4]) * (d4_pred)
        w_union_r = torch.min(d4_gt, d4_pred)
        h_union_r = torch.min(d1_gt[mask4], d1_pred[mask4]) + torch.min(d2_gt[mask4], d2_pred[mask4])
        area_intersect_r = w_union_r * h_union_r
        area_union_r = area_gt_r + area_pred_r - area_intersect_r
        AABB_r = - torch.log((area_intersect_r + 1.0)/(area_union_r + 1.0))
        if AABB_r.dim() > 0:
          self.box_loss_value += torch.mean(AABB_r)
          
      #else:
      #  print('too big angle!')
      
    return self.iou_loss_value +  self.angle_loss_value  + self.box_loss_value  
  
  def _make_layer(self, block, planes, blocks, stride=1, dilation=1):
    
    downsample = None
    if stride != 1 or self.inplanes != planes * block.expansion:
      downsample = nn.Sequential(
  
        nn.Conv2d(self.inplanes, planes * block.expansion,
                  kernel_size=1, stride=stride, bias=False),
        nn.InstanceNorm2d(planes * block.expansion, affine=True),
      )

    layers = []
    layers.append(block(self.inplanes, planes, stride, downsample, dilation=dilation))
    self.inplanes = planes * block.expansion
    for i in range(1, blocks):
      layers.append(block(self.inplanes, planes))

    return nn.Sequential(*layers)
  
  def freeze_features(self):
    self.conv1.weight.reqires_grad = False
    self.batch1.weight.reqires_grad = False
    self.batch1.bias.reqires_grad = False
    
    self.conv2.weight.reqires_grad = False
    self.batch2.weight.reqires_grad = False
    self.batch2.bias.reqires_grad = False
    
    self.conv3.weight.reqires_grad = False
    self.batch3.weight.reqires_grad = False
    self.batch3.bias.reqires_grad = False
    
    self.conv4.weight.reqires_grad = False
    
    
  
      
    
