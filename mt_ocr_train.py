'''
Created on Sep 29, 2017

@author: Michal.Busta at gmail.com
'''
import numpy as np
import torch.nn as nn
import os

f = open('codec.txt', 'r')
codec = f.readlines()[0]
f.close()
print('Total Num chars:' + str(len(codec)))

from torch.nn import MaxPool2d, LeakyReLU, Conv2d, Dropout2d, InstanceNorm2d, LogSoftmax
import torch
import net_utils
import argparse
import ocr_gen
from warpctc_pytorch import CTCLoss
from torch.autograd import Variable
from ocr_test_utils import test
import cv2


class OCRModel(nn.Module):
  def __init__(self, activation='Relu'):
    super(OCRModel, self).__init__()
    self.conv1 = Conv2d(3, 32, (3,3), padding=1, bias=False)
    self.conv2 = Conv2d(32, 32, (3,3), padding=1, bias=False)
    self.conv3 = Conv2d(32, 64, (3,3), padding=1, bias=False)
    self.conv4 = Conv2d(64, 64, (3,3), padding=1, bias=False)
    self.conv5 = Conv2d(64, 128, (3,3), padding=1, bias=False)
    self.conv6 = Conv2d(128, 128, (3,3), padding=1, bias=False)
    self.conv7 = Conv2d(128,256, (3,3), padding=1, bias=False)
    self.conv8 = Conv2d(256, 256, (3,3), padding=1, bias=False)
    self.conv9 = Conv2d(256, 512, (3,3), padding=1, bias=False)
    self.conv10 = Conv2d(512, 512, (3, 3), padding=1, bias=False)
    self.conv10_s = Conv2d(512, 512, (2, 3), padding=(0, 1), bias=False)
    self.conv11 = Conv2d(512, 7507, (1, 1), padding=(0,0))
    
    self.batch1 = InstanceNorm2d(32, eps=1e-05, momentum=0.1, affine=True)
    self.batch2 = InstanceNorm2d(32, eps=1e-05, momentum=0.1, affine=True)
    self.batch3 = InstanceNorm2d(64, eps=1e-05, momentum=0.1, affine=True)
    self.batch5 = InstanceNorm2d(128, eps=1e-05, momentum=0.1, affine=True)
    self.batch7 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.batch8 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
    self.batch9 = InstanceNorm2d(512, eps=1e-05, momentum=0.1, affine=True)
    self.batch10 = InstanceNorm2d(512, eps=1e-05, momentum=0.1, affine=True)
    self.batch101 = InstanceNorm2d(512, eps=1e-05, momentum=0.1, affine=True)
    self.batch10_s = InstanceNorm2d(512, eps=1e-05, momentum=0.1, affine=True)
    self.drop1 = Dropout2d(p=0.2, inplace=False)
    
    if activation == 'ELU':
      self.leaky = nn.ELU()
    else:
      self.leaky = LeakyReLU(negative_slope=0.01, inplace=False)
    self.max1 = MaxPool2d((2, 2), stride=None)
    self.max2 = MaxPool2d((2, 1), stride=(2,1))
      
  def forward(self, x):
    try: x = x.cuda(0)
    except: pass
    x = self.conv1(x)
    x = self.batch1(x)
    x = self.leaky(x)
    x = self.conv2(x)
    x = self.batch2(x)
    x = self.leaky(x)
    x = self.max1(x)
    x = self.conv3(x)
    x = self.batch3(x)
    x = self.leaky(x)
    
    x = self.conv4(x)
    x = self.leaky(x)
    x = self.conv4(x)
    x = self.leaky(x)
    
    x = self.max1(x)
    x = self.conv5(x)
    x = self.batch5(x)
    x = self.leaky(x)
    
    x = self.conv6(x)
    x = self.leaky(x)
    x = self.conv6(x)
    x = self.leaky(x)
    
    x = self.max2(x)
    x = self.conv7(x)
    x = self.batch7(x)
    x = self.leaky(x)
    
    x = self.conv8(x)
    x = self.leaky(x)
    x = self.conv8(x)
    x = self.leaky(x)
    
    x = self.max2(x)
    x = self.conv9(x)
    x = self.batch9(x)
    x = self.leaky(x)
    
    x = self.conv10(x)
    x = self.batch10(x)
    x = self.leaky(x)
    
    x = self.conv10(x)
    x = self.batch101(x)
    x = self.leaky(x)
    
    x = self.conv10_s(x)
    x = self.batch10_s(x)
    x = self.leaky(x)
    
    x = self.drop1(x)
    x = self.conv11(x)
    x = x.squeeze(2)
    
    x = x.permute(0,2,1)
    y = x
    x = x.contiguous().view(-1,x.data.shape[2])
    x = LogSoftmax()(x)
    x = x.view_as(y)
    x = x.permute(0,2,1)

    return x
  

base_lr = 0.001
lr_decay = 0.99
momentum = 0.9
weight_decay = 0.0005
batch_per_epoch = 1000
disp_interval = 100
buckets = [54, 80, 124, 182, 272, 410, 614, 922, 1383, 2212]
     
def main(opts):
  model_name = 'ICCV_OCRF_ELU'
  net = OCRModel(activation='RELU') 
  acc = []
  if opts.cuda:
    net.cuda()
  optimizer = torch.optim.Adam(net.parameters(), lr=base_lr)
  step_start = 0  
  if os.path.exists(opts.model):
    print('loading model from %s' % args.model)
    step_start, learning_rate = net_utils.load_net(args.model, net)
  else:
    learning_rate = base_lr
  net.train()
  acc_test = test(net, codec, opts, list_file=opts.valid_list, norm_height=opts.norm_height, max_samples=1500)
  acc.append([0, acc_test])
  ctc_loss = CTCLoss()
  data_generator = ocr_gen.get_batch(num_workers=opts.num_readers,
          batch_size=opts.batch_size, 
          train_list=opts.train_list, in_train=True, norm_height=opts.norm_height)
  train_loss = 0
  cnt = 0
  for step in range(step_start, 10000000):
    images, labels, label_length = next(data_generator)
    im_data = net_utils.np_to_variable(images, is_cuda=opts.cuda, volatile=False).permute(0, 3, 1, 2)
    labels_pred = net(im_data)
    '''
    acts: Tensor of (seqLength x batch x outputDim) containing output from network
        labels: 1 dimensional Tensor containing all the targets of the batch in one sequence
        act_lens: Tensor of size (batch) containing size of each output sequence from the network
        act_lens: Tensor of (batch) containing label length of each example
    '''
    probs_sizes =  Variable(torch.IntTensor( [(labels_pred.permute(2,0,1).size()[0])] * (labels_pred.permute(2,0,1).size()[1]) ))
    label_sizes = Variable(torch.IntTensor( torch.from_numpy(np.array(label_length)).int() ))
    labels = Variable(torch.IntTensor( torch.from_numpy(np.array(labels)).int() ))
    loss = ctc_loss(labels_pred.permute(2,0,1), labels, probs_sizes, label_sizes) / opts.batch_size # change 1.9.
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    train_loss += loss.data.cpu().numpy()[0] #net.bbox_loss.data.cpu().numpy()[0]
    cnt += 1
    if step % disp_interval == 0:
      train_loss /= cnt
      print('epoch %d[%d], loss: %.3f, lr: %.5f ' % (
          step / batch_per_epoch, step, train_loss, learning_rate))
      train_loss = 0
      cnt = 0
    if step > step_start and (step % batch_per_epoch == 0):
      save_name = os.path.join(opts.save_path, '{}_{}.h5'.format(model_name, step))
      state = {'step': step,
               'learning_rate': learning_rate,
              'state_dict': net.state_dict(),
              'optimizer': optimizer.state_dict()}
      torch.save(state, save_name)
      print('save model: {}'.format(save_name))
      acc_test, ted = test(net, codec, opts,  list_file=opts.valid_list, norm_height=opts.norm_height, max_samples=1500)
      acc.append([0, acc_test, ted])
      np.savez('train_acc_{0}'.format(model_name), acc=acc)

if __name__ == '__main__': 
  parser = argparse.ArgumentParser()
  parser.add_argument('-train_list', default='/home/busta/data/90kDICT32px/train_icdar_ch8.txt')
  parser.add_argument('-valid_list', default='/home/busta/data/icdar_ch8_validation/ocr_valid.txt')
  parser.add_argument('-save_path', default='backup2')
  parser.add_argument('-model', default='ICCV_OCRF_792000.h5')
  parser.add_argument('-debug', type=int, default=0)
  parser.add_argument('-batch_size', type=int, default=2)
  parser.add_argument('-num_readers', type=int, default=1)
  parser.add_argument('-cuda', type=bool, default=True)
  parser.add_argument('-norm_height', type=int, default=64)
  args = parser.parse_args()  
  main(args)
