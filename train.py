'''
Created on Sep 3, 2017

@author: Michal.Busta at gmail.com
'''

import torch, sys, os
from torch.optim import SGD
import numpy as np
import cv2

import net_utils
import icdar
import timeit

from models import ModelResNet, ModelResNetSep, ModelResNetSep50

base_lr = 0.0001
lr_decay = 0.99
momentum = 0.9
weight_decay = 0.0005
batch_per_epoch = 1000
disp_interval = 1
     
def main(opts):
  
  model_name = 'SemanticText'
  if opts.small == 1:
    net = ModelResNet()
    model_name = 'SSText'
  elif opts.small == 2:
    net = ModelResNetSep(attention=True)
    model_name = 'SSTextResSep'
    print("Using Resnet Sep")
  elif opts.small == 50:
    net = ModelResNetSep50()
    model_name = 'SSTextResSep50'
    print("Using Resnet Sep 50")
  else:
    net = ModelResNetSep(attention=True)
    model_name = 'SSTextResSep'
  if opts.cuda:
    net.cuda()
    
    
  optimizer = torch.optim.Adam(net.parameters(), lr=base_lr)
  step_start = 0  
  if os.path.exists(opts.model):
    print('loading model from %s' % args.model)
    step_start, learning_rate = net_utils.load_net(args.model, net)
  else:
    learning_rate = base_lr
  
  net.train()
  
  data_generator = icdar.get_batch(num_workers=opts.num_readers, 
           input_size=opts.input_size, batch_size=opts.batch_size, 
           train_list=opts.train_list)
  
  train_loss = 0
  bbox_loss, iou_loss, angle_loss = 0., 0., 0.
  cnt = 0
  
  for step in range(step_start, 10000000):
    # batch
    images, image_fns, score_maps, geo_maps, training_masks, gtso, lbso, gt_idxs = next(data_generator)
    im_data = net_utils.np_to_variable(images, is_cuda=opts.cuda, volatile=False).permute(0, 3, 1, 2)
    start = timeit.timeit()
    iou_pred, roi_pred, angle_pred, features = net(im_data)
    end = timeit.timeit()
    
    # backward
    smaps_var = net_utils.np_to_variable(score_maps.squeeze(3), is_cuda=opts.cuda, volatile=False)
    training_masks = net_utils.np_to_variable(training_masks, is_cuda=opts.cuda, volatile=False)
    angle_gt = net_utils.np_to_variable(geo_maps[:, :, :, 4], is_cuda=opts.cuda, volatile=False)
    geo_gt = net_utils.np_to_variable(geo_maps[:, :, :, [0, 1, 2, 3]], is_cuda=opts.cuda, volatile=False)
    
    loss = net.loss(iou_pred, smaps_var, training_masks, angle_pred, angle_gt, roi_pred, geo_gt)
    bbox_loss += net.box_loss_value.data.cpu().numpy()[0] #net.bbox_loss.data.cpu().numpy()[0]
    iou_loss += net.iou_loss_value.data.cpu().numpy()[0]
    angle_loss += net.angle_loss_value.data.cpu().numpy()[0]
    train_loss += loss.data.cpu().numpy()[0]
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    cnt += 1
    if step % disp_interval == 0:
      
      if opts.debug:
        
        iou = iou_pred.data.cpu()[0].numpy()
        iou = iou.squeeze(0)
        cv2.imshow('iou_map', iou)
        iou_res = cv2.resize(score_maps[0], (images.shape[2], images.shape[1]))
        mask = np.argwhere(iou_res > 0)
        images[0][mask[:, 0], mask[:, 1], 1] = 1 
        images[0][mask[:, 0], mask[:, 1], 0] = 0 
        images[0][mask[:, 0], mask[:, 1], 2] = 0
        cv2.imshow('img', images[0]) 
        cv2.imshow('score_maps', score_maps[0] * 255)
        cv2.imshow('train_mask', training_masks.data.cpu()[0].numpy())
        cv2.waitKey(100)
      
      train_loss /= cnt
      bbox_loss /= cnt
      iou_loss /= cnt
      angle_loss /= cnt
      print('epoch %d[%d], loss: %.3f, bbox_loss: %.3f, iou_loss: %.3f, ang_loss: %.3f, lr: %.5f in %.3f' % (
          step / batch_per_epoch, step, train_loss, bbox_loss, iou_loss, angle_loss, learning_rate, end - start))

      train_loss = 0
      bbox_loss, iou_loss, angle_loss = 0., 0., 0.
      cnt = 0

    if step > step_start and (step % batch_per_epoch == 0):
      save_name = os.path.join(opts.save_path, '{}_{}.h5'.format(model_name, step))
      state = {'step': step,
               'learning_rate': learning_rate,
              'state_dict': net.state_dict(),
              'optimizer': optimizer.state_dict()}
      torch.save(state, save_name)
      print('save model: {}'.format(save_name))


import argparse

if __name__ == '__main__': 
  
  parser = argparse.ArgumentParser()
  parser.add_argument('-train_list', default='/home/busta/data/SynthText/loh/list.txt')
  parser.add_argument('-valid_list', default='/mnt/textspotter/tmp/SynthText/train.txt')
  parser.add_argument('-save_path', default='backup')
  parser.add_argument('-model', default='SSTextResSep_700000.h5')
  parser.add_argument('-debug', type=int, default=1)
  parser.add_argument('-batch_size', type=int, default=2)
  parser.add_argument('-num_readers', type=int, default=1)
  parser.add_argument('-cuda', type=bool, default=True)
  parser.add_argument('-input_size', type=int, default=512)
  parser.add_argument('-small', type=int, default=2)
  
  args = parser.parse_args()  
  main(args)
  
