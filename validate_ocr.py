'''
Created on Sep 29, 2017

@author: Michal.Busta at gmail.com
'''
import numpy as np
import torch.nn as nn

import os

f = open('codec.txt', 'r')
codec = f.readlines()[0]
#codec = u' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~£ÁČĎÉĚÍŇÓŘŠŤÚŮÝŽáčďéěíňóřšťúůýž'
f.close()
print(len(codec))

import torch

import net_utils
import argparse

from torch.autograd import Variable

from ocr_test_utils import test
from models import ModelResNetSep2, OCRModel

import cv2
  

base_lr = 0.001
lr_decay = 0.99
momentum = 0.9
weight_decay = 0.0005
batch_per_epoch = 1000
disp_interval = 100

buckets = [54, 80, 124, 182, 272, 410, 614, 922, 1383, 2212]

     
def main(opts):
  # pairs = c1, c2, label
  
  model_name = 'ModelResNetSep2'
  if opts.small == 2:
    net = OCRModel()
  else:
    net = ModelResNetSep2() 
  
  acc = []
    
  if opts.cuda:
    net.cuda()
    
  if os.path.exists(opts.model):
    print('loading model from %s' % args.model)
    step_start, learning_rate = net_utils.load_net(args.model, net)
  
  #net.eval()
  
 
      
  acc_test, ted = test(net, codec, opts,  list_file=opts.valid_list, norm_height=opts.norm_height, max_samples=100000)
  acc.append([0, acc_test, ted])
  np.savez('train_acc_{0}'.format(model_name), acc=acc)

if __name__ == '__main__': 
  parser = argparse.ArgumentParser()
  
  parser.add_argument('-valid_list', default='/home/busta/data/90kDICT32px/train_mlt.txt')
  parser.add_argument('-model', default='/mnt/textspotter/tmp/DS_CVPR/backup2/ICCV_OCRF_ELU_3000.h5')
  #parser.add_argument('-model', default='/mnt/textspotter/ICCV_OCRF_ELU_326000.h5')
  parser.add_argument('-cuda', type=bool, default=True)
  parser.add_argument('-norm_height', type=int, default=40)
  parser.add_argument('-small', type=int, default=1)
  
  args = parser.parse_args()  
  main(args)
  
