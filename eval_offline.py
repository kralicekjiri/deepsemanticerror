'''
Created on Sep 3, 2017

@author: Michal.Busta at gmail.com
'''
import os

import torch
import numpy as np
import cv2

import net_utils

import argparse

import math

from icdar import draw_box_points


from ocr_tools2 import OCRModel
from ocr_test_utils import print_seq_ext

import csv
import unicodedata as ud

f = open('codec.txt', 'r')
codec = f.readlines()[0]
#codec = u' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~£ÁČĎÉĚÍŇÓŘŠŤÚŮÝŽáčďéěíňóřšťúůýž'
f.close()
print(len(codec))

buckets = [54, 80, 124, 182, 272, 410, 614, 922, 1383, 2212]

eval_text_length = 0

def load_detections(p):
  '''
  load annotation from the text file
  :param p:
  :return:
  '''
  text_polys = []
  if not os.path.exists(p):
    return np.array(text_polys, dtype=np.float32)
  with open(p, 'r') as f:
    reader = csv.reader(f,  delimiter=',',  quotechar='"')
    for line in reader:
      # strip BOM. \ufeff for python3,  \xef\xbb\bf for python2
      line = [i.strip('\ufeff').strip('\xef\xbb\xbf') for i in line]
      
      x2, y2, x3, y3, x4, y4, x1, y1, conf = list(map(float, line[:9]))
      #cls = 0 
      text_polys.append([x1, y1, x2, y2, x3, y3, x4, y4, conf])
     
    return np.array(text_polys, dtype=np.float)
  
def normalize_text(text_line):
  text_line = text_line.replace("β", 'ß')
  text_line = text_line.replace("Ａ", 'A')
  text_line = text_line.replace("：", ':')
  text_line = text_line.replace("Ｃ", 'C')
  text_line = text_line.replace("Ｂ", 'B')
  text_line = text_line.replace("Ｄ", 'D')
  text_line = text_line.replace("Ｅ", 'E')
  text_line = text_line.replace("Ｆ", 'F')
  text_line = text_line.replace("Ｇ", 'G')
  text_line = text_line.replace("Ｈ", 'H')
  text_line = text_line.replace("Ｉ", 'I')
  text_line = text_line.replace("Ｊ", 'J')
  text_line = text_line.replace("Ｋ", 'K')
  text_line = text_line.replace("Ｌ", 'L')
  text_line = text_line.replace("Ｍ", 'M')
  text_line = text_line.replace("Ｎ", 'N')
  text_line = text_line.replace("Ｏ", 'O')
  text_line = text_line.replace("Ｐ", 'P')
  text_line = text_line.replace("Ｒ", 'R')
  text_line = text_line.replace("Ｓ", 'S')
  text_line = text_line.replace("Ｔ", 'T')
  text_line = text_line.replace("Ｕ", 'U')
  text_line = text_line.replace("Ｖ", 'V')
  text_line = text_line.replace("Ｗ", 'W')
  text_line = text_line.replace("Ｘ", 'X')
  text_line = text_line.replace("Ｙ", 'Y')
  text_line = text_line.replace("Ｚ", 'Z')
  
  text_line = text_line.replace("０", '0')
  text_line = text_line.replace("１", '1')
  text_line = text_line.replace("２", '2')
  text_line = text_line.replace("３", '3')
  text_line = text_line.replace("４", '4')
  text_line = text_line.replace("５", '5')
  text_line = text_line.replace("６", '6')
  text_line = text_line.replace("７", '7')
  text_line = text_line.replace("８", '8')
  text_line = text_line.replace("９", '9')
  return text_line
  
def load_gt(p):
  '''
  load annotation from the text file
  :param p:
  :return:
  '''
  text_polys = []
  text_gts = []
  if not os.path.exists(p):
    return np.array(text_polys, dtype=np.float32)
  with open(p, 'r') as f:
    reader = csv.reader(f,  delimiter=',',  quotechar='"')
    for line in reader:
      # strip BOM. \ufeff for python3,  \xef\xbb\bf for python2
      line = [i.strip('\ufeff').strip('\xef\xbb\xbf') for i in line]
      
      x1, y1, x2, y2, x3, y3, x4, y4 = list(map(float, line[:8]))
      #cls = 0
      gt_txt = '' 
      delim = ''
      for idx in range(9, len(line)):
        gt_txt += delim + line[idx]
        delim = ','
        
      text_polys.append([x4, y4, x1, y1, x2, y2, x3, y3])
      text_line = normalize_text(gt_txt)
        
      
      text_gts.append(text_line) 
     
    return np.array(text_polys, dtype=np.float), text_gts

def ocr_image(net, codec, img, norm_height, args):
          
  scale = norm_height / float(img.shape[0])
  ssf_y = norm_height / 32.0
  width = int(img.shape[1] * scale)
  
  best_diff = width
  bestb = 0
  for b in range(0, len(buckets)):
    if best_diff > abs(width * 1.3 - buckets[b] * ssf_y):
      best_diff = abs(width * 1.3 - buckets[b] * ssf_y)
      bestb = b

  width =  buckets[bestb] 
  
  scaled = cv2.resize(img, (int(buckets[bestb] * ssf_y), norm_height))  
  scaled = np.expand_dims(scaled, axis=2)
  scaled = np.expand_dims(scaled, axis=0)
  
  
  scaled_var = net_utils.np_to_variable(scaled, is_cuda=args.cuda, volatile=False).permute(0, 3, 1, 2)
  ctc_f = net(scaled_var)
  ctc_f = ctc_f.data.cpu().numpy()
  ctc_f = ctc_f.swapaxes(1, 2)
  
  labels = ctc_f.argmax(2)
  det_text, conf, dec_s = print_seq_ext(labels[0, :], codec)  
  
  if len(det_text) > 0 and 'ARABIC' in ud.name(det_text[0]):
    det_text = det_text[::-1]
  
  return det_text, conf, dec_s

def draw_detections(img, boxes, color = (255, 0, 0)):
  
  draw2 = np.copy(img)
  for i in range(0, boxes.shape[0]):
    pts = boxes[i]
    pts  = pts[0:8]
    pts = pts.reshape(4, -1)
    pts = np.asarray(pts, dtype=np.int)
    draw_box_points(draw2, pts, color=color, thickness=2)
    
  #cv2.imshow('nms', draw2)
  
  return draw2
  
def get_normalized_image(img, box):
  
  box2 = box[0:8].reshape(4, 2)
  box2 = np.asarray(box2, dtype=np.float32)
  
  dw = box2[2] - box2[1] 
  dh =  box2[0] - box2[1]
    
  w = math.sqrt(dw[0] * dw[0] + dw[1] * dw[1])
  h = math.sqrt(dh[0] * dh[0] + dh[1] * dh[1])
  center = box2.sum(0) / 4
  angle = math.atan2(dw[1], dw[0])  
  rr = ( (center[0], center[1]), (w, h), angle * 180 / math.pi )
  #box2 = cv2.boxPoints(rr)
  
  extbox = cv2.boundingRect(box2)

  if extbox[2] *  extbox[3] > img.shape[0] * img.shape[1]:
    print("Too big proposal: {0}x{1}".format(extbox[2], extbox[3]))
    return None, None
  extbox = [extbox[0], extbox[1], extbox[2], extbox[3]]
  extbox[2] += extbox[0]
  extbox[3] += extbox[1]
  extbox = np.array(extbox, np.int)
  
  extbox[0] = max(0, extbox[0])
  extbox[1] = max(0, extbox[1])
  extbox[2] = min(img.shape[1], extbox[2])
  extbox[3] = min(img.shape[0], extbox[3])
  
  tmp = img[extbox[1]:extbox[3], extbox[0]:extbox[2]]
  center = (tmp.shape[1] / 2,  tmp.shape[0] / 2)
  rot_mat = cv2.getRotationMatrix2D( center, rr[2], 1 )
  
  if tmp.shape[0] == 0 or tmp.shape[1] == 0:
    return None, rot_mat
  
  
  rot_mat[0,2] += rr[1][0] /2.0 - center[0]
  rot_mat[1,2] += rr[1][1] /2.0 - center[1]
  try:
    norm_line = cv2.warpAffine( tmp, rot_mat, (int(rr[1][0]), int(rr[1][1])), borderMode=cv2.BORDER_REPLICATE )
  except:
    return None, rot_mat
  return norm_line, rot_mat  
 

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 

import glob


def intersect(a, b):
  '''Determine the intersection of two rectangles'''
  rect = (0,0,0,0)
  r0 = max(a[0],b[0])
  c0 = max(a[1],b[1])
  r1 = min(a[2],b[2])
  c1 = min(a[3],b[3])
  # Do we have a valid intersection?
  if r1 > r0 and  c1 > c0: 
      rect = (r0,c0,r1,c1)
  return rect

def union(a, b):
  r0 = min(a[0],b[0])
  c0 = min(a[1],b[1])
  r1 = max(a[2],b[2])
  c1 = max(a[3],b[3])
  return (r0,c0,r1,c1)

def area(a):
  '''Computes rectangle area'''
  width = a[2] - a[0]
  height = a[3] - a[1]
  return width * height

import editdistance

def evaluate_image(img, detections, gt_rect, gt_txts, iou_th=0.3, iou_th_vis=0.5, iou_th_eval=0.4):
    
  '''
  Summary : Returns end-to-end true-positives, detection true-positives, number of GT to be considered for eval (len > 2).
  Description : For each predicted bounding-box, comparision is made with each GT entry. Values of number of end-to-end true
                positives, number of detection true positives, number of GT entries to be considered for evaluation are computed.
  
  Parameters
  ----------
  iou_th_eval : float
      Threshold value of intersection-over-union used for evaluation of predicted bounding-boxes
  iou_th_vis : float
      Threshold value of intersection-over-union used for visualization when transciption is true but IoU is lesser.
  iou_th : float
      Threshold value of intersection-over-union between GT and prediction.
  word_gto : list of lists
      List of ground-truth bounding boxes along with transcription.
  batch : list of lists
      List containing data (input image, image file name, ground truth).
  detections : tuple of tuples
      Tuple of predicted bounding boxes along with transcriptions and text/no-text score.
  
  Returns
  -------
  tp : int
      Number of predicted bounding-boxes having IoU with GT greater than iou_th_eval.
  tp_e2e : int
      Number of predicted bounding-boxes having same transciption as GT and len > 2.
  gt_e2e : int
      Number of GT entries for which transcription len > 2.
  '''
  
  gt_to_detection = {}
  detection_to_gt = {}
  tp = 0
  tp_e2e = 0
  tp_e2e_ed1 = 0
  gt_e2e = 0
  
  gt_matches = np.zeros(gt_rect.shape[0])
  gt_matches_ed1 = np.zeros(gt_rect.shape[0])
  
  for i in range(0, len(detections)):
      
    det = detections[i]
    box =  det[0] # Predicted bounding-box parameters
    box = np.array(box, dtype="int") # Convert predicted bounding-box to numpy array
    box = box[0:8].reshape(4, 2)
    bbox = cv2.boundingRect(box)
    
    bbox = [bbox[0], bbox[1], bbox[2], bbox[3]]
    bbox[2] += bbox[0] # Convert width to right-coordinate
    bbox[3] += bbox[1] # Convert height to bottom-coordinate
    
    det_text = det[1] # Predicted transcription for bounding-box
    
    for gt_no in range(len(gt_rect)):
        
      gtbox = gt_rect[gt_no]
      txt = gt_txts[gt_no] # GT transcription for given GT bounding-box
      gtbox = np.array(gtbox, dtype="int")
      gtbox = gtbox[0:8].reshape(4, 2)
      rect_gt = cv2.boundingRect(gtbox)
      
      
      rect_gt = [rect_gt[0], rect_gt[1], rect_gt[2], rect_gt[3]]
      rect_gt[2] += rect_gt[0] # Convert GT width to right-coordinate
      rect_gt[3] += rect_gt[1] # Convert GT height to bottom-coordinate 

      inter = intersect(bbox, rect_gt) # Intersection of predicted and GT bounding-boxes
      uni = union(bbox, rect_gt) # Union of predicted and GT bounding-boxes
      ratio = area(inter) / float(area(uni)) # IoU measure between predicted and GT bounding-boxes
      
      # 1). Visualize the predicted-bounding box if IoU with GT is higher than IoU threshold (iou_th) (Always required)
      # 2). Visualize the predicted-bounding box if transcription matches the GT and condition 1. holds
      # 3). Visualize the predicted-bounding box if transcription matches and IoU with GT is less than iou_th_vis and 1. and 2. hold
      if ratio > iou_th:
        if not gt_no in gt_to_detection:
          gt_to_detection[gt_no] = [0, 0]
          
        edit_dist = editdistance.eval(det_text.lower(), txt.lower())
        if edit_dist <= 1:
          gt_matches_ed1[gt_no] = 1
          draw_box_points(img, box, color = (0, 128, 0), thickness=2)
            
        if txt.lower() == det_text.lower():
          draw_box_points(img, box, color = (0, 255, 0), thickness=2)
          gt_matches[gt_no] = 1 # Change this parameter to 1 when predicted transcription is correct.
          
          if ratio < iou_th_vis:
              #draw_box_points(draw, box, color = (255, 255, 255), thickness=2)
              #cv2.imshow('draw', draw) 
              #cv2.waitKey(0)
              pass
                
        tupl = gt_to_detection[gt_no] 
        if tupl[0] < ratio:
          tupl[0] = ratio 
          tupl[1] = i 
          detection_to_gt[i] = [gt_no, ratio, edit_dist]  
                  
  # Count the number of end-to-end and detection true-positives
  for gt_no in range(gt_matches.shape[0]):
    gt = gt_matches[gt_no]
    gt_ed1 = gt_matches_ed1[gt_no]
    txt = gt_txts[gt_no]
    
    gtbox = gt_rect[gt_no]
    gtbox = np.array(gtbox, dtype="int")
    gtbox = gtbox[0:8].reshape(4, 2)
    
    if len(txt) > eval_text_length and txt != '###':
      gt_e2e += 1
      if gt == 1:
        tp_e2e += 1
      if gt_ed1 == 1:
        tp_e2e_ed1 += 1
        
            
    if gt_no in gt_to_detection:
      tupl = gt_to_detection[gt_no] 
      if tupl[0] > iou_th_eval: # Increment detection true-positive, if IoU is greater than iou_th_eval
        tp += 1   
      #else:
      #  draw_box_points(img, gtbox, color = (255, 255, 255), thickness=2)
        
  for i in range(0, len(detections)):  
    det = detections[i]
    box =  det[0] # Predicted bounding-box parameters
    box = np.array(box, dtype="int") # Convert predicted bounding-box to numpy array
    box = box[0:8].reshape(4, 2)
    
    if not i in detection_to_gt:
      draw_box_points(img, box, color = (0, 0, 255), thickness=2)
    else:
      [gt_no, ratio, edit_dist] = detection_to_gt[i]
      if edit_dist > 0:
        draw_box_points(img, box, color = (255, 0, 0), thickness=2)
            
  #cv2.imshow('draw', draw)             
  return tp, tp_e2e, gt_e2e, tp_e2e_ed1, detection_to_gt 

evaluate = True          

if __name__ == '__main__': 
  
  parser = argparse.ArgumentParser()
  parser.add_argument('-cuda', type=int, default=1)
  parser.add_argument('-model_ocr', default='/home/busta/git/DeepSemanticText/backup2/ICCV_OCRF_8859000.h5')
  parser.add_argument('-debug', default=1)
  parser.add_argument('-small', type=int, default=0)
  
  args = parser.parse_args()    
  
  orc_net = OCRModel()
  net_utils.load_net(args.model_ocr, orc_net)
    
  if args.cuda:
    print('Using cuda ...')
    orc_net = orc_net.cuda()
  
  orc_net = orc_net.eval()
  
  images_dir = '/home/busta/data/SynthText/icdar2017rctw_train/train'
  images_dir = '/home/busta/data/SynthText/icdar_ch8/Train'
  images_dir = '/home/busta/data/ch8_validation_e2e'
  #images_dir = '/home/busta/data/cvpr2018_show'
  loc_dir = 'icdar_valid'
  loc_dir = 'valid'
  #loc_dir = '/mnt/textspotter/tmp/EAST/icdar_valid'
  #loc_dir = '/mnt/textspotter/tmp/EAST/show'
  
  images = glob.glob( os.path.join(images_dir, '*.jpg') )
  
  tp_all = 0  
  gt_all = 0
  tp_e2e_all = 0
  gt_e2e_all = 0
  tp_e2e_ed1_all = 0
  detecitons_all = 0
  
  im_no = 0
  for img_name in sorted(images):
    base_nam = os.path.basename(img_name)
    res_gt = base_nam.replace(".jpg", '.txt')
    res_gt = '{0}/gt_{1}'.format(images_dir, res_gt)
    if not os.path.exists(res_gt):
      print('missing! {0}'.format(res_gt))
      continue
    gt_rect, gt_txts  = load_gt(res_gt)
    
    res_txt = base_nam.replace(".jpg", '.txt')
    res_txt = '{0}/{1}'.format(loc_dir, res_txt)
    if not os.path.exists(res_txt):
      print('missing! {0}'.format(res_txt))
      #continue
    
    print(img_name)
    
    img = cv2.imread(img_name)
    
    detections = load_detections(res_txt)
    
    
    draw = draw_detections(img, detections)
    #draw = draw_detections(draw, gt_rect, color=(0, 255, 0))
    
    
    cv2.imshow('draw', draw)
    
    detetcions_out = []
    
    pil_img = Image.fromarray(draw)
    pil_draw = ImageDraw.Draw(pil_img)
    
    font = ImageFont.truetype("/home/busta/git/DS_CVPR/Arial-Unicode-Regular.ttf", 30)
    
    box_no = 0
    for box in detections:
    #for box in gt_rect:
      
      box_img, rot_mat = get_normalized_image(img, box)
      if box_img is None:
        print('Bad bounding box!')
        box_no += 1
        continue
      
      if box_img.shape[1] < box_img.shape[0]: # and len(gt_txts[box_no]) > 2: and gt_txts[box_no] != '###':
        cv2.imshow('box', box_img)
        box_img = np.rot90(box_img)
        cv2.imshow('boxr', box_img)
        key = cv2.waitKey(1)
      
      box_no += 1
      ocr_src = box_img.mean(2)
      ocr_src /= 128
      ocr_src -= 1
      
      det_text, conf, dec_s = ocr_image(orc_net, codec, ocr_src, 32, args)
      det_text = normalize_text(det_text)
      
      det_text = det_text.strip()
      if len(det_text) <= eval_text_length:
        continue
      
      if not evaluate:
                
        width, height = pil_draw.textsize(det_text, font=font)
        center =  [box[2] + 2, box[3]]
        
        draw_text = det_text 
        if len(det_text) > 0 and 'ARABIC' in ud.name(det_text[0]):
            draw_text = det_text[::-1]
        
        pil_draw.text((center[0], center[1]), draw_text, fill = (0,255,0),font=font)
        
      detetcions_out.append([box, det_text])
      
    pix = np.array(pil_img)
    
    if evaluate:
      tp, tp_e2e, gt_e2e, tp_e2e_ed1, detection_to_gt  = evaluate_image(pix, detetcions_out, gt_rect, gt_txts)
      tp_all += tp 
      gt_all += len(gt_txts)
      tp_e2e_all += tp_e2e
      gt_e2e_all += gt_e2e
      tp_e2e_ed1_all += tp_e2e_ed1
      detecitons_all += len(detetcions_out)
      
      pil_img = Image.fromarray(pix)
      pil_draw = ImageDraw.Draw(pil_img)
      
      det_no = 0
      for box, det_text in detetcions_out:
        
        width, height = pil_draw.textsize(det_text, font=font)
        center =  [box[2] + 3, box[3] - height - 2]
        
        draw_text = det_text
        if len(det_text) > 0 and 'ARABIC' in ud.name(det_text[0]):
            draw_text = det_text[::-1]
        
        pil_draw.text((center[0], center[1]), draw_text, fill = (0,255,0),font=font)
        if det_no in detection_to_gt:
          [gt_no, ratio, edit_dist] = detection_to_gt[det_no]
          if edit_dist > 0:
            center[0] += width + 5
            gt_text = gt_txts[gt_no]
            pil_draw.text((center[0], center[1]), gt_text, fill = (255,0,0),font=font)

            
            
        det_no += 1
      pix = np.array(pil_img)
      
      print("  E2E recall {0:.3f} / {1:.3f} / {2:.3f}, precision: {3:.3f}".format( 
        tp_e2e_all / float( max(1, gt_e2e_all) ), 
        tp_all / float( max(1, gt_e2e_all )), 
        tp_e2e_ed1_all / float( max(1, gt_e2e_all )), 
        tp_e2e_all /  float( max(1, detecitons_all)) ))
      
      #pix = cv2.cvtColor(pix, cv2.COLOR_BGR2RGB)
      cv2.imwrite('/tmp/prev/{1:.3f}_{0}'.format(base_nam, tp_e2e / float( max(1, gt_e2e) ) ), pix )
    else:
      cv2.imwrite('/tmp/prev/{0}'.format(base_nam), pix)
      
    if im_no > 100:
      break
    im_no += 1
    cv2.imshow('pix', pix)
    cv2.waitKey(1)
      
    
    
    
    
    
    
  
